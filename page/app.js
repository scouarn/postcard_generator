const URL = "/gen";

const ctx = new window.AudioContext();
let source;
let playing = false;

document.addEventListener("keydown", async (event) => {

    // Stop playing
    if (playing && event.key == "x") {
        source.stop();
        source.disconnect();
        playing = false;
        return
    }

    else if (playing || event.key != " ") {
        return;
    }

    playing = true;

    // Empty text
    const textField = document.getElementById("card");
    textField.innerText = "[Génération de la carte postale]";

    // Get generated card
    const resp = await fetch(URL)
        .then(resp => resp.json())

    // Set text
    fetch(resp.text)
        .then(resp => resp.text())
        .then(text => textField.innerText = text);

    // Load audio
    const buff = await fetch(resp.audio, { cache: "no-store" })
        .then(resp => resp.arrayBuffer())
        .then(data => ctx.decodeAudioData(data));

    // Play audio
    source = ctx.createBufferSource();
    source.buffer = buff;
    source.connect(ctx.destination);
    source.onended = () => { playing = false };
    source.start();
});

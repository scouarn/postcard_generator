# Générateur de cartes postales

Générateur de cartes postales qui utilise le gros modèle de langage (LLM)
[BLOOM](https://huggingface.co/bigscience/bloom) et un corpus de 638 cartes de
Royan. Le fichier `sample.txt` contient des exemples de cartes générées.

Vous aurez besoin d'un [compte HuggingFace](https://huggingface.co/) (gratuit)
et de copier [votre clé](https://huggingface.co/docs/api-inference/quicktour#get-your-api-token).

Requiert une licence pour le [SDK CereVoice](https://www.cereproc.com/products/sdk).


## Installation

### Avec Docker desktop

Charger l'image d'une archive :
```console
$ docker load --input royan.tar.gz
```

Images -> royan -> Run -> Optional settings :

* Container name: `royan`
* Host port : `8000`
* Host path : choisir le répertoire où seront placés les textes et l'audio
* Container path: `/royan/output`
* Variable : `HF_TOKEN`
* Value : votre clé HuggingFace

![](docker.png)

Après avoir lancé le container vous pouvez afficher les logs du serveur web et
accéder à un shell en cliquant sur Terminal, ce qui permet de lancer le
générateur.


### Avec Docker CLI

Charger l'image d'une archive :
```console
$ docker load --input royan.tar.gz
```

Démarrer un container :
```console
$ docker run --name royan -d --rm \
    -p 8000:8000 \
    -v /home/scouarn/royan_output:/royan/output \
    --env HF_TOKEN=hf_xxxxxxxxxxxxxxxxxxxxxxxxxxx \
    royan
```

Accéder au container avec un shell :
```console
$ docker exec -it royan /bin/bash
# ...
```


### Manuelle

Dans le répertoire `cerevoice` sont attendus des fichiers du SDK CereVoice :
 
* `txt2wav` : de `cerevoice_sdk_.../examples/basictts/txt2wav`
* `voice.voice` : fichier voix
* `license.lic` : licence de la voix

Votre clé HuggingFace doit être stockée dans le fichier `config.py` :
```python
HF_TOKEN = "hf_xxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

Ou bien dans une variable d'environnement :
```console
$ export HF_TOKEN='hf_xxxxxxxxxxxxxxxxxxxxxxxxxxx'
```


## Utilisation du générateur

Génère des cartes postales, les textes et les enregistrements audio sont
placés dans le répertoire `output`.

```console
$ ./gen.py
```


## Client web

Pour démarrer le serveur web si vous ne l'avez pas déjà lancé via Docker :
```console
$ ./server.py
```

Lecture des cartes générées. Touche espace pour lire une carte.

http://localhost:8000


## Bot discord

### Création

- Si vous n'en avez pas déjà un, créez un compte [Discord](https://huggingface.co/) (gratuit).

- Il faut ensuite [créer une application Discord](https://discord.com/developers/applications)
(`New application`).

- Dans l'onglet `Bot`, copiez sa clé (`View Token` ou `Reset Token`) pour plus tard.

- Dans la section `Privileged Gateway Intents`, activez `Message Content Intent`.

Vous pouvez inviter le bot sur un serveur en utilisant le générateur d'URL dans
l'onglet `OAuth2`. Il faut choisir le scope `bot` puis autoriser la permission
`Send Message`. En visitant le lien, on peut choisir sur quel serveur ajouter
le bot.


### Lancement

Stockez la clé du bot dans une variable d'environnement :
```console
$ export DISCORD_TOKEN='clé_du_bot'
```

Installez le module `discord.py` :
```console
$ pip install discord.py
```

Le bot peut être démarré avec (hébergement local) :
```console
$ ./bot.py
```


### Utilisation

- **`[scouarn]:`** 
> !help

- **`[royan-bot]:`**
>     Générateur de cartes postales
>         !help       Affiche ce message
>         !gen        Génère une carte
>         !sents      (En réponse à un message) Analyse des sentiments phrase par phrase
>         !ssml       (En réponse à un message) Analyse des sentiments format ssml

- **`[scouarn]:`**
> !gen

- **`[royan-bot]:`**
> Je suis arrivée le 20 courant. Le temps était magnifique et les environs de
> Royan ne m'ont point déçu. J'espère revenir encore cette année. Bonnes
> vacances à vous tous et bonne fin d'année. Cordialement.


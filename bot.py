#!/usr/bin/env python3

# Discord bot wrapper for the project

import sys
import os

import discord

import gen


TOKEN = os.getenv("DISCORD_TOKEN")

if TOKEN is None :
    print("`DISCORD_TOKEN` env variable not found", file=sys.stderr)
    print("run: export DISCORD_TOKEN='your_token'", file=sys.stderr)
    exit(1)


intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

HELP = """```Générateur de cartes postales
    !help       Affiche ce message
    !gen        Génère une carte
    !sents      (En réponse à un message) Analyse des sentiments
    !ssml       (En réponse à un message) Analyse des sentiments format ssml
```"""

# Truncate if too long
def trunc(s: str, max_len: int) -> str :
    err = "[...]"

    if len(s) <= max_len :
        return s

    return s[0:max_len-len(err)] + err


# NOTE: Exceptions don't make the server crash
@client.event
async def on_message(message) :
    if message.author == client.user:
        return

    match message.content :

        # Generate postcard
        case "!gen" :
            resp = gen.generate_text()
            if resp == "" :
                resp = "[VIDE]"
            await message.channel.send(resp, reference=message)


        # Sentence by sentence sentiment analysis (`[sentiment:score]` sentence)
        case "!sents" :
            ref = message.reference

            # Usage message
            if ref is None :
                await message.channel.send("Il faut envoyer `!sents` en réponse à un message.", reference=message)
                return

            # Get referenced message
            response_to = await message.channel.fetch_message(ref.message_id)
            text = response_to.content

            # Query sentiments and build response
            sentiments = gen.extract_sentiments(text)
            resp = " ".join(
                f"`[{s['labels'][0]}:{s['scores'][0]:0.3f}]`{s['sequence']}"
                for s in sentiments
            )

            await message.channel.send(resp, reference=message)


        # Sentence by sentence sentiment analysis and output SSML
        case "!ssml" :
            ref = message.reference

            # Usage message
            if ref is None :
                await message.channel.send("Il faut envoyer `!ssml` en réponse à un message.", reference=message)
                return

            # Get referenced message
            response_to = await message.channel.fetch_message(ref.message_id)
            text = response_to.content

            # Query sentiments and build response
            sentiments = gen.sentiments(text)
            ssml = gen.generate_ssml(sentiments)
            resp = f"```xml\n{ssml}\n```"

            await message.channel.send(resp, reference=message)


        case "!help" :
            await message.channel.send(HELP, reference=message)


client.run(TOKEN)

#!/usr/bin/env python3

import html
import http.client
import json
import os
import random
import re
import signal
import subprocess
import sys
import time
import wave

import config
import data.cards as data


# HuggingFace API wrapper ######################################################

class HFAPIException(Exception):
    pass

HF_URL = "api-inference.huggingface.co"

HF_TOKEN = os.getenv("HF_TOKEN")
if HF_TOKEN is None :
    HF_TOKEN = config.HF_TOKEN


# Query HF API
def hf_query(payload: dict, model: str) -> dict :

    headers = {
        "Authorization": f"Bearer {HF_TOKEN}",
        "Content-type": "application/json",
    }

    conn = http.client.HTTPSConnection(HF_URL, timeout=10)

    try :
        conn.request(
            "POST",
            f"/models/{model}",
            json.dumps(payload),
            headers
        )
        resp = conn.getresponse()
        data = resp.read().decode()
        conn.close()

    # FIXME: bad catchall for socket.gaierror and TimeoutError
    # (and other errors ?)
    except Exception as e : 
        raise HFAPIException(e)

    if resp.status != 200 :
        raise HFAPIException(resp, data)

    parsed_json = json.loads(data)

    if "error" in parsed_json :
        raise HFAPIException(parsed_json)

    return parsed_json


# Send inference query and return generated text
def predict(prompt: str, size:int=120, seed:int=None) -> str :

    if seed is None :
        seed = random.randint(0, 999999)

    # Inference params
    parameters = {
        "max_new_tokens": size,
        "seed": seed,
        "top_k": 50,
        "top_p": 0.9,
        "temperature": 1.0,
        "do_sample": True,
        "return_full_text": False,
    }

    # Query params
    options = {
        "use_cache": True,
        "wait_for_model": True,
    }

    # Send query
    payload = {
        "inputs": prompt,
        "parameters": parameters,
        "options" : options
    }

    response = hf_query(payload, "bigscience/bloom")
    return response[0]["generated_text"]


# Zero-shot classification query
def zero_shot_class(inputs: list[str], labels: list[str], multi_label=False) :
    payload = {
        "inputs": inputs,
        "parameters": { "candidate_labels": labels, "multi_label": multi_label },
        "options" :   { "use_cache": True, "wait_for_model": True },
    }

    response = hf_query(payload, "BaptisteDoyen/camembert-base-xnli")
    return response


# Postcard generator ###########################################################

def generate_text() -> str :

    # Params
    num_cards = 5
    max_pred  = 3

    description = "Voici une collection de vieilles cartes postales de Royan de la période avant-guerre."
    separator = "<Carte postale de Royan>"

    sep_re = re.compile(r"\<[^\>]*\>?")
    seed = random.randint(0, 99999999)

    # Preprompt generation
    input_blocks = random.sample(data.cards, k=num_cards)
    preprompt = (
        description 
      + "\n".join(separator + " " + block for block in input_blocks)
      + separator
    )

    # Accumulate predictions by appending to the history
    # until the begining of the next card is found
    generated = ""
    for _ in range(max_pred) :
        pred = predict(
            preprompt + generated,
            size=64,
            seed=seed,
        )

        # Don't include start of next card
        output_blocks = sep_re.split(pred, maxsplit=1)
        generated += output_blocks[0]

        # A separator was found
        if len(output_blocks) > 1 :
            break

    return generated.strip()


# Sentiment analysis ###########################################################

# Split after punctuation (including it)
# "a b ... c, d" => [ "a b ...", "c,", "d" ]
def split_sentences(text: str) -> list[str] :
    stops = [m.end() for m in re.finditer(r"[.?!;,:]+", text)]
    stops.append(len(text))

    sentences = []
    start = 0
    for s in stops :
        sentence = text[start:s].strip() #.replace("\n", " ")
        start = s

        if len(sentence) > 0 :
            sentences.append(sentence)

    return sentences


# Do zero-shot classification
def extract_sentiments(text: str) :
    # Newlines should be ignored
    sentences  = split_sentences(text.replace("\n", " "))

    labels = list(config.SENT_TAGS.keys())
    sentiments = zero_shot_class(sentences, labels)

    return sentiments


# Generate markup for TTS
def generate_ssml(sentiments) -> str :
    ssml  = "<?xml version='1.0'?>\n"
    ssml += "<speak>\n"

    for s in sentiments :
        sent = s['labels'][0]
        pre, post = config.SENT_TAGS[sent]

        inner = html.escape(s['sequence'])

        # Fix pronunciation of Royan
        # TODO: escape IPA chars ?
        inner = inner.replace("Royan", "<phoneme alphabet='ipa' ph='ʁwajɑ̃'>Royan</phoneme>")

        ssml += f"  {pre}{inner}{post}\n"

    ssml += "</speak>"
    return ssml


# Speech synthesis #############################################################

# TODO: less hacky solution
# Output to cerevoice/output.wav
def generate_audio(ssml: str) :
    with open("cerevoice/input.xml", "w") as fout :
        fout.write(ssml)

    os.system("cd cerevoice; ./txt2wav voice.voice license.lic - - - input.xml output.wav")



# Entrypoint(s) ################################################################

def pipeline() :
    # FIXME: TODO: not safe
    base_name = time.strftime("%Y-%m-%d_%H-%M-%S")

    path_txt  = os.path.join(config.OUTPUT_DIR, base_name + ".txt")
    path_ssml = os.path.join(config.OUTPUT_DIR, base_name + ".xml")
    path_wav  = os.path.join(config.OUTPUT_DIR, base_name + ".wav")

    print(f"ID: {base_name}")
    print("-" * 80)

    text = generate_text()
    print(text)
    print("-" * 80)

    sentiments = extract_sentiments(text)
    ssml = generate_ssml(sentiments)
    print(ssml)
    print("-" * 80)


    # Output

    with open(path_txt, "w") as fout :
        fout.write(text)

    with open(path_ssml, "w") as fout :
        fout.write(ssml)

    subprocess.run([
        "cerevoice/txt2wav", "cerevoice/voice.voice", "cerevoice/license.lic",
        "-", "-", "-",
        path_ssml, path_wav
    ])

    return base_name


if __name__ == "__main__" :

    # Handle sigint gracefully
    running = True

    def sig_handler(sig, frame) :
        global running
        running = False
        print("Quit received")

    signal.signal(signal.SIGINT, sig_handler)


    while running :

        try :
            t_start = time.time()
            pipeline()
            t_stop = time.time()
            print(f"OK: {t_stop-t_start:0.1f}s")

        except HFAPIException as e :
            print(f"HFAPIException: {e}")
            print("Error: waiting 10s")
            time.sleep(10)


        # Print output dir stats

        output_size = 0
        output_duration = 0
        output_audio_files = 0

        files = os.listdir(config.OUTPUT_DIR)

        for f in files :
            path = os.path.join(config.OUTPUT_DIR, f)

            if os.path.isfile(path) :
                output_size += os.path.getsize(path)

                if os.path.splitext(path)[1] == ".wav" :
                    output_audio_files += 1

                    w = wave.open(path, "r")
                    output_duration += w.getnframes() / w.getframerate()
                    w.close()

        print(f"Size: {output_size//1000000}Mb")
        print(f"Cards: {output_audio_files}")
        print(f"Duration: {output_duration:0.0f}s")

        print("\n")


#!/usr/bin/env python3

import config
import html
import http.server
import json
import os
import random
import sys

import gen


class Handler(http.server.SimpleHTTPRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def do_GET(self) :

        # Respond with the text and audio URLs of a generated postcard
        if self.path == "/gen" :

            try :
                basename = gen.pipeline()

            # Fallback on a previously generated card
            except gen.HFAPIException as e :
                print(f"HFAPIException: {e}")

                files = [
                    os.path.splitext(f)[0]
                    for f in os.listdir(config.OUTPUT_DIR)
                    if ".txt" in f
                ]

                # FIXME: case when files is empty
                basename = random.choice(files)

            path = os.path.join(config.OUTPUT_DIR, basename)
            resp = {"text": path + ".txt", "audio": path + ".wav"}

            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(json.dumps(resp), "utf-8"))


        elif self.path == "/" :
            self.path = "/page/index.html"
            super().do_GET()

        else :
            super().do_GET()


if __name__ == "__main__":

    host = "0.0.0.0"
    port = 8000

    if len(sys.argv) > 1 :
        port = int(sys.argv[1])

    server = http.server.HTTPServer((host, port), Handler)
    print(f"Listening on {host}:{port}")
    server.serve_forever()


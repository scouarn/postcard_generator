cards = ['Chère amie,\n'
 'Je suis très très bien enfin, il fait chaud. Un peu trop quand même ! La mer '
 'est\n'
 "belle et le soleil superbe. Bref, quinze jours où j'ai été bien gâtée. Mes "
 'amitiés à\n'
 'tout le monde.',
 'Chère Annick et Bernard,\n'
 'Nous vous envoyons un grand bonjour ensoleillé de Royan où nos vacances se\n'
 'passent très bien. Nous profitons au maximum de la mer, des belles plages '
 'de\n'
 'sable fin et de la piscine du camping. Les enfants font du vélo avec leurs '
 'copains\n'
 'dans le camping. Gros bisous. Florie, Nicolas, Nicole et Jean-Marie.',
 'Chers tous deux,\n'
 "J'espère que cette carte vous trouvera en pleine forme et que pour vous les\n"
 'vacances approchent. Pour nous ici ça va. Il fait beau. Nous sommes allés '
 'passer\n'
 'la journée à Royan avec Jean et maman. Pour eux, les vacances se terminent.\n'
 "Ils repartent samedi matin. Nous profitons au maximum. C'est le repos "
 'complet.\n'
 'Je vous quitte en vous embrassant bien fort de loin. Jocelyne, Ludovic, '
 'Franck.\n'
 'Salut Dédé,\n'
 'Je me fais bronzer Paupaul. Crois-moi, il est pas au chômage.',
 'Village Joël, avenue de la Grande Conches, Royan, Charente inférieure.\n'
 'Samedi,\n'
 'Chers parents,\n'
 'Nous avons fait un très bon voyage, pas fatiguant du tout. Ma tante nous\n'
 'attendait à la gare. Il fait un temps superbe et chaud comme on a pas ceci '
 'à\n'
 'Paris, du soleil dès le matin. Nous sommes allés au marché et Madeleine est\n'
 "déjà installée sur la plage à faire des pâtés. Je vais la retrouver. J'ai "
 'reçu une\n'
 "carte de Jules hier. Je fais des vœux pour qu'il soit content de son mois et "
 'ne se\n'
 'fatigue pas trop. Bons baisers. Charles.',
 'Mille baisers de votre petite frangine. Amitiés à tous. Marguerite.',
 "Il y en a trop. J'ai lu moi aussi le Torquoy de la Pour Dieu, je crois que "
 "c'est\n"
 'ainsi le titre. Il est en effet très beau et très bien écrit. Que tu es '
 'heureuse de\n'
 "pouvoir lire ainsi ! Pendant ces vacances, j'ai été en Bretagne. Tu as dû "
 'recevoir\n'
 "des cartes de là-bas. J'ai lu la Chauvinesse d'André Cheurret. Renée.",
 'Lundi 18h30\n'
 'Couchons à Saint-André-de-Cubzac et serons demain soir à Biarritz. Avons\n'
 'quitté les jeunes ce matin en bonne condition. Espérons des nouvelles à '
 'Biarritz\n'
 'ou à Lourdes. Bons baisers. Janine.',
 'Nini ainsi que toute la petite famille,\n'
 "Nous vous envoyons cette petite carte en espérant qu'il fait aussi beau où "
 'vous\n'
 "vous trouvez que par ici, car c'est vraiment le beau temps. Bien le bonjour "
 'et à\n'
 'bientôt. Marco, Lulu.',
 'Royan, mardi. Apporter pompe de bicyclette. Bonjour.',
 'Bonne pensée de vacances avec un temps magnifique. Alexandre en profite au\n'
 'mieux. Il adore la mer. Sincères amitiés.',
 'Samedi,\n'
 'Chers amis,\n'
 'Yvette doit vous écrire pour vous dire combien elle est contente de sa '
 'raquette,\n'
 "mais voyant qu'elle ne se décide pas vite, je viens vous donner de nos "
 'nouvelles.\n'
 "Pour le moment, il n'y a pas de symptômes pour Daniel ou Yvette. Louisette "
 'est\n'
 'tout à fait remise. Elle lui faudra seulement reprendre des forces. Nos '
 'vacances\n'
 "cette année n'auront pas été aussi paisibles que celle de l'an dernier. "
 'Moi-même,\n'
 "j'ai eu et ai encore un mal de reins qui me gêne beaucoup. Je suis obligée "
 'de\n'
 "rester assise à la plage et avec Daniel ce n'est guère possible. Pierre "
 "s'amuse-t-il\n"
 'un peu ? Bons baisers de tous.',
 'Chère marraine,\n'
 'Nous avons bien reçu ta lettre. Nous allons bien. Il y a beaucoup de '
 'soleil,\n'
 "vraiment du beau temps. Tous réunis, nous t'embrassons bien fort. Nathalie,\n"
 'Ginette.',
 'Bien cher frère et sœur et Christian,\n'
 'Nous sommes à Royan pour plusieurs jours. Après, nous partons pour Biarritz\n'
 'et là nous faisons demi-tour. Bons baisers à tous trois, et à bientôt. L. M. '
 'Cabot',
 'Souvenir de bonnes vacances passées à Royan avec un temps magnifique. Bons\n'
 'baisers de nous quatre. Édith.',
 'Saint-Georges-de-Didonne, le 5 juillet 1979\n'
 'Chers amis,\n'
 'Nous sommes bien arrivés après avoir fait un parcours sans aucun ennui et '
 'avoir\n'
 'traversé Le Mans impeccablement. La location est très bien. Nous y sommes\n'
 'bien installés et nous nous y plaisons beaucoup. Le paysage est joli, mais '
 'pour\n'
 'Titi comme pour moi, il nous faut pas le regarder en pensant à la Bretagne, '
 'il\n'
 "n'y a rien de comparable. Beaucoup moins de promenades à faire, mais nous\n"
 'allons nous reposer, tricot, lecture... À bientôt de vous revoir, et bon '
 'courage à\n'
 'vous trois. Amitiés. Geneviève, Titi.',
 "Amical souvenir d'une promenade faite avec un temps splendide. S. Heriaud.",
 'Chers tous,\n'
 "Quelques mots de Royan nous passons d'agréables vacances, il a fait très "
 'beau\n'
 'quinze jours, mais maintenant le ciel est brumeux, mais nous profitons pour\n'
 'visiter les régions. Je pense que vous vous portez tous bien. Bientôt le '
 'retour\n'
 'pour nous. Grosses bises à vous deux et aux enfants. Brigitte, Toni.',
 'Le 10 juillet 1967\n'
 'Mes chers parents et sœurs,\n'
 "En souvenir de notre journée d'hier, je vous donne un aperçu de Royan. Nous\n"
 'avons passé une bonne après-midi avec beaucoup de soleil et nous nous '
 'sommes\n'
 'bien baignés. En rentrant à vingt-deux heures, nous avons pris un bon repas. '
 'Le\n'
 'temps de faire tout cela, nous nous sommes couchés à minuit. Nous étions '
 'quatre\n'
 "dont un engagé de notre région. J'ai moi aussi reçu des nouvelles de "
 'Christian.\n'
 "Il va très bien. Il me dit qu'il monte la garde. Je lui envoie à lui aussi "
 'une carte.\n'
 'Samedi, nous sommes allés à Fouras, mais il ne faisait pas chaud, mais cela '
 'ne\n'
 "m'a pas empêché de me baigner. J'ai bien reçu votre lettre ce matin. Pas de\n"
 "chance avec les maisons, pas moyen d'en trouver, mais cela va venir. "
 "J'espère, je\n"
 'reviendrai peut-être à la fin de la semaine, mais quarante-huit heures pas '
 'plus !\n'
 'Car il y a le défilé. Je vous embrasse tous très tendrement ainsi que '
 'Madame\n'
 'Barrier. Guy.',
 'Royan, le 5 juin 1940\n'
 'Ma chère petite Micou,\n'
 "Je suis arrivé à bon port, mais j'ai eu beaucoup de difficulté pour trouver "
 'une\n'
 "chambre, J'ai toutefois réussi après avoir passé la première nuit dans un "
 'grenier.\n'
 "Monsieur Prainville est bien gentil et l'atelier est du même genre que celui "
 'de\n'
 'Monsieur Monguille. Je commence donc cet après-midi. Tu me répondras à\n'
 "l'adresse de l'atelier : 38, rue des écoles, Royan. Bon et affectueux "
 'baisers pour\n'
 'toi et pour chère maman. Papa.',
 'Le 22 mai 1992\n'
 'Cher Émile.\n'
 'Nous avons reçu ta carte hier. Nous sommes tous en bonne santé. Tu dois '
 'être\n'
 "content d'avoir plus que 165 jours à faire. Il doit te tarder de venir chez "
 'toi. Moi\n'
 "je vais toujours à l'école et je travaille bien. Élise. Je joins un mot à ma "
 'sœur ;\n'
 "tu n'as pas l'air de trop t'en faire ; moi je bûche toujours beaucoup "
 'quoique je ne\n'
 "m'en fais pas plus qu'il ne faut. J'aimerais tout de même autant être à ta "
 'place\n'
 "qu'à la mienne. J'espère que tu viendras nous voir lorsque tu seras "
 'démobilisé.',
 '20 avril.\n'
 'Chère Marcelle,\n'
 "Je profite d'un moment de tranquillité pour vous écrire. J'ai écrit aussi à "
 'Camille.\n'
 "J'ai passé un pas trop mauvais voyage et je suis arrivé hier au soir à 8 h "
 '30 ;\n'
 "aujourd'hui nous avons longé la falaise et il brisait beaucoup, c'était très "
 'joli. Je\n'
 'vous quitte. Donnez-moi de vos nouvelles et je vous répondrai. Je vous '
 'embrasse.',
 'Bien arrivée avec du beau temps. Hier, mardi, une chaleur torride de '
 'vingt-huit\n'
 "degrés à l'ombre. Nous sommes déjà comme des écrevisses, mais nous avons eu\n"
 'hier soir un fort orage et ce matin, il ne fait pas beau. Nous partons voir '
 'le tour\n'
 'de France. Bons baisers de tous les cinq. Yvette',
 'Royan, 12 juillet 1918\n'
 'Mon grand trésor,\n'
 "Aujourd'hui le ciel est encore maussade, il a plu une partie de la nuit, à "
 'la grande\n'
 "joie des paysans, mais ce n'est pas cela qui fera baisser les vivres. Ce "
 'matin il y a\n'
 'eu une émeute au marché, le préfet a taxé le poisson et les pommes de terre, '
 'mais\n'
 "les marchands ont déclaré qu'ils se foutaient de la taxe et ils ont vendu au "
 'prix\n'
 "ordinaire ici, c'est-à-dire la moitié plus cher que la taxe. Alors, les "
 'clients ont\n'
 'refusé de payer si cher, cela a fait une bataille générale. Toute la '
 'marchandise\n'
 'volait dans le marché, les agents sont intervenus, mais les marchands ont '
 'alors\n'
 'déclaré que si on ne levait pas la taxe, ils apporteraient plus rien sur le '
 'marché et\n'
 'que Royan manquerait de tout. Le maire a cédé, mais la population est '
 'outrée.\n'
 "J'ai reçu hier une lettre charmante de Fontaine. Il m'écrit que Chevalier "
 'est\n'
 'un fumiste et que malgré ses promesses, il ne lui fait pas travailler son '
 'chant.\n'
 'Organise donc une messe avant ton départ, Fontaine serait content de '
 'chanter.\n'
 'Fais-le chanter accompagné par ton violoniste. Fais jouer aussi Bildet.\n'
 'Le quatorze juillet il va y avoir un joli concert populaire en plein air sur '
 'les\n'
 "promenades. Mme Boyez et Bardot, contralto de l'opéra, M. Lestely, célèbre\n"
 "baryton de l'opéra, Huborton, du théâtre américain de New York et ténor de\n"
 "l'opéra qui vient de remporter un grand succès dans Samson, prêteront leur\n"
 "concours. Ce sera épatant. Tâche donc d'avoir une permission, car je crois "
 'que\n'
 "ton voyage n'est pas prêt d'aboutir et je commence à trouver le temps bien "
 'long\n'
 'loin de mon cher mari adoré. Embrasse bien Mère pour nous tous et reçoit '
 'des\n'
 "baisers de tous, mais je t'envoie le miens en particulier, ce que tu aimes "
 'et que\n'
 "j'aime tant te donner. Ta petite amante à toi.",
 'Nous vous envoyons de grosses bises de Royan où nous sommes venus passer\n'
 "quelques jours. Nous pensons bien à vous et vous remercions pour l'imper de\n"
 "Philippe et votre petit mot. À bientôt j'espère. Monique.",
 'Bon voyage. Nous quittons Royan demain matin. Bons baisers de nous tous.\n'
 'Marc.',
 'Royan, le 28/8/70\n'
 'Chère maman, cher papa, chère Claude,\n'
 'Juste quelques mots, car je vous écris de la poste de Royan, pour vous dire '
 'que\n'
 "nous rentrerons jeudi trois septembre dans l'après-midi. Nous pensons donc "
 'vous\n'
 'voir. Nous espérons que vos vacances se passent bien. Pour nous, tout va '
 'bien\n'
 'et Christine devient bien coquine. Elle a réussi ce matin à retirer toute '
 'seule sa\n'
 'genouillère. Vous devez espérer que le bébé va arriver prochainement pour '
 'vous\n'
 'éviter de revenir. Je vous quitte. Je vous embrasse très fort.',
 'Merci pour votre aimable lettre. À vos bons souhaits. Nous avons un temps\n'
 "splendide. J'espère avoir le plaisir de vous voir à mon retour à Paris, je "
 'vous\n'
 'parlerai de mes vacances. E. Allard.',
 'Royan, le 8/8/64\n'
 'Ma pépette chérie,\n'
 "Aujourd'hui, nous avons reçu ta carte. Cela nous fait plein de plaisir, car "
 'on\n'
 "s'ennuie un peu de toi. En ce moment, Nanot est en plein boom avec Mimile "
 'au\n'
 'foin. Christine, elle, joue avec Nadine et moi. Je lis. Ce matin, nous avons '
 'été\n'
 'aux huîtres, coques, moules et crabes. Ce soir le dîner est complet. Le '
 'temps\n'
 "n'est pas brillant. Le soleil se cache et il vente comme l'an dernier. "
 'Remarque,\n'
 "trop de chaleur, ce n'est pas bien. Ma fifille repose-toi bien, et sois "
 'mignonne. Je\n'
 "t'embrasse bien affectueusement et Nanot aussi et titite de gros mimis "
 'partout.\n'
 'Maman.',
 'Royan, le 29 août\n'
 'Grand hôtel\n'
 'Mon cher petit Pierre,\n'
 "J'espère beaucoup que vous viendrez pendant votre séjour ici nous faire une\n"
 "visite. Les petits cousins l'espèrent bien aussi ! Je t'embrasse bien "
 'tendrement\n'
 'mon cher petit neveu, ta tante Jeanne.',
 'Un bonjour de Royan où vous avons rejoint la famille. À bientôt.',
 'Chère mémé,\n'
 "J'espère que ta santé est bonne et que le soleil est là. Lundi, je vais "
 'manger chez\n'
 "Tata pour l'anniversaire de Martine. Nous avons du beau temps. Gros bisous "
 'à\n'
 "toi ainsi qu'à Jeff. David.\n"
 'Un bonjour de mémé Jeannette.',
 'Amical souvenir de vacances. Nicole Geneaux.',
 'Ma petite Renée,\n'
 'Voyage de quelques jours sur la côte très chaude. Je ne sais pas si ma carte '
 'te\n'
 "trouvera à Feins. Ici, de bonnes nouvelles de maman, mais je n'en ai aucune "
 'de\n'
 "Pat et je m'inquiète. As-tu eu quelque chose ? Je t'embrasse "
 'affectueusement\n'
 'ainsi que mémère et Paulette. Germaine.',
 'Chers amis,\n'
 "Nous espérons que vous passez d'excellentes vacances. Nous, cela se passe "
 'au\n'
 'mieux, ici. Et nous vous envoyons notre bon souvenir et amitiés. Michel et\n'
 'Louise.',
 'Architecture moderne ! Et amicales pensées. Annie.',
 "Laquelle choisirais-tu ? Longtemps, j'ai aimé les 203. Sais-tu distinguer "
 'Caravelle\n'
 "et Floride ? Je me souviens que des Dauphines s'appelaient Ondines. À quel\n"
 'âge as-tu passé le permis de conduire ? Claude.',
 '17/7/09\nBon bécot et amitiés aux parents. Félix.',
 'Très chère Bibiche,\n'
 'Si tu savais comme j\'ai envie de petit "rieux" et mignon petit chat, mon '
 'cher\n'
 'somnifère ! Avec toutes les estivantes qui commencent à affluer... et le '
 'soleil\n'
 "aidant... Mais c'est toi que j'aime, toi seule ma petite fafame adorée. "
 "J'embrasse\n"
 "ton p'tit moineau et ta p'tite boubouche de tout mon cœur.",
 "Souvenirs d'Ami. Georges.",
 'Bons souvenirs. G. Mette.',
 'Un bonjour de Royan. Chantal, Sylvia.',
 'Auvilar, 28 mai 1924,\n'
 'Cher père, Jeanne et Joseph,\n'
 "Je suis aujourd'hui plus fatiguée qu'hier. J'ai pu arriver après 6 heures. "
 'Le temps\n'
 'est magnifique, la campagne est superbe. Profitez bien des quelques jours '
 "qu'il\n"
 "vous reste encore. J'ai déjà eu l'occasion d'avoir des vers, et des "
 'renseignements\n'
 "dans un cas de danger plus grand. Il faut espérer que tout s'arrangera. Je "
 'vous\n'
 'embrasse bien affectueusement.',
 "Affectueuses pensées de Royan où nous avons la chance d'avoir du beau "
 'temps.\n'
 'À bientôt.',
 'De Royan, mon meilleur souvenir. F. Duchier.',
 'Un bonjour de Royan avec un beau temps et du soleil. Bons baisers.',
 'Chère tante,\n'
 '15 jours de vacances à Royan. La plage pour Léna qui prend des couleurs. '
 'Nous\n'
 "l'avons emmené au zoo, elle te racontera. Une bise à Pascal. Nous "
 "t'embrassons\n"
 'bien fort. Vincent, Liliane, Léna.',
 '11 juillet 1968\n'
 'Chère madame Martin,\n'
 'Nous vous adressons notre amical souvenir de la Charente-Maritime où nous\n'
 'passons de reposantes et agréables vacances chez des amis. La région est si '
 'belle\n'
 'il y a tant à voir. Le temps est magnifique. Nos enfants sont en Corse.',
 'Ma chère Lydie,\n'
 'Nous partons demain pour Angers et comme nous y allons par le train, nous\n'
 "irons donc jusqu'à Sedan. Nous arriverons à Paris à 13h40 vendredi, donc si "
 'tu\n'
 'peux viens nous chercher à la gare Montparnasse. Et le train pour Sedan est '
 'à\n'
 '17h05. À bientôt bon baisers.',
 'Ma chère amie,\n'
 "Je suis très étonné que vous n'ayez pas reçu ma lettre ou je vous disais que "
 'nous\n'
 "partions en vacances à Ussy à la pêche où nous sommes allés l'année "
 'dernière.\n'
 'En ce moment, nous sommes à Royan, mais nous repartirons lundi à Ussy. Donc\n'
 'écrivez-moi. Je vous embrasse votre ami L. Tluyo.',
 'Bien le bonjour de Royan. Marcel Souille.',
 'Saint-Palais, 30 juillet\n'
 "Dans le pays des huîtres, je t'en envoie quelques-unes que, je l'espère, tu "
 'trouveras\n'
 'à ton goût. Remercie de ma part ta maman de toutes les gâteries pour moi et\n'
 'garde un gros baiser de Gaby. G. Mathe.',
 'Chère Nicole et Raymond,\n'
 'Nous entamons notre dernière semaine. Le temps passe très vite et nous '
 "n'aurons\n"
 "sûrement pas fini tout ce que nous pensions faire. Il faut dire qu'il fait "
 'très chaud\n'
 'et la chaleur nous accable un peu si bien que le courage nous manque. Nous\n'
 'allons souvent nous baigner, car les gamins aiment bien cela. Avez-vous '
 'fait\n'
 "votre fameux feu d'artifice ? Nous sommes allés le voir à Cozes. On a vu "
 'mieux.\n'
 'Carole et Patrick ont fait la retraite aux flambeaux avec des falots. Sinon, '
 'tout\n'
 "le monde est en forme. J'espère qu'il en est de même pour vous. Grosses "
 'bises.\n'
 'À bientôt.',
 'Avec notre meilleur souvenir, de bonnes amitiés. Famille Bouchut.',
 '7-1960\n'
 'Bien arrivé pour trouver... La pluie, et du monde. Tout va bien. Déjà '
 'beaucoup\n'
 'de "choses" à faire et de gens à voir. Je t\'écrirai demain matin et '
 "t'enverrai\n"
 'un petit paquet. Floflo va bien. Nous allons chez Beaumont. Embrasse bien\n'
 "Geneviève et dis-lui qu'il aura quelque chose pour elle dans le paquet. "
 'JeanClaude.',
 'Cordiales poignées de mains. Bonjour aux copains. Roland.',
 'Amicales pensées de vacances. Nous avons un temps moyen. Nous espérons que\n'
 'tes vacances se passent bien. Bonjour à tes parents. Bernard.',
 'Bon souvenir de Saint-Georges-de-Didonne. Colette, Nicole.',
 'Beaucoup de soleil, mais beaucoup de monde pour notre petite balade du '
 '15-8.\n'
 'La Vendée nous a plu. Les huîtres aussi. Bises à tous. Françoise.',
 '17 Villa Petit Nice, allée de la Grandière, Saint-Georges-de-Didonne. Marcel '
 'a\n'
 'dû te mettre au courant de son voyage. Il a bien employé son temps et ne '
 'doit\n'
 'pas le regretter. Nos filons des jours heureux et je pense souvent à ta '
 'sagesse.\n'
 'Ce petit coin te conviendrait à merveille. Mes amitiés aux tiens. Je te la '
 'serre.\n'
 'Bernard.',
 'Bons baisers de vacances dans un coin splendide et avec un soleil radieux '
 'tous les\n'
 "jours. Nous espérons qu'il en est de même pour vous et que la santé est "
 'bonne.\n'
 'Corinne, Bruno, Patricia, Jacques.',
 'Sincères amitiés de fin de vacances. Temps splendide, eau salée sauf pour '
 'le\n'
 'Ricard. Nous espérons en bonne santé. À la semaine prochaine. René, Ginette.',
 'Envoi de Bargeaud, 137 territorial, 32e compagnie, 2e Escouade à Saintes\n'
 'Chers cousins et cousines,\n'
 'Je viens vous donner de mes nouvelles qui sont très bonnes pour le moment,\n'
 "je désire bien qu'il en soit de même chez vous. Nous sommes rendu à Saintes\n"
 'depuis le vint-neuf au soir, mais ça ne vaut pas la grève. Je termine en '
 'vous\n'
 'embrassant tous. Votre cousin.\n'
 'Bonjour de ma part à Jeanne. Jean Bargeaud.',
 "État de santé stationnaire, aussi j'ai pu partir quinze jours en vacances "
 'faire de\n'
 'la chaise longue au soleil. Christian en profite pour visiter le coin '
 'charentais\n'
 "qu'il ne connaît pas. Pour mon compte, il faut me contenter d'un tour de "
 'ville\n'
 'en voiture : Royan, La Rochelle, Rochefort. Merci de ta carte. Bons '
 'baisers.\n'
 'Amical souvenir. Christian, Michelle.',
 'Il ne fait pas chaud à Royan. Nous espérons que vous êtes en bonne santé. '
 'Nous\n'
 'rentrerons le 15 à Sèvres après un détour sur Saint-Malo où nous passerons '
 'deux\n'
 'jours avec Jean. Avec nos bonnes amitiés.',
 'Nous sommes en bonne santé tous trois et vous envoyons de bons baisers. '
 'Voici\n'
 "notre adresse : villa Paul, boulevard de l'Océan, Royan. J'écrirai ces "
 'jours-ci.\n'
 'Henriette.',
 'Avons suivi votre conseil. Amitiés. Alice.\n'
 'Un souvenir de Royan. Bonjour à tous. Claude.',
 'Royan, le 26 juillet 1918\n'
 'Chère Madame,\n'
 'Deux mots pour vous donner mes nouvelles. Je suis en bonne santé et je vous\n'
 'souhaite que ma carte vous trouvera de même. Je vous assure que je suis\n'
 "loin d'être soigné comme à Buffon, la référence. C'est comme « la nuit et "
 'le\n'
 'jour ». Je garderais un bon souvenir de vous tous. Je serai heureux de '
 'pouvoir y\n'
 'retourner. Cela me sera bien dur. On est nourri parfaitement comme dans la\n'
 'caserne. Chacun à son tour va chercher la soupe. Chère madame, je termine\n'
 'en vous remerciant beaucoup de votre dévouement et je vous serre la main.\n'
 'Cordialement. Micout Corentin.\n'
 "Ce qui me fait plus de peine c'est que je ne vois plus ma bonne et gentille\n"
 "infirmière qui s'est donnée tant de peine à me sauver.",
 'Vacances agréables, mais pas assez de soleil pour la plage, mais idéal pour '
 'la\n'
 'promenade. Recevez tous nos bons baisers. Paulette, Raymond, Monique.',
 "Royan, le 12 août 1909\nSouvenir d'un bon ami.",
 "Je m'amuse bien, je mange bien, on va sur la plage chanter ou faire des "
 'jeux,\n'
 'Colonne des papeteries Navarre, Saint-Georges-de-Didonne, camp de Suzac,\n'
 'Charente-Maritime.',
 'Arrivé à bon port. Beau temps. Résidence à la plage pour tous à '
 'Saint-Georgesde-Didonne.',
 'Bonjour de Pontaillac.',
 'Notre bon souvenir de vacances. Le séjour à Royan tire à sa fin. Bientôt à '
 'votre\n'
 'retour de partir en vacances. Bien amicalement. Denise, Alain, Bertrand.',
 'Bons baisers.',
 'Un gros baiser.',
 'Bon souvenir et meilleures amitiés de Royan ou le temps est incertain. En\n'
 'profitons pour nous reposer le plus possible. À bientôt le plaisir de vous '
 'revoir.\n'
 'Sylvaine, Nadine.',
 'Vaux sur mer, 8/9/64\n'
 'Mon cher papa,\n'
 'Il est 21h30 et nous allons bientôt nous coucher, car nous démarrons demain\n'
 "matin vers huit heures en direction de l'île de Ré. Le temps est toujours "
 'beau.\n'
 'Nous avons pris cet après-midi notre neuvième bain ! Mais je ne suis pas '
 'resté\n'
 "très longtemps dans l'eau, car aujourd'hui il y avait des méduses. Ça "
 "m'écœure.\n"
 "Ensuite nous avons encore fait un tour jusqu'à Royan. Je voulais aussi te\n"
 "prévenir que j'ai passé il y a quelques jours une commande aux filatures de "
 'la\n'
 "Redoute. Peut-être ne tarderont-il pas à l'envoyer à Essey. La commande est\n"
 'de 24 000 Francs Si tu peux la payer au facteur, je te rembourserai en '
 'revenant.\n'
 'Merci tout plein en attendant. Gros baisers, cher papa. Cécile.',
 'Un bonjour de Royan ou les vacances se passent bien avec beaucoup de '
 'chaleur.\n'
 'Grosses bises. Lucienne, Bernard, Pascal, Nathalie.',
 "J'ai un crayon-feutre, c'est affreux.\n"
 'Pendant que vous manipuliez le goqueux, nous faisons les lézards sur cette '
 'belle\n'
 "plage de Saint-Georges. Aujourd'hui pas question, il flotte, donc courrier. "
 'Avec\n'
 'ce temps, nous avons confiance que le commerce marche. Je gave mon grand\n'
 'neveu de somnifères pour être tranquille, car il a la bougeotte. Nos amitiés '
 'de\n'
 'vacances. Gilbert.',
 "Bon séjour à Royan où j'ai la chance d'avoir un très beau temps. Toute la "
 'petite\n'
 "famille va bien. Un gros bisou d'Arnaud. Amitiés et bons baisers.",
 "Comme je n'ai rien à faire, je regarde la télé et, en même temps, j'en "
 'profite\n'
 "pour t'écrire. J'espère que tu t'es bien amusé en voyage. Bonjour à tous.",
 '01-08-1995\n'
 'Chère amie,\n'
 "Nous voici à Royan, Henri et moi, jusqu'au 20 août. Il fait pas très beau, "
 'et\n'
 "baignades et promenades sont notre emploi du temps. J'espère que votre "
 'santé\n'
 'est bonne, pas trop fatigué par vos travaux. Nous nous joignons pour vous\n'
 'envoyer nos bons baisers. Janine, Henri.',
 'Bien amicalement de Royan.',
 'Chers parents,\n'
 'Nous sommes arrivés à Royan hier, lundi, à cinq heures. Le temps et '
 'passable,\n'
 "c'est-à-dire que le soleil est pas trop brûlant, mais il y en a quand même "
 'un peu.\n'
 'La plage est magnifique, elle a trois kilomètres de longueur, et la ville '
 'est très\n'
 "grande. J'espère que pour vous il y a du soleil et malgré que vous ne "
 'sortiez\n'
 'pas que vous passerez de bonnes vacances quand même. Maman, quand tu vas\n'
 'recevoir des nouvelles de Lulu, donne-moi son adresse au Portugal comme ça, '
 'je\n'
 "pourrais lui passer un petit mot. J'espère que Daniel va bien et que la télé "
 'lui\n'
 "plaît ainsi qu'a tout le monde. Pour aujourd'hui, je n'ai plus grand-chose à "
 'vous\n'
 'dire. Embrassez bien ma petite grand-mère et mon grand-père ainsi que toute '
 'la\n'
 "famille. J'espère que Régine profite du solex et qu'elle va bronzer. Cricri.",
 "Il faut que l'on se voie avant de partir en vacances. D'accord ? Nous "
 'passons\n'
 'une semaine enchanteresse. Soleil plus bain égal ambiance parfaite. Bécot '
 '(de\n'
 'Vincennes).',
 'Bons souvenirs et bons baisers.',
 'Chères Marcelle et Agnès,\n'
 'Un petit mot pour vous donner de mes nouvelles et je pense que pour vous la\n'
 'santé est bonne pour toutes deux. Dimanche, nous sommes allés voir Claudine\n'
 'en camping. Nous y avons passé une bonne journée avec du soleil. Cela était\n'
 'agréable. Je vous quitte en vous disant toutes les bonnes amitiés. Renée.',
 'Royan le 4-8-1913\n'
 'Chère grand-mère,\n'
 "Beau temps, nous prenons des bains délicieux. J'ai été à la pêche vendredi "
 'avec\n'
 "Cousy et avons passé une bonne journée. Ce matin j'ai été aux moules avec\n"
 'papa et il faisait un peu chaud. Reçois le bon baiser de ton petit-fils qui '
 "t'aime\n"
 'tendrement. Écris-moi bientôt. Maurice',
 'Chère mémé, chère Yvonne,\n'
 'nous sommes bien arrivés ce matin après un voyage assez agréable. Il fait '
 'assez\n'
 "beau et nous avons déjà profité de l'air de la plage et du soleil cet "
 'après-midi.\n'
 'Voici notre adresse : villa Kimidien, Avenue de Nauzon, Vaux-sur-Mer, '
 "CharenteMaritime. J'espère que toutes deux allez bien. Bons baisers de tout "
 'le monde.\n'
 'Nicolas.',
 'Chers Monsieur et Madame,\n'
 'Nous passons de bonnes vacances à Royan. Le soleil a eu du mal à percer,\n'
 'et maintenant il fait beau. Les enfants font de beaux pâtés de sable, mais '
 'ne\n'
 'se baigne que les jambes, car il y a du vent. Nos amitiés. Madame Chaillié.\n'
 'Monsieur Chaillié. Grosses bises de Florence qui parle souvent de vous.',
 'Cher frère,\n'
 "Je t'envoie ce petit mot pour te prévenir que nous serons à Saint-Pierre "
 'dimanche.\n'
 "Nous serions contents de te voir, donc ce n'est pas la peine que tu montes à "
 'la\n'
 'cité. Bons baisers affectueux.',
 "Me voilà enfin en vacances ! Je vous adresse mes bonnes amitiés ainsi qu'à\n"
 'Françoise. Arlette.',
 "C'est sous un soleil éclatant que nous vous envoyons une carte. J'espère "
 'que\n'
 'vous allez bien. Quant à nous, nous passons de bonnes vacances en mangeant\n'
 'du bifteck haché tous les jours et avec un matelas pneumatique qui se '
 'dégonfle.\n'
 'Jean-Claude, Jacky.',
 'Affectueux souvenirs. Adèle Poirier.',
 'Le 24/08/89\n'
 'Chers amis,\n'
 'Voici la vue de la ville construite après 1945 que nous avons sur Royan '
 'depuis\n'
 'notre balcon. Favorisés par un temps superbe, soleil continuel, mais pas '
 'trop\n'
 'chaud, nous profitons à plein de la plage et de la mer avec Hélène qui cette '
 'année\n'
 'nage comme un poisson. Le temps passe vite en période de vacances. Nous\n'
 'serons heureux de vous revoir au début du mois prochain. Avec toute notre\n'
 'amitié. Claire.',
 'Royan, 10 juin 1939\n'
 'Ma chère cousine,\n'
 'Nous vous embrassons bien affectueusement. La santé est assez bonne pour\n'
 "l'instant et nous souhaitons qu'il en soit de même pour vous. Votre cousin "
 'adoré.\n'
 'Guy Cuffet.',
 'Royan est très jolie. Il fait beau temps et je me porte bien.',
 'Royan\n'
 'Ma chère Émilienne,\n'
 'Les transformations des rochers on fait la joie de Betty et aussi celle des '
 'petits\n'
 "Lombard auxquels ma nièce a généreusement donné une des cartes. L'envoi de\n"
 'ton mari est arrivé le jour des six ans de Betty. Elle le te remercie de '
 'tout cœur.\n'
 "Le ciel s'est enfin éclairci après une période pluvieuse et nous avons un "
 'chaud\n'
 'soleil sur la plage. Affectueusement à toi.',
 'Une bonne grosse bise.',
 "C'est avec du retard que je vous envoie cette petite carte et que je pense "
 'vous\n'
 'trouver en bonne santé. Excusez-moi de ce retard et je pense toujours à '
 'vous.\n'
 'Bons baisers. Pierrot.',
 "Nous sommes arrivés à bon port. Je t'écrirais plus longuement demain. Je "
 'vous\n'
 'embrasse de tout cœur. Marguerite chez Monsieur Georget, vingt-trois, '
 'avenue\n'
 'du parc.',
 'Bons baisers. Sophie.',
 'Bonjour de Royan. Marie.',
 'Chers parents, frère et sœur,\n'
 'La première étape est faite sans encombre et aussi sous le soleil. Pas de '
 'circulation\n'
 "jusqu'alors. Nous avons dîner et coucher à Surgères (Charente-Maritime), à\n"
 'vingt-six kilomètres de Rochefort. Un soleil formidable. Ne vous inquiétez '
 'pas,\n'
 'tout marche très bien. Ici, nous sommes passés à Royan par le bac pour une\n'
 "traversée de quarante minutes. C'est bien joli et le soleil et la chaleur "
 'sont de la\n'
 "partie. Nous pensons être en Espagne ce soir. Nous n'oublierons pas de vous\n"
 'envoyer chaque jour une carte. Soyez tranquilles, nous sommes prudents. '
 'Bons\n'
 'baisers à tous. Nous pensons bien à vous. Denise.',
 "André est un charmant enfant, quoiqu'un peu désobéissant. Je vous embrasse\n"
 'très fort.',
 "Et Guy, bientôt le retour, c'est dommage. Bien cordialement à tous. L. "
 'Disassis.',
 'Merci pour votre lettre. Nous passons de bonnes vacances avec un temps '
 'beau,\n'
 'mais orageux. Nous prenons surtout beaucoup de repos. Je pense que mes '
 'fleurs\n'
 'se portent bien. Amitiés.',
 'Ma chère rosette,\n'
 'Nous voici à Royan depuis vendredi après-midi reçus à bras ouverts par nos\n'
 "bons amis Ralier très heureux de nous revoir après tant d'années de "
 'séparation.\n'
 'Depuis ce matin, nous avons pris complètement possession du gentil studio '
 'mis à\n'
 'notre disposition et y ai fait pour la première fois la tambouille pour nous '
 'deux.\n'
 "Nous sommes prêts à aller prendre le café chez eux à deux pas d'ici. Avons "
 'eu\n'
 "très beau temps pour le voyage et le soleil brille bien ici. C'est "
 'magnifique cette\n'
 "ville de Royan ainsi que l'immense plage. Nous pensons à ta main. Guéris "
 'bien.\n'
 'Bons baisers à ta maman, et à tous. Grosses bises pour toi de nous.',
 'Nous sommes arrivés à Saint-Georges de Didonne depuis hier soir, nous avons\n'
 'enfin trouvé le soleil. Nous sommes dans une pension de famille très '
 'correcte.\n'
 'Nous vous espérons en bonne santé, inutile de nous écrire, car nous pensons\n'
 'partir lundi ou mardi. Bons baisers. Gisèle, Roger.',
 'Nous passons de bonnes vacances avec beaucoup de soleil. Recevez nos '
 'amitiés.\n'
 'Camille, Lanny.',
 'Bien chers vous deux,\n'
 'Un affectueux bonjour de courtes vacances où le soleil bande, mais où le '
 'vent et\n'
 'la pluie ne nous manquent pas. On se couche à neuf heures, on mange bien, '
 'et\n'
 'on se repose. Je vous embrasse avec André. Mimi.',
 'Pontaillac, 13 juillet 39,\n'
 'Monsieur Digeon présente ses meilleurs compliments à Monsieur Faulhaber et '
 'le\n'
 "féliciter de sa nomination, heureux d'avoir pu lui être utile. Ch. Digeon.",
 "Salut, c'est encore moi. J'espère que tu vas toujours bien, moi ça va. Petit "
 'bisou\n'
 'de Charente-Maritime où le soleil joue à cache-cache avec les nuages. David, '
 'je\n'
 'suis ton neveu. Vivien. May the Force Be With You.',
 "Nos vacances se terminent, mais il n'a pas fait beau. À très bientôt. Raoul.",
 'Nous sommes à Royan depuis lundi, Eugène étant venu nous relancer à Paris.\n'
 'De Royan nous allons à Gaillères et ensuite à Paris. René est-il toujours '
 'auprès\n'
 "de vous ? Voilà bien longtemps que je n'ai pas eu de ses nouvelles. "
 'Affectueux\n'
 'baisers. Léontine.',
 'Bien le bonjour de Royan. Madeleine.',
 'Nous avons passé quelques jours en camping au bord de la mer. Nous avons '
 'très\n'
 'beau temps. Toute la famille va bien. Nous rentrons dimanche. À bientôt. '
 'Nos\n'
 'amitiés.',
 "Passons un bon séjour, temps magnifique, et l'eau est chaude. Dommage que "
 'le\n'
 'retour est proche. Grosses bises. Nadine.\n'
 'Grosses bises de la famille Guenand qui ressort du bain. Marie-Claude.',
 'Bien amicales pensées à tous.',
 'En balade, temps superbe, amitiés.',
 'Venons de Royan. Amitiés. E. Barre.',
 'Chers parents,\n'
 "Rien qu'en regardant la carte postale, vous devinez sûrement que nous "
 'sommes\n'
 "à Royan. Nous y sommes en effet. Mais il fait mauvais temps et nous n'avons\n"
 "pas pu nous baigner. Même à Cognac d'ailleurs, il fait mauvais temps. Nous\n"
 'sommes au café des sports où nous regardons passer les gens. Je vous '
 'embrasse.\n'
 'Patrick, Jean-François.',
 "Après un bon voyage, nous voici à Royan d'où nous repartons demain matin\n"
 "pour Graullet. C'est avec regret que l'on voit à la fin des vacances, que "
 'vous nous\n'
 'avez rendues des plus agréables. Aussi, je vous en remercie bien '
 'sincèrement.\n'
 'Nous nous rappelons du bon accueil que nous avons reçu chez vous tous et '
 'des\n'
 'bons moments passés ensemble. Gabriel, Alain, Odette, vous embrassent tous\n'
 'les trois et vous renouvellent leurs amitiés. Odette.',
 'Bien cordialement à bientôt. Bonnes amitiés.',
 'Le 11/9/75\n'
 'De Royan, vous envoyons notre amical souvenir. Il fait beau et pas trop '
 'chaud\n'
 "aujourd'hui, car il a plu cette nuit, ce qui a rafraîchi un peu la "
 'température\n'
 'et nous convient mieux, car depuis dimanche il faisait très chaud. Bonjour '
 'à\n'
 'Monsieur et Madame Deboux. G.',
 "Je t'adresse mon bien sympathique souvenir de Royan où je suis pendant ces\n"
 "quatre jours de repos. Je t'assure que j'apprécie d'être à la mer d'autant "
 'plus\n'
 "que cette année j'ai eu mes vacances en avril. Quant à toi, je crois que tu "
 'as\n'
 "un petit point faible pour la montagne. J'espère que tu as profité au "
 'maximum\n'
 'de tes vacances. Et maintenant que deviens-tu ? Espérons que nous aurons\n'
 "l'occasion de nous revoir. Je t'envoie mes meilleurs baisers. Bernadette. "
 'Grand\n'
 'merci de ta carte.',
 "Nos vacances sont gâchées, mon mari est rentré à l'hôpital de Royan avec un\n"
 'épanchement de pleurésie depuis le vendredi six mars. Depuis, il va '
 'beaucoup\n'
 'mieux. Dommage, car nous avons du beau temps. Bonnes amitiés. Simone et\n'
 'les enfants.',
 'Cher Parrain,\n'
 "Nous t'envoyons cette carte qui nous espérons te fera plaisir et te trouvera "
 'en\n'
 "bonne santé. Avons du beau temps. Nous t'embrassons bien fort. Malika, "
 'Bruno.',
 'Cher Jean,\n'
 'Je te remercie sincèrement de ce que tu fais pour moi. Oui, ici je suis '
 'tranquille\n'
 "et je vivrai dans l'espérance de vous revoir l'an prochain et surtout le "
 'petit Alain.\n'
 'À tous en attendant, je vous embrasse bien fort. Tante Thérèse.',
 'Saint-Georges, 1er août,\n'
 'Nous vous envoyons tous un bon souvenir. Liliane a retrouvé des amis de '
 "l'an\n"
 "dernier et le voyage s'est très bien passé. Merci de la carte de Poissy. "
 'Meilleures\n'
 'amitiés à tous. Liliane.',
 'Chère Marguerite,\n'
 'Comme tu le sais, je fais ma première communion dimanche. Si tu veux venir\n'
 'passer la journée avec nous, tu nous feras bien plaisir. Adieu ma chère '
 'cousine.',
 'Affectueux souvenirs. Rue Rochefort, 183 bis, Royan.',
 'Cher David,\n'
 "Notre week-end royannais a été un succès en tout point. L'architecture, "
 "l'hôtel,\n"
 "les balades à vélo jusqu'à Talmont et la baignade. Amitiés. Gilles",
 'Bien arrivé. Bonne chaire. Bon gîte. Bonne ambiance. Beau temps. Tout va\n'
 'bien. R.A.S. Lucienne.',
 'Amical souvenir de vacances.',
 'Ma chère Louise,\n'
 "J'ai fait un très bon voyage. Je te prie de dire bien des choses pour nous à "
 'tous\n'
 "tes enfants ; et Victorine te prie de dire à Philibert et à Abéline qu'elle "
 'a trouvé\n'
 "les boudins très bons et qu'elle les remercie beaucoup. Il y a longtemps "
 "qu'elle\n"
 'en avait mangé. Nous vous envoyons à vous tous un bon baiser. Venez nous '
 'voir\n'
 'tous cet été. A. Gaillard.',
 '22 juin 1908\nBons baisers. Y Charpentier.',
 '22 septembre\nPrompt retour à Paris. Renée',
 'Nous passons de bonnes vacances. Nous vous espérons en bonne santé. '
 'JeanJacques, Jean-Marc.',
 'Bons baisers de tous en bonne santé. Ta mère plutôt dépassée par les '
 'événements.\n'
 'Jeannot pour tous.',
 'Affectueux baisers. Bien tendres baisers.',
 'Bon souvenir de Royan. Louise, Marie.',
 'Bon souvenir. Madame Béasse.',
 'Meilleurs souvenirs de Royan. Jeanne, François.',
 'Ma chère Juliette,\n'
 'Pas grand-chose à te dire de nouveau ; je pense que tu vas bien ainsi que '
 'maman.\n'
 "Je n'ai pu envoyer le mois à maman d'ici, je lui donnerai à mon retour. Je\n"
 'compte rentrer mercredi à Paris. Il fait toujours un beau soleil, mais un '
 'peu\n'
 "frais tout de même. Je n'ai pas vu une goutte de pluie depuis un mois que "
 'je\n'
 "suis ici. Je t'embrasse bien ainsi que maman. À bientôt. Thomas.",
 'Chers tous,\nTemps incertain. Bons baisers de toute la famille. André.',
 'Souvenirs de Royan. Camille, Raymond.',
 'Royan 23 septembre,\n'
 'Bons baisers de votre ancienne apprentie qui ne vous oubliera pas. Yvonne.',
 'Bonjour de Royan. Alice.',
 'Amical souvenir de vacances. M. Bezos.',
 'Mercredi, 22/6/94\n'
 "Qu'il est doux de ne rien faire et de se laisser bichonner. Mes généreux "
 '"donateurs" n\'auraient pas à regretter, car j\'apprécie pleinement le '
 'cadeau. Mon\n'
 '"accompagnatrice" partage les impressions ! Par contre, nous n\'aurons pas\n'
 'beaucoup à raconter sur les environs, car, à part les balades à pied en bord '
 'de\n'
 'mer, le reste du temps passe en soins et repos ! Encore merci ma grande de\n'
 "m'avoir procuré ce moment de bien-être. Plein de bises affectueuses. À "
 'bientôt.\n'
 'Jeannette.',
 'La Rochelle, 22 juillet\n'
 'Chers parents,\n'
 'Sommes arrivées hier après-midi, ici, après vingt-deux kilomètres en vélo et '
 'le\n'
 'reste en autocar. Traversée de la Gironde de la pointe de Grave à Royan en\n'
 'passant ! Nous faisons sensation dans tous les pays où nous passons. Le port '
 'de\n'
 'La Rochelle est très beau, la lumière est douce, les couleurs sont rares. On '
 'a pas\n'
 "campé cette nuit, on a couché dans un lit et on s'est bien lavé. Cet "
 'après-midi\n'
 'départ pour Nantes : camping, à Nantes, deux jours, dans le parc du lycée.\n'
 "J'espère trouver de vos nouvelles samedi soir au moins à la P. R. centrale. "
 'On\n'
 'a un appétit féroce. Je vous embrasse affectueusement. On a mangé dix-huit\n'
 'huîtres chacune en arrivant à La Rochelle. Mimi.',
 'Meilleur souvenir de vacances. M. et J. Leroy, Philippe, Claudine.',
 'Hôtel Au tigre, boulevard Clémenceau, Royan, téléphone : 61\n'
 "En vacances à Royan, profitons d'un beau temps dans une ville splendide\n"
 "reconstruite complètement à neuf. C'est une réussite. Bons baisers de tous "
 'en\n'
 'vous espérant en bonne santé.',
 'Maman,\n'
 "Je passe ce premier week-end d'août à Pontaillac, tout près de Royan. Je "
 'suis\n'
 "avec un copain. Nous logeons dans une tente prêtée par l'armée. Nous sommes\n"
 'arrivés vendredi soir et sommes rentrés sur la base lundi matin. Nous avons '
 'eu\n'
 'un temps magnifique et en avons profité pleinement même pour la nourriture.\n'
 "Nous logions dans un camping appartenant aux parents d'un pote. Nous "
 "n'avions\n"
 "donc pas de problème malgré un monde fou. Je t'embrasse.",
 "J'espère que l'Ondine vous satisfait toujours. Notre rouge à nous nous a "
 'amenés\n'
 'vers une belle région où il y a plus de soleil que de pluie. Notre bon '
 'souvenir.\n'
 'Germaine, Edith, Cathy.',
 'Sincères amitiés à vous deux. Nous avons espéré du mieux pour Michel. À\n'
 'bientôt. Michele, Rémy, Laurent.',
 'Royan, 10 juin 1925\n'
 'De Royan où je suis à passer quelques jours, je vous envoie mes meilleures '
 'amitiés.\n'
 'F. Hubert',
 'De Royan, nos amitiés. A.-M. Pigache.',
 'Royan, le 12 juin 1930,\n'
 'Ma chère jeune amie,\n'
 "J'espère que votre santé est satisfaisante, car il y a huit jours vous étiez "
 'encore\n'
 'bien fatiguée. Soignez-vous, ménagez vos forces, profitez bien de votre '
 'beau\n'
 'jardin si commode et agréable afin que ceux qui vous aiment soient heureux. '
 'De\n'
 "même pour notre Paulette qui a tant besoin d'activité. Je suis ici avec un "
 'bon\n'
 "temps et au milieu d'amis bien agréables ; je me plais beaucoup, car je suis "
 'bien\n'
 "gâtée. Je vous adresse toutes mes amitiés, ainsi qu'à votre dévoué voisin. "
 'Votre\n'
 'amie qui vous embrasse, ainsi que Paulette. N. Bergeroy.',
 "Mes chères tantes. Merci pour le beau livre que m'a apporté Simone, cadeau\n"
 'de vous toutes. Je prierai bien pour vous le jour du 15 août et vous '
 'penserez à\n'
 'moi. Encore un grand merci et bons baisers de Marie-Françoise Férot. Papa '
 'et\n'
 'maman vous embrassent bien fort.\n'
 'Merci de tout cœur à vous toutes en attendant une lettre relatant la fête du '
 '19.\n'
 'Affectueusement. Michel.',
 'Bien chère madame et monsieur Saint-Pierre,\n'
 'Avec toutes mes amitiés de vacances qui se déroulent bien vite à notre gré.\n'
 "Dommage qu'il faut rentrer, nous resterions bien volontiers ici : plage à "
 'deux cents\n'
 'mètres, repos et belles promenades. Royan est une fort belle ville. '
 'Architecture\n'
 'très moderne. Je souhaite que la reprise au bureau ne vous sera pas trop '
 'pénible\n'
 'le 1er surtout ; et que vous êtes bien reposés à Aix-les-Bains et satisfaits '
 'de votre\n'
 "baptême de l'air ! Arlette et Laurence.",
 'Chère petite Henriette,\n'
 "J'espère que vous êtes bien arrivée. Bonnes vacances. Bonnes amitiés. Mamie.",
 '28-5-1918\nAffectueux baisers. À bientôt. G. Collot, H. Collot, P. Collot.',
 "Tout va bien. Beau temps. Quelques pluies d'orage, la nuit, le jour très "
 'ensoleillé.\n'
 "J'espère que vous passez de bonnes vacances. Amitiés de tous trois à toute "
 'la\n'
 'famille. Retour dimanche, cas urgent.',
 'Tu embrasseras demain grand-mère. Je te prie pour moi. Amitiés. Ernest',
 "Pour confirmer carte d'il y a 15 jours nous faisant connaître acceptation "
 'par\n'
 'Madame Darrouzés pour location septembre. Bien à vous.',
 'Chers parents,\n'
 'Nous sommes sur la plage et pensons à vous. Bien reçu votre lettre. Avons\n'
 'dépensé pour les parasols deux milles cinq cents francs. Nous verrons-nous '
 'avec\n'
 "le soleil ? Jeudi, avec Patrick, nous allons à l'Aiguille. Sommes allés à "
 'Vallières.\n'
 'Bons baisers à tous. Jean\n'
 'Chers parents,\n'
 'Je vous joins un petit mot. Sommes sur la plage, mais il ne fait guère '
 'chaud. Je\n'
 'vous écrirais plus longuement demain. Nous allons bien. Pour Sybile, elle a '
 'la\n'
 'diarrhée. Je vous embrasse bien fort. Bons baisers. Mary.',
 'Chère Mademoiselle,\n'
 "Je suis tout à fait de votre avis : Vive la mer ! Que c'est joli ! Seulement "
 'les\n'
 'coups de soleil ne sont pas aussi beaux. Cela me fait penser à vous, mes '
 'bras\n'
 'commencent à me faire mal. Recevez mes meilleures amitiés. S. Daleux.',
 'Vacances merveilleuses. Temps superbe. Guy, Denise.',
 'Bien chère maman,\n'
 "Je t'envoie cette carte depuis l'île d'Oléron où j'ai dû aller pour "
 'affaires. Le\n'
 "pouce d'Hélène est guéri, mais tout noir. La hernie de Denis n'est pas "
 'encore\n'
 'tout à fait formée et pas encore opérable, on fera tout en même temps, mais '
 'cela\n'
 'gâche mes jours actuels. Le travail va mal. Les élections et le chantage du '
 'grand\n'
 'sifflet, les taxes lourdes sur tous les circuits, fabrication et '
 'distribution, en sont\n'
 "la cause. Nous t'embrassons tous bien affectueusement en te souhaitant de "
 'te\n'
 'remettre complètement et rapidement de ta mauvaise grippe. Robert.',
 "Je te remercie de ta carte postale que j'ai trouvée en arrivant à Royan "
 'étant\n'
 'arrivé ce matin lundi. Tâche de venir passer quelques jours ici. André '
 'Garjon.',
 "Alors que tout commence à s'organiser, il faut déjà songer à boucler les "
 'valises.\n'
 'En ce moment le soleil est plutôt timide, mais nous bronzons quand même.\n'
 'Grosses bises. À bientôt. José.',
 'Royan 22 août,\n'
 "Depuis que nous sommes ici, chère Madame, bien souvent ma pensée s'en est\n"
 "allée vers Houdan. À notre retour qui est bien proche maintenant, j'espère "
 'vous\n'
 'retrouver bien reposée étant bien secondée. Bon souvenir pour tous et les '
 'vôtres.\n'
 'Émilien',
 'Le 6 septembre 1959\n'
 'Amical souvenir. Bons baisers de Sylviane. Mademoiselle Nicolas.',
 "Ton parrain qui est en bonne santé et qui t'envoie mille baisers à toi ainsi "
 "qu'à\n"
 "tous et qui ira vous voir bientôt j'espère. Paul Raymond Médos.",
 'Le 28-8-18\n'
 'Chère maman,\n'
 "La santé est toujours bonne. Je ne sais pas quoi t'apprendre de nouveau. Le\n"
 'major vient de passer la visite. Pour moi, je suis toujours pareil. Je ne '
 'demande\n'
 "pas à sortir d'ici. Seulement, il y a si peu d'amusement que cela ne va "
 'guère à\n'
 "mon caractère. Enfin, patience. Je termine en t'embrassant bien des fois. "
 'Ton\n'
 'fils pour la vie. Maurice Decourcelle.',
 'Ma chère Nina,\n'
 'Je vous fais toutes mes excuses de ne pas vous avoir remercié des photos de\n'
 "Larmita que vous avez eu l'amabilité de m'envoyer. Je suis si absorbée par "
 'mon\n'
 "petit-fils que j'en oublie les amis. Je ne trouve pas une minute pour le "
 'leur dire,\n'
 "mais soyez assuré que vous m'avez fait le plus grand plaisir en m'offrant "
 'Larmita\n'
 'en première communiante et en interprète petit amazone. Elle est mignonne '
 'tout\n'
 'plein, mais vous savez par Alice pourquoi. Je ne vous ai pas envoyé la photo '
 'de\n'
 "Lily amour. J'attends toujours celle que Mme Jerent m'a promise pour madame\n"
 'Moscas.',
 'Chers tous,\n'
 'De Royan recevez nos bons et affectueux souvenirs. Nous vous espérons en '
 'bonne\n'
 'santé, comment va Éric ? Il doit bien se remettre de son opération. Espérons '
 'que\n'
 "maintenant tous marchent bien et que l'année prochaine vous aurez la "
 'possibilité\n'
 'de venir passer quelques jours parmi nous en Anjou. Nous vous embrassons.\n'
 'Marcel, Christian, Gaby, Hélène.',
 "Une bonne journée au bord de la mer. Il fait très beau et le soleil n'est "
 'pas trop\n'
 "dur. Ce soir, je serai à Angoulême. Ça va vite passer. J'espère que tout va "
 'bien\n'
 "à la maison. À demain soir. Je t'embrasse bien fort. Embrasse les petits "
 'pour\n'
 'moi.',
 'Amicales pensées de Royan.',
 'Mon cher Riri,\n'
 "J'espère que tu ne t'ennuies pas trop. Je regrette joliment que tu ne sois "
 'pas\n'
 "avec nous. Je m'amuse beaucoup, car j'ai retrouvé une petite amie de Rouen\n"
 'avec laquelle je joue au tennis. Je fais quelques progrès. Je viens '
 "d'envoyer mon\n"
 "album à Jules Lemaitre qui est ici pour avoir sa signature. Je t'envoie de "
 'bien\n'
 'gros baisers ma vieille.',
 'Amitiés.',
 "Or la bonne et Jeannine rencontrées l'autre jour à la Direction, dans un "
 'couloir,\n'
 "m'a dit : « Vous en faites pas, il y aura des concerts classiques, ici, cet "
 'hiver.\n'
 "Si on prend des femmes, je vous y ferai mettre, mais n'en parlez pas ». Je "
 'vois\n'
 "pourquoi il ne faut pas que j'en parle. C'est à cause de Mademoiselle "
 'Leroux, la\n'
 "violoniste qui est avec moi à la Gaieté. L'autre jour, sachant qu'elle "
 'cherchait\n'
 "quelque chose pour l'hiver de pas trop dur, à cause des répétitions à neuf "
 'heures\n'
 "du matin chez Colonne, j'en parle à Beaudu pour la Potinière si Descamps ne\n"
 "revient pas comme il en a l'idée.",
 'Mon cher Jean,\n'
 "Nous sommes arrivés à Saint-Palais depuis deux jours et j'espère bientôt "
 'voir\n'
 'mon ami. Adieu mon cher Jean. Ton ami René.',
 'Monique, Jean-Pierre,\n'
 "Souvenir de nos vacances où pour le moment, c'est très moyen. Des énormes\n"
 'averses et du soleil. Par contre, il brûle. Hier, nous sommes allés aux '
 'grottes de\n'
 "Matata et à Talmont au village artisanal. L'après-midi, il faisait beau, "
 'mais la\n'
 'vie est très chère, on profite. Gégène ça va. Le midi, on arrive à manger '
 'dehors,\n'
 'mais pas le soir, il y a du vent. Grosses bises.',
 'Meursac, le 10/7/96\n'
 'Chers tous,\n'
 'Un petit mot pour vous donner des nouvelles. La région est très belle et le '
 'soleil\n'
 'est présent tous les jours. Avec Manu, on fait du vélo, nous allons faire du '
 'tennis,\n'
 'etc. Les parents de Jean-Louis sont très gentils et marrants. Nous sommes '
 'allés\n'
 "hier à Royan et la mère est magnifique. Je ne pensais pas que c'était si "
 'beau.\n'
 'Un peu plus, la mer allait me prendre ! Bon, je vous laisse ci-joint un '
 'petit mot\n'
 'de Colette. Michael.',
 'Royan, 7-8-1925\n'
 'Nous sommes en promenade par un temps superbe. Avons pris le train à Royan\n'
 'pour Saint-Georges-de-Didonne qui est charmante. Irons à Saint-Trojan ces\n'
 'jours-ci. Luz doit être installée, elle serait bien à la Ronce avec Mimi. '
 'Bons\n'
 'baisers à tous. Marthe, Lucie.',
 "Bon souvenir de Royan. Passons d'agréables vacances ensoleillées. Bisous À\n"
 'tous. Ludovic et Odype, Nadège, Patrick.',
 "Nous sommes sur le retour de notre promenade. Le temps s'est gâté, nous "
 'avons\n'
 'retrouvé la mer et la tempête. Nous espérons que votre santé se maintient. '
 'Pour\n'
 'nous ça va. Bons baisers. Michel. Noémie. Grosses bises. Françoise.',
 'Royan 27 1908\nNos meilleures amitiés. Suzanne.',
 'Chère Madame,\n'
 "Vous serez heureuse j'en suis sûre de savoir que ma santé est complètement\n"
 "remise et si les premiers jours cela m'a un peu éprouvé en revanche elle "
 'est\n'
 "complètement rétablie. Comment va Mademoiselle Bragard ? J'espère que votre\n"
 'santé et celle de votre famille va bien et je vous prie de présenter vos '
 'salutations\n'
 'à votre mari et pour vous mon bien affectueux souvenirs.',
 'Affectueuses pensées de Royan ou le soleil est là. Mille grosses bises. '
 'Isabelle\n'
 'Laffargue (de Paris).',
 'Cher Daniel,\n'
 "Après avoir voulu t'imiter pendant le voyage aller, nous sommes quand même\n"
 'arrivé à destination avec seulement quelques heures de retard. Mais nous... '
 'Il\n'
 "n'y avait pas de pierres... Notre séjour se passe admirablement, sous un "
 'soleil\n'
 'de plomb. Nous mangeons dehors, matin et soir, et tout le monde est en '
 'forme.\n'
 'Les balades vont bon train, et hier nous sommes allés voir "Bossis". '
 "J'espère que\n"
 'tu vas bien et que le soleil est présent aussi à Rouen. Grosses bises de '
 'Isabelle,\n'
 'Patrick et Carole. Philippe et Françoise. À bientôt.',
 'Bons souvenirs. Amitiés à tous.',
 "Souvenir de vacances, le temps est beau. J'espère qu'il en est de même pour\n"
 'vous. Amicalement. Bernard.',
 'Bons souvenirs de vacances. Amitiés. Claude.',
 'Meilleurs souvenirs de vacances. Amitiés. Didier.',
 'Le 19/8/09\nBonjour de Royan.',
 "Passons d'excellentes vacances, temps pas si beau que l'année dernière, mais "
 'pas\n'
 "mauvais par comparaison à certaines régions. Les enfants s'en payent. "
 'Meilleurs\n'
 'souvenirs à tous.',
 'Bons baisers affectueux. N. J. Reny.',
 'Bon souvenir de vacances. Monsieur, madame R. Blanchard.',
 'Madame,\n'
 "Je m'excuse tout d'abord d'être partie sans pouvoir vous dire au revoir. Je "
 'vous\n'
 "envoie donc cette carte qui j'espère vous apportera en chemin un peu plus "
 'de\n'
 'soleil que celui qui brille dans cette région malgré tout si jeune et si '
 'gaie. Veuillez\n'
 'agréer mes sentiments les plus distingués. Madame Vengeon.',
 'Bon séjour à Saint-Palais. Beau temps. Bon repas. Temps très calme. Tout\n'
 "va à peu près bien pour le moment. J'espère qu'il en est de même pour vous.\n"
 'Amitiés de vous trois.',
 'Bonjour. Nous sommes très bien installés. La mer est très près et chaude : '
 '21,5°.\n'
 'Nous nous reposons beaucoup. Hier, la pêche, peu de poisson. Les vacanciers\n'
 'sont nombreux à Royan. Grosses bises. Corinne et toute la famille.',
 'Cher papa,\n'
 'Une carte en vitesse pour te fait savoir que nous sommes bien arrivés. Pas\n'
 "d'ennuis sur la route. Nous avons couchés à Saumur. Même pas fatigués.\n"
 'Sommes arrivés vers quatorze heures. À bientôt. Franck, Catherine.',
 'Royan, 13 septembre 1918\nMillions de baisers. Eugénie, Louise et Rodolphe.',
 'Bon souvenir de Royan. Amitiés.',
 'Bons souvenirs de Royan. Jacqueline, Robert, Daniel, Manon.',
 "Merci mille fois, ma chère Simone, de tes poésies. Tu es bien gentil d'avoir "
 'pensé\n'
 'à moi. Le colis a beaucoup amusé Odile. Je dors souvent et fais de '
 'ravissantes\n'
 'promenades en vélo. Baisers.',
 "Vous avez manqué. On s'amuse bien. Annette Broussin, S. Roche.",
 "Je suis bien arrivé à 19h30 avec une petite valise. Je t'embrasse et "
 "t'écrirai une\n"
 'lettre. Joy, Jean, Alain, Frédéric, François.',
 'Enfin le beau temps, mais il faut rentrer. À bientôt. Amitiés à vous tous. '
 'Michel\n'
 'Pignon.',
 'Souvenirs. Amitiée. Madame Vélise.',
 'Ici aussi le soleil est parfois rare et le vent assez fort quoique dans '
 'notre coin on\n'
 'ne le sente que très peu. Mais le repos y est toujours aussi complet. Nous '
 'pensons\n'
 'que Marceau est guéri et nous vous envoyons notre bon souvenir. Marthe.',
 "Les vacances se poursuivent normalement, c'est-à-dire très agréablement. "
 'Après\n'
 'avoir pensé au fils, nous pensons aux parents et aux malheureux "gros Bill" '
 'qui\n'
 'doit travailler ? Dacho se porte bien et profite des vacances. Grosses bises '
 'de\n'
 'nous trois en attendant le plaisir de vous revoir. Retour le trente et un. '
 'Marcel.',
 'Mes bons souvenirs. Simone.',
 '29 juillet 1913\nTemps splendide. Demeux.',
 'Nous arriverons mercredi soir ou jeudi matin. Bons baisers de nous deux. '
 'Votre\n'
 'grande.',
 'Amitiés. E. Armand',
 'Royan le 5/8/43\nPoignée de main à tous.',
 'Chère madame,\n'
 "Nous voilà arrivés à la fin de notre séjour, car c'est dimanche que nous "
 'regagnons\n'
 'Bordeaux. Les bonnes sont parties. Jean était tout à fait guéri et a pu '
 'profiter\n'
 'des derniers jours de vacances. Votre départ a laissé un grand vide dans '
 'notre\n'
 'coin. Colette cherche encore sa petite amie Christiane à la promenade du '
 'soir.\n'
 "J'espère que nous rapprochant de la Gironde, nous aurons bientôt le plaisir "
 'de\n'
 'vous revoir. Bien vives amitiés de nous deux pour vous tous et gros baisers '
 'à\n'
 'Christiane.',
 'Le 8 septembre 1938.\nUne bonne pensée. Marcel, Mariette, Louis, André.',
 "Voilà notre troisième semaine qui s'achève. Dommage, car nous avons le "
 'soleil\n'
 'avec nous, un paysage très joli et ce cher pineau ! Tout enfin pour nous '
 'faire\n'
 "oublier la capitale et le boulot surtout ! À bientôt. Le plaisir d'aller "
 'vous\n'
 'embrasser. Dolorès, Jean-Marie.',
 'Bien chers tous,\n'
 'Nous avons bien reçu votre lettre et nous vous en remercions. Nous allons\n'
 "toujours bien et pensons de votre côté, c'est de même. Nous sommes bien "
 'rentrés\n'
 'de Dordogne. Au retour, nous avons visité la distillerie Martel. On est '
 'ressorti\n'
 'avec le mal de tête à cause des odeurs. On a eu droit à la dégustation. Il '
 'fait\n'
 'toujours beau. Mes parents sont bien arrivés samedi à deux heures. Vous '
 'verriez\n'
 "Bernard sortir de la canadienne à quatre pattes, c'est un vrai spectacle. "
 'Nous\n'
 'pensons rentrer vendredi soir. Fini la grasse matinée. Nous nous baignons, '
 'mais\n'
 "l'eau est froide. Bernard demande que le bateau soit armé pour aller à la "
 'pêche\n'
 'quand il va arriver. Nous vous embrassons bien fort. Sylvie, Bernard.',
 'Chers neveux et nièces,\n'
 "Je suis à Royan depuis une semaine. Aujourd'hui, il fait beau temps. Je ne\n"
 'pense pas que je pourrais vous rendre visite, car je dois rentrer fin du '
 'mois pour\n'
 "me préparer pour la fête à Jacques. J'espère vous voir. Dans cette attente, "
 'je\n'
 'vous embrasse tous bien fort. Votre oncle Louis Charlier.',
 'Souvenir de mes vacances. H. Hasan.',
 'Peut-être vous aussi êtes-vous en vacances. Temps passable, soleil lui aussi '
 'en\n'
 'vacances. Avons déménagé. Nouvelle adresse : 298, rue L.Proust, 72 - '
 'Château\n'
 'du Loir. Grosses bises.',
 'Le 18-8-67\n'
 'Chers amis,\n'
 'Nous espérons que vous passez de bonnes vacances malgré le temps détraqué\n'
 "depuis trois jours. J'espère que tout votre petit monde va bien. Ici nous "
 'avons\n'
 "eu 10 jours radieux. Armand est reparti hier. Hélas, j'ai eu de très "
 'mauvaises\n'
 'nouvelles de chez nous. Vous allez sans doute avoir Nicole et J. J. pour les '
 'fêtes.\n'
 'Meilleurs baisers de nous deux. Paulette, Adrien.',
 'Amitiés. De Lulu, John, Suzon, Josette, Germain.',
 "Si je ne dois pas te déranger de tes projets, j'arriverai samedi matin "
 'passer la\n'
 'journée.',
 'Bon souvenir de vacances. Ici le temps est assez variable. Raymond, '
 'Henriette,\n'
 'Fabienne.',
 'Le mensonge, ce rêve pris sur le fait.',
 'Cher cousin,\n'
 'Je pense que vous serez assez indulgent pour excuser le retard que nous '
 'avons\n'
 "apporté à vous donner de nos nouvelles. Nous sommes à Royan jusqu'au\n"
 "vingt courant. Comme vous le savez, nous avons fait l'emplette du poêle qui\n"
 "d'ailleurs marche très bien. Si vous voulez que je vous règle ce que je "
 'vous\n'
 "dois, vous pourrez m'adresser le montant à Royan : villa Malou, route de La\n"
 'Tremblade. Je pense que vous êtes tous les trois en bonne santé malgré le '
 'froid\n'
 'qui continue. Marcel en ce moment est en service. Nos amitiés très sincères. '
 'Vos\n'
 'cousins.',
 'Affectueux souvenirs. À bientôt. Courage. David Liaudet.',
 'Bons baisers. Marguerite Rambert.',
 'Royan, 23 août,\nMerci, souvenirs amicals à tous. Monsieur Terville.',
 "Arrivé à bon port. Temps magnifique. Petite Micheline s'amuse bien. Bons\n"
 'baisers à tous.',
 'Le 24 août\n'
 'Chère madame,\n'
 "Je ne vous écris d'ici qu'une carte ne pouvant trouver une minute pour "
 'écrire.\n'
 "Nous sommes tous à Royan. J'ai trouvé mes petits-neveux en parfaite santé. "
 'Ils\n'
 "voudraient garder Gay jusqu'à leur départ pour faire des promenades, mais "
 'tous\n'
 "leurs arguments ne l'ont pas encore décidée. Je ne sais si elle restera. "
 'Mamie lui\n'
 "manque beaucoup. Si elle l'avait auprès d'elle, elle n'hésiterait pas.",
 "Excuse la carte. Ne pensais qu'écrire demain et comme ici il n'y en a pas, "
 'mais\n'
 "t'envoyons celle-ci. Nous te souhaitons un bon anniversaire avec nos bons "
 'baisers.\n'
 "Merci pour la carte qui m'a fait grand plaisir. Bons baisers et bon "
 'anniversaire.',
 'Bises pour Brigitte. Roger.',
 'Bons baisers de nous tous. Garde surtout le timbre, il va être oblitéré par '
 'le\n'
 'trou. Mamy.',
 'Chère Nicole et Raymond, cher Daniel,\n'
 'Un petit mot pour vous donner de nos nouvelles qui sont bonnes. Tout le '
 'monde\n'
 'est en forme. Les gamins ont bien profité de la baignade au début de notre\n'
 'séjour, car nous étions à 150 m de la mer et il pouvait y aller tout seul. '
 'Là, nous\n'
 "sommes installés à Épargnes et c'est moins facile, car nous sommes à 7 km "
 'de\n'
 'la mer et nous avons quand même à aménager un peu de terrain, car au début\n'
 "c'était le camping sauvage. Heureusement que nous sommes dans les vignes. "
 'Le\n'
 'temps est très beau.',
 'Vingt mai 24\nNous vous envoyons notre très amical et sympathique souvenir.',
 'Souvenir de Royan. Camille, Raymond.',
 'Temps idéal pour un bain. Soleil éblouissant. Laurent et Alain se baignent.\n'
 "Sandrine n'a toujours pas de dents. J'espère que Pascal en a plus qu'elle. "
 'Grosses\n'
 'bises. Chantal, Laurent, Sandrine, Alain.',
 "Nous rentrons samedi neuf dans l'après-midi. Nous avons toujours beau "
 'temps,\n'
 'nous en profitons au maximum. Christine, Alain, Antoine et Thomas.',
 "Tonton,\nIl fait beau. On s'amuse bien. À bientôt. Grosses bises. Thierry.",
 'Le 20-7-76\n'
 "Nous passons d'agréables vacances à la ferme et à la mer. Il fait assez "
 'beau.\n'
 'Nous profitons beaucoup de notre bateau. Sophie est très sage ! À bientôt. '
 'La\n'
 'joie de te revoir. Jeanne, Pascal.',
 'Temps superbe, bons souvenirs. S. Carley.',
 'Cher oncle est chère tante,\n'
 'Cette carte aurait pu vous être envoyée le 1er janvier 1900 ! '
 'Quatre-vingt-dix\n'
 'ans plus tard, elle me permet de vous souhaiter une bonne et heureuse '
 'année.\n'
 'Jean se joint à moi pour vous adresser ses affectueuses pensées. Françoise, '
 'Jean.',
 '11-9\n'
 'Calme et repos complet. Beau temps ensoleillé, pas trop chaud, mais '
 'agréable.\n'
 'Bon courage à tous et bien amicalement.\n'
 'Téléphone 388 à Royan, Golf Hôtel Pontaillac\n',
 'Nos amitiés de Royan où nous avons du beau temps et un bon repos pour les\n'
 'vacances. L. et E. Gosse.',
 'Grosses bises de Royan. Nous sommes comme des coqs en pâte ! '
 'Marie-Christine,\n'
 'Alain, Antoine et Thomas',
 'Ma chère grand-mère,\n'
 "Élise et moi pensons bien à toi et sommes heureuses à l'idée de te revoir "
 'bientôt,\n'
 'car voilà déjà quatre mois de passé depuis que tu es à Tagnon. Aussi, nous '
 'nous\n'
 "réjouissons de penser que sous peu nous pourrons t'embrasser et voir comment "
 'tu\n'
 'es installée dans ta chambre. Élise te dit que nous avons eu de bonnes '
 'nouvelles\n'
 'de Raymond. Il a dit avoir la visite le jour de la Toussaint de son camarade '
 'Ray\n'
 "Laujat. Nous avons vu Madame Bernard ce matin qui est de retour d'un voyage\n"
 'à Strasbourg dont elle est émerveillée.',
 'Ma chère petite Mimi,\n'
 "Nous pensons bien à toi, André et moi. Nous t'envoyons un gros baiser ainsi\n"
 "qu'à ton papa et ta maman.",
 "Bons baisers à vous deux ainsi qu'à vos enfants et petites filles des "
 'Charentes où\n'
 'nous passons des vacances agréables. À bientôt. Simone et Robert.',
 'Me voilà revenue à Royan où je passe un excellent séjour ensoleillé en '
 'compagnie\n'
 'de mes cousins qui ont de beaux enfants. Je vous espère tous deux en bonne\n'
 'santé et vous envoie mon meilleur souvenir. Agnès Pion.',
 'Le 21/8/78\n'
 'Salut,\n'
 "Enfin, je suis en condition pour écrire après l'agonie de mes fatigues. À "
 'Royan,\n'
 'il fait un temps très agréable, parfois un peu de vent, mais le soleil est '
 'là. À cet\n'
 'instant, Pierrette tricote, Gaëlle joue avec un petit parisien de son âge. '
 'Gaëlle\n'
 "va très bien dans l'eau. Le camp est très bien surtout depuis le 15, car il "
 'est\n'
 "moins chargé. Pour vous, j'espère que vous avez passé quelques jours "
 'agréables\n'
 'dans votre campagne.\n'
 "André et moi pensons bien à vous. J'espère que les vacances ont été "
 'bénéfiques à\n'
 "tous trois. Gaëlle est très en forme. Elle raffole de l'eau, des vagues, et "
 "s'amuse\n"
 "beaucoup. Aujourd'hui, nous avons un temps très ensoleillé. À bientôt. "
 'Grosses\n'
 'bises partagées à vous. Pierrette, Gaëlle, André.',
 "Parti hier soir à six heures d'Arcachon en yacht. Arriverons à Royan à deux\n"
 'heures. Très belle mer et bon voyage. Vous envoie mes meilleures amitiés.\n'
 'Auguste.',
 "Reçu lettre ce matin. Passerons samedi par le train de 11h46. C'est un bien\n"
 'mauvais jour et une bien mauvaise heure pour que vous puissiez venir nous\n'
 'embrasser... mais nous ne pouvons passer à un autre moment.',
 'Ma chère Marguerite,\n'
 'Je viens de passer une bonne journée à Royan. La saison ne fait que '
 'commencer,\n'
 "mais une fête de gymnastique a donné cependant bien de l'amusement. Hier "
 'soir,\n'
 "grande retraite bien illuminée et, aujourd'hui, exercices variés dont je ne "
 'me suis\n'
 "pas occupé, car je suis parti de tous les côtés de Royan : à l'est, à "
 "l'ouest et\n"
 'au sud, à Soulac, après avoir traversé la Gironde. Je ferai mon possible '
 'pour\n'
 'revenir par la chaleur en pleine saison.',
 '18/8/31\n'
 "Nous serons certainement à Metz avant le 23. Donc présent pour l'ouverture.\n"
 'Meilleurs souvenirs.',
 'Ma chère René,\n'
 'Voici quelques lignes pour te donner de nos nouvelles. Le matin, le marché ; '
 'à\n'
 "quatre heures la plage jusqu'à six heures, ensuite une promenade en ville. "
 "J'ai\n"
 'eu des nouvelles de mon frère par Maurice, il est rentré chez lui, mais il '
 'faut\n'
 "qu'il fasse des stages de temps en temps à l'hôpital. Et toi, Renée, "
 'toujours la\n'
 'mauvaise passe, je pense bien souvent à ma Nénette, je voudrais tant que sa\n'
 'santé revienne, hélas ? Lorsque tu recevras cette petite missive, nous '
 'serons\n'
 'près du départ, bien content tous les deux, car nous sommes quand même dans\n'
 "l'inquiétude. J'ai confiance en Maurice vis-à-vis de mon frère. Il lui a "
 'offert tous\n'
 'ses services. Voilà toutes les nouvelles. Je termine en embrassant ma '
 'Nénette\n'
 "de tout cœur. Fernand et moi-même t'envoyons de grosses bises. Fernand,\n"
 'Jeannette.',
 'Passons de bonnes vacances. Meilleures pensées à tous. Claude, Lucien, '
 'Christine.',
 "Jeudi, tout s'est bien passé. Tendresses. Charles.",
 "Souvenir d'une promenade à Royan. Germaine.",
 'Chers oncle, tante, cousins et cousines,\n'
 'Nous passons de bonnes vacances à la Palmyre avec un temps magnifique. Tout\n'
 "le monde se porte bien et j'espère qu'il en est de même pour tous. Bons "
 'baisers\n'
 'multipliés par nous cinq et à bientôt. Claude.',
 'Tous, nous vous envoyons nos amitiés. La mémé est heureuse des photos et '
 'vous\n'
 'remercie beaucoup. Les deux Patrick, mémé, Yolande, Juliette.',
 '16 août 76\n'
 'Chère Simone,\n'
 'Bien reçu ta carte, et espérons que tu ne penses pas trop au déménagement,\n'
 'il sera temps lorsque tu rentreras. Roland est allé voir maman à Villejuif, '
 'je\n'
 'lui avais dit de venir en septembre, que nous ayons le plaisir de les voir, '
 'mais\n'
 "il semble que lui n'a pas ce plaisir. Enfin ! Nous passons nos journées sur "
 'la\n'
 'plage, nous avons eu une journée de pluie samedi, mais le beau temps semble\n'
 'revenir. Nous espérons que tu trouveras un jour pour venir à la maison '
 'après\n'
 "ton déménagement. Plus rien ne t'empêchera de voyager. Toute la famille se\n"
 'joint à moi pour te faire la bise. Claudette, Jean-Claude, Albert.\n'
 'Bons baisers. Évelyne.',
 '14/8/75\n'
 "Les vacances s'écoulent avec le beau temps. L'hôtel, marqué d'un X sur la\n"
 "carte, est très très bien situé face à la mer, par contre la nourriture n'y "
 'est pas\n'
 'abondante. Heureusement que nous ne sommes pas de gros mangeurs ! Royan\n'
 'est très animé et les plaisirs ne manquent pas. Nous vous espérons en bonne\n'
 'santé. Bons baisers. Isabelle, Gérard.',
 'Nous sommes très bien arrivés avant-hier soir ; le temps est magnifique, '
 'soleil,\n'
 "air pur et l'appartement merveilleux. Nous y avons une vue splendide. Tout "
 'le\n'
 'monde est content, surtout papa qui se sent en liberté. Affectueusement. '
 'Nicole.',
 'Un bonjour de Royan.',
 "Simone a pris hier avec son père un bain complet. Te dire ce qu'elle était "
 'contente.\n'
 'Amitiés de toute la famille. A. L.',
 'Notre meilleur souvenir.',
 '15/6/81\n'
 'Chers amis,\n'
 'En vacances à Royan avec de la chaleur. Je pense que vous vous remettez\n'
 'de votre cure, que vous allez bien. Notre affectueux souvenir à tous les '
 'deux.\n'
 'Réjane.',
 'Chers amis,\n'
 'Nous ne vous oublions pas et Titille voudrait bien aller chez Mano prendre\n'
 "l'apéritif et mémé aussi bien entendu. Nous profitons de notre séjour, mais\n"
 'depuis samedi, il fait un vent terrible, le soleil est bon, mais alors ça '
 'souffle. Mes\n'
 'bonnes amitiés, nos bons baisers. G. Pellegrino et Hermine.',
 'Un baiser de votre petite Jacqueline. Un bonjour à Hélène et à Jeanne.',
 'Bons souvenirs de Saint-Georges-de-Didonne. A. Biziz.',
 'Royan, 19 août\n'
 'Chers amis,\n'
 'Je suis bien étourdie, voici notre adresse : 51, boulevard Samuel '
 'Champlain.\n'
 "C'est derrière le marché. Cela ne fait rien que vous veniez après le "
 'vingt-cinq.\n'
 'Nous repartons que le trente. Nous serions contents de vous avoir. Malgré '
 'le\n'
 'temps mauvais depuis quelques jours, les vacances se passent agréablement. '
 'À\n'
 "bientôt, je l'espère.",
 'Chez Monsieur Riquet, 10 bis boulevard Garnier.\n'
 'Cher vous deux,\n'
 'Vacances de petite famille tranquille : jeux pour les enfants, repos pour '
 'maman.\n'
 'Il fait tellement chaud, car certaines heures nous restons enfermés. '
 'Dormons\n'
 "presque jusqu'à 10 heures. Je me refais une petite santé. Bisous.\n"
 'Bonjour mamie Mouseau, Bonjour papy mousou,\n'
 'On est bien bronzé. Belles vacances. Grosses bises. Céline.',
 'Meilleurs souvenirs.',
 'Un gros bisou de Royan où je passe une superbe semaine. Région magnifique,\n'
 'ville très animée. Je profite au maximum des petits-enfants, surtout de '
 'Nina. Je\n'
 'pense beaucoup à vous et surtout à Laurent. À bientôt. Mamie Jo.',
 'Bien arrivé. Bons baisers. Simon.',
 'Chère madame,\n'
 'Ma mère se joint à moi pour vous remercier du colis que vous nous avez '
 'envoyé.\n'
 'Cela nous a fait bien plaisir. Sommes toujours en bonne santé. À bientôt de '
 'vos\n'
 'nouvelles. Mille baisers. Jane.',
 "J'espère que vous passez de bonnes vacances. Pour nous, cela est bientôt "
 'terminé.\n'
 'Nous avons eu assez beau temps. Affectueuses pensées. Jean-Louis, Janine,\n'
 'Blanche.',
 'Vendredi 2 septembre 1904\n'
 'Chère madame,\n'
 "Veuillez cesser de m'envoyer mon courrier à partir de dimanche. Envoyez-moi\n"
 'encore ce qui arrivera demain samedi. Bien cordiales salutations.',
 'Amitiés de Saint-Georges-de-Didonne où nous passons un agréable séjour. '
 'Grosses\n'
 'bises de la part de nous trois. Raymond, Sylvie, Marie-Louise Gaucher.',
 'Amical bonjour de La Tremblade où nous passons de bonnes vacances avec\n'
 "l'apparition du soleil. Ma fille s'en donne à cœur joie. Grosses bises. "
 'Corinne,\n'
 'Alain, Mélanie.',
 'Chère cousine,\n'
 'Je suis en vacances à Royan. Il fait très beau temps. Je suis rouge de la '
 'tête\n'
 "aux pieds par les coups de soleil. J'espère que vous allez bien. Je vous "
 'embrasse\n'
 'bien fort. Jacques.',
 'Mer très belle. Temps superbe. Royan, le 12 - 9 - 12. R. Ollague.',
 'Grosses bises de vacances où il fait beau et nous pensons bien aux '
 'courageux\n'
 'qui travaillent. Ghislaine, Michel.',
 'Un baiser au patron. Marc, Alice, Louise.',
 'Bon souvenir de vacances. Bientôt le retour. Nous espérons que tout va bien\n'
 'pour vous. Amitiés.',
 'Bons baisers de Royan. Marcelle.',
 'Royan, le 3 septembre\n'
 'Cher maire,\n'
 "J'espère que ma carte te fera plaisir. Nous allons bien. Il fait beau. "
 'Royan\n'
 'est une jolie ville toute neuve : des beaux magasins, des immeubles tous '
 'neufs\n'
 'et multicolores. Bons baisers à Denise, Bernard et ses enfants. La plage '
 'est\n'
 "immense et là où on demeure, c'est la campagne, des belles villas toutes "
 'neuves.\n'
 "Je te quitte en t'embrassant bien fort. Léon, Marie.",
 'Bonjour à tous,\n'
 "Cette petite carte pour vous envoyer un peu de soleil, car il paraît qu'il "
 'ne\n'
 'fait pas beau. Le matin et frais. Samedi pluie, mais le beau temps est '
 'revenu.\n'
 "J'espère que vous allez tous bien. Si tu étais là Aimé, tu te régalerais "
 'avec la\n'
 'pêche ; il y a de quoi. Tu peux me répondre si tu veux. Tel. 16 46 22 42 '
 '38.\n'
 'Amitiés à tous. Bernard Tosca.',
 'Amitiés sincères. R. Simoneton.',
 "J'espère que vous allez bien. Moi ça va. J'espère que zizi va bien et ne "
 "s'ennuie\n"
 'pas trop. Le beau temps persiste, nous bougeons et nous bronzons. Nous '
 'faisons\n'
 'du pédalo. Grosses bises. Florence.',
 'Septembre 1919\n'
 'Chère Mademoiselle,\n'
 "J'ai reçu avec plaisir toutes vos jolies cartes. Merci beaucoup et à "
 'bientôt.\n'
 'Paulette.',
 'Sommes bien arrivés. Fait très beau. La famille va beaucoup bronzer. Le '
 'terrain\n'
 'de camping est en plein dans le bois et les plages sont magnifiques. '
 "J'espère qu'il\n"
 'fait beau à Houlme. Nous vous embrassons. Jany, la femme de ménage, Titi,\n'
 'Styven.',
 'Meilleurs souvenirs de vacances.',
 'Amicalement. Monsieur Ponsot.',
 'Cher Richard,\n'
 'Nous sommes arrivés à Royan depuis lundi. Voici notre adresse : villa '
 'Hélène,\n'
 'Boulevard du champ de foire, à Royan. Nous espérons aller te voir un de ces\n'
 'jours. Reçois les bons baisers de la mère et de ton frère ainsi que les '
 'miens et\n'
 'ceux des petits cousins.',
 'Cher Monsieur Bertin,\n'
 'Je vous adresse mes bons souvenirs de cette jolie plage où je passe de bons\n'
 'moments avec les enfants. Malheureusement, il ne fait pas très très beau et '
 'le\n'
 'soleil se cache souvent comme à Clamart. Je vous espère en bonne santé et '
 'vous\n'
 'envoie mes toutes bonnes amitiés. Bien vôtre.',
 'Le 30 août 1931\nPromenade humide. Souvenir à tous. N. Bonneau.',
 'Monsieur Joussain,\n'
 "Je vous informe que j'ai bien reçu les deux courriers adressés. Merci "
 "d'avance.\n"
 "Notre voyage s'est bien passé, nous comptons repartir lundi. Les deux "
 'cartes\n'
 'de mon fils nous ont rassuré de le savoir en bonne santé. Dans le moment, '
 'le\n'
 'bonjour à toute la famille de notre part. À bientôt. Embrasser Pierrot pour\n'
 'nous.',
 'Affectueuses pensées de vacances. Beau temps. Vive la paresse. Janine, '
 'Lucien,\n'
 'Françoise.',
 'De gros baisers de Royan à notre petit Danou. Espérons que ton œil aille '
 'mieux.\n'
 'Fait-il beau à La Grière. Baisers de papa, maman.',
 'Nous avons un temps magnifique. Encore beaucoup de monde ici. Le temps\n'
 'passe vite : promenades, pêche, parties de cartes. Je ne me repose pas, '
 'mais\n'
 'cela me change les idées. Mes amitiés. Lucienne.',
 'Chère maman et Patrick,\n'
 'Je suis arrivé à bon port après un voyage de six heures sous la chaleur '
 'exténuante.\n'
 'De grosses bises. Thierry.',
 'Bien affectueux souvenirs. J. Bugeaut.',
 'Meilleur souvenir de Royan. Colette.',
 'Royan, 13 juillet 1932\nBons souvenirs et amitiés. A. Fleury.',
 'Chers Monsieur et Madame,\n'
 'Depuis notre agréable et bonne journée que nous avons passé ensemble, nous\n'
 'avons eu le plaisir de revoir Marie-Françoise à Tharon ; malheureusement, '
 'nous\n'
 'nous sommes quittés le lendemain. Nous passons nos vacances à Royan. '
 "Jusqu'ici\n"
 'le soleil ne nous a pas gâtés ! De ce fait, la promenade dans les pins '
 'remplace\n'
 'assez souvent la plage. Mes enfants devant être de retour le 26 août, '
 'Pierre\n'
 'et moi-même serions très heureux si vous pouviez venir passer quelques '
 'jours\n'
 "en notre compagnie jusqu'à la fin du mois, date à laquelle nous quitterons\n"
 "cette région. Dans l'attente de votre bonne réponse ; recevez chers Monsieur "
 'et\n'
 'Madame mes bonnes amitiés.',
 'Le 26 juillet\n'
 'Ma chère Alice,\n'
 "Je suis ennuyé d'apprendre que ton mari est souffrant. Enfin puisqu'il va "
 'mieux,\n'
 "cela ne va plus être qu'une question de temps. Tu seras bien gentil de me "
 'dire\n'
 "quand il sera tout à fait guéri et que tu auras plus le temps d'écrire. "
 'Nous\n'
 "serons à Saint-Martin au premier août, nous jouissons ici d'un temps "
 'splendide,\n'
 'la mer est chaude et les bains sont agréables. Bien affectueusement à toi et '
 'bons\n'
 'souvenirs à toi cher malade et aussi aux enfants. Marthe Lejeune.',
 'Bonjour à tous.',
 'Passons de bonnes vacances ensoleillées. Les promenades sont agréables '
 'ainsi\n'
 'que les bains. Pensons bien à vous les vacances arrivant. Bons baisers. '
 'Christian,\n'
 'Françoise.',
 'Bon souvenir de vacances. Louis.',
 'Phrase numéro trois cent six : "Poumons".',
 "Je passe de bonnes vacances malgré que le temps n'est pas beau. Je vous\n"
 'embrasse tous quatre. Sandrine, Stéphane, Monsieur et Madame Pigne.',
 "Beau temps, quelques pluies d'orage la nuit, mais le jour très ensoleillé. "
 "J'espère\n"
 'que vous passez de bonnes vacances. Amitiés de tous trois à toute la '
 'famille.\n'
 'Catherine.',
 '25-6\n'
 'Nous remontons tout doucement vers Paris. Encore une escale à la mer aux\n'
 "Sables-d'Olonne, puis Angers, et Blois, et enfin Paris, le 25. Dans "
 "l'ensemble,\n"
 'nous avons beau temps pour la majorité des jours. Je vous espère tous les '
 'deux\n'
 'en bonne santé. Charles se joint à moi pour vous embrasser affectueusement.\n'
 'Odette.\n'
 "Amitiés à toute la famille et à bientôt, j'espère. Charles.",
 'Juste un petit mot de Royan pour vous dire que le temps est superbe. En\n'
 "espérant qu'il en est de même à Bordeaux. Dans quinze jours les vacances "
 'seront\n'
 'terminées pour Philippe. Pour moi, je ne sais pas encore. Ici, les journées '
 'passent\n'
 "vite. Plus vite qu'à Bordeaux. Je vous embrasse ainsi qu'à pioupiou. "
 'Christine,\n'
 'Philippe.',
 'Je passe de bonnes vacances à Royan. Il fait un soleil splendide. Je vous '
 'embrasse\n'
 'tous les deux bien fort. Catherine.',
 'Respectueux hommages. M.G.',
 'Un petit bonjour de Royan où le temps est magnifique, les filles passables '
 'et les\n'
 "boites supportables. Je compte y rester jusqu'au 28 ou 29. Ensuite je "
 'reviens à\n'
 'Paris pour deux ou trois jours. Si tu veux venir pour le 14 juillet ou un '
 'week-end,\n'
 'écris-moi : Philippe Maquet, poste restante Royan 17. Salutations. Philippe.',
 'Royan, 30-9-1930\n'
 'Chère Madame,\n'
 'Nous apprenons dans le bulletin évangélique que la première réunion de '
 'monitrices\n'
 'a lieu le trois. Par suite de la maladie de Tante, nous avons retardé notre\n'
 'départ et ne pourrons y assister, mais nous tenons à assurer dès maintenant\n'
 'Monsieur Ribagnac de notre concours. Maman me charge de vous transmettre\n'
 "son bon souvenir ainsi qu'à Monsieur Ribagnac. Alice et moi vous envoyons "
 'nos\n'
 'respectueuses amitiés.',
 'Meilleurs souvenirs. R. Gauthier.',
 'Avril 28\n'
 'Chère Paulette,\n'
 "Nous aussi nous pensons à toi, petite adorée, et nous voudrions bien t'avoir "
 'avec\n'
 "nous. Nous t'envoyons beaucoup de grosses bises. Marraine.",
 "Super séjour de détente. Je me suis fait des amis. J'espère que tout va "
 'continuer\n'
 "comme cela. Je te fais un gros bisou ainsi qu'à ta fille. Bisous. Annie.",
 'Le 6/8/76\n'
 'Chère Simone,\n'
 "J'espère que la vente s'est bien passée et ton voyage aussi. Le nôtre a été "
 'sans\n'
 'histoire. Circulation fluide, parti à 5h15 de Paris, petit déjeuner à Tours '
 'à\n'
 '7h30, déjeuner à Rochefort à midi et demi, étions dans notre logis à 3h. '
 'Logis\n'
 'confortable et propre. Jean-Claude peut descendre sur la plage et se '
 'baigner.\n'
 "L'eau est bonne. Nous nous reposons, cela semble bon. Seul ennui : le "
 'marché\n'
 'est un peu loin. Mais ayant frigo, nous faisons le gros des courses en '
 'voiture.\n'
 'Albert est à la pêche, mais ça ne mord guère et il ne faut pas compter sur '
 'lui\n'
 'pour faire des économies. Grosses bises de toute la famille. Claudette.',
 'Bons souvenirs à tous. Jane.',
 'Bon souvenir.',
 'Nous espérons que tout va pour le mieux pour vous. Nous avons eu une pensée\n'
 'pour vous samedi. Souhaitons que votre maman ne souffre pas trop. Que votre\n'
 'maison ne vous donne pas trop de soucis. Nous voici de nouveau à Courlay.\n'
 'Nous y sommes très bien et avons très beau temps. Très peu de campeurs dans\n'
 "ce terrain. C'est reposant. Affectueuses pensées à tous.",
 '13 aout 1932\nBons baisers et joyeuses fêtes.',
 'Le repos continue. Tout va bien, avec un beau soleil et chaud. Bien '
 'cordialement\n'
 'à tous trois ainsi qui est votre gentille serveuse. P. Desassis.',
 'Bonne nouvelle de Royan.',
 "J'espère que vous allez bien et que vous êtes au calme. Les enfants ont-ils "
 'fait\n'
 "bon voyage ? La température est supportable et la chaleur n'est pas "
 'excessive.\n'
 'Pierre se joint à moi pour vous embrasser. Bonnes amitiés à monsieur Jules.',
 'Royan, 29 août\n'
 'Chère madame,\n'
 "Je pense que vous êtes enfin de retour et que vous avez bien pris l'air et "
 'comme\n'
 'récompense vous allez retrouver votre fin de mois pour vous remettre '
 "d'aplomb.\n"
 'Merci de vos cartes que nous avons bien reçues. Nous avons un facteur '
 "débrouillard qui a su trouver le nom de la villa sans la rue, c'est bien de "
 'vous être étourdi,\n'
 "c'est le soleil d'Italie qui a dû vous troubler les idées. Notre séjour se "
 'termine,\n'
 "envoyez plus rien ici. À Bordeaux jusqu'à jeudi prochain cinq septembre "
 'chez\n'
 'mademoiselle Bienvenu, vingt-cinq rue Adrien Bayssellance si vous voulez ; '
 'mais\n'
 "envoyer nos mandats à Tours ce sera plus sûr nous n'avons pas besoin "
 "d'argent.\n"
 "Bonnes amitiés ainsi qu'à Henri.",
 'Un merveilleux souvenir de Royan. Une copine qui pense bien à toi. Lucette.',
 'Fais bon voyage. Nous avons très beau temps, même chaud. Je vous espère\n'
 "en assez bonne santé : tant qu'à moi passable. Affectueuses pensées et bons\n"
 'souvenirs.',
 'Ma chère cousine,\n'
 'Joyeux Noël, nous vous souhaitons. Ici santé toujours la même. Nous ne '
 'pouvons\n'
 "sortir ni l'un ni l'autre, car il y a de la neige depuis mardi et il fait un "
 'froid\n'
 'de canard. Nos amitiés à votre famille. Nous nous unissons, Mamie et votre\n'
 'cousin, pour vous embrasser, bien affectueusement comme nous vous aimons.\n'
 'Votre cousin dévoué. Guy Cuffet.',
 'Bon souvenir de vacances. Ici, il fait très beau. Et à Vierzon ? À bientôt.\n'
 'Andrée, Emmanuel, Hubert.',
 'Le 7/7/71,17 heures\n'
 'Nous sommes bien arrivés, mais fatigués. Il fait trop chaud. Nous vous '
 'remercions\n'
 "pour les cadeaux et les fleurs que nous avons reçus à l'occasion de notre "
 'mariage.\n'
 "Nous vous embrassons. J'ai toujours mon briquet de mariée en vie. Il est "
 'sous\n'
 'la tente. Marie-Josée.\n'
 'La carrosserie de Marie-Jo est en train de passer au rouge. Bons baisers. '
 'Frédéric.',
 'Mon cher tonton Charles,\n'
 "Tu sais qu'Adèle m'avait écrit pour voir ce qui il y avait à louer par "
 "l'agence\n"
 'Devault. Nous avons trouvé une villa qui remplissait les conditions. Tante '
 'Louise\n'
 'était aussi de cet avis, mais il fallait répondre par télégramme. Mon oncle '
 'Alex,\n'
 "d'après les renseignements que je lui avais donnés, a jugé sans doute que "
 'cela\n'
 "irait puisqu'il a télégraphié à l'agence Devault d'arrêter. Je crois qu'ils "
 'ont bien\n'
 "fait, car il n'y a plus grand-chose dans le parc. C'est une villa dans "
 "l'avenue des\n"
 'Semis, pas très loin de la mer. Il y a un rez-de-chaussée, cuisine, salle à '
 'manger,\n'
 'salon où on mettra un lit ; au premier, trois chambres. Nous devons y '
 'retourner\n'
 "voir ce qui manque pour te le préciser. Ils t'écriront aussitôt sans doute. "
 'Mon\n'
 'oncle me dit que Juliette doit arriver mardi matin à Lonnes. Elle pourrait '
 'juste\n'
 "peut être venir ne serait-ce qu'un jour, par exemple, puisqu'on peut jouir "
 'de la\n'
 "villa où y déjeuner. Quand Jean viendra qu'il nous apporte deux torchons et\n"
 'des œufs. Il fait une chaleur excessive, mais, heureusement, il y a un peu '
 "d'air.\n"
 "Nous allons partir par le train à Saint-Georges. Je t'embrasse fort.",
 'Mon cher ami,\n'
 "j'espère que vous êtes en bonne santé ainsi que Madame Guiot et votre bébé.\n"
 'La première partie des vacances a été très réussie.',
 'Bien chers frères et sœurs et Christian,\n'
 'Nous sommes à Royan pour plusieurs jours. Après, nous partons pour Biarritz '
 'et\n'
 'là nous faisons demi-tour. Bons baisers à tous trois, et à bientôt. L. M. '
 'Cabot.',
 'Mais sincères amitiés. M. Rouxel.',
 'Recevez nos meilleures amitiés de notre séjour en Charente. Le soleil est '
 'au\n'
 'rendez-vous et les enfants profitent des joies de la plage. Monique, R. '
 'Benoît,\n'
 'Sandra et Ludovic.',
 'Chers parents,\n'
 'Vous avez dû emporter la pluie dans vos valises ; ici il fait très beau. '
 'Vous avez\n'
 'oublié les journaux de crochets, les cailloux et une barrette à maman. '
 'David.',
 'Royan, Charente inférieure\n'
 'Chers amis,\n'
 'Nous attendions de vos nouvelles avec impatience. Enfin, nous savons '
 'maintenant\n'
 'où vous êtes. Je suis sûre que Saint-Lunaire est plus frais que Royan. Ici, '
 'nous\n'
 "crevons de chaleur. Pas d'ombre nulle part. À part cela, nous sommes bien, "
 'la\n'
 "vie n'est pas chère, et les femmes sont jolies, surtout les Royannaises. "
 'Toute la\n'
 'famille Cortes va bien et nous aussi. Amitiés à tous. R. Bouraud.',
 '22/07/85\n'
 "Merci pour votre carte de vacances. C'est maintenant notre tour de vous "
 'écrire.\n'
 "Nous sommes ici jusqu'au 8 août. Il fait un temps splendide et nous en "
 'profitons\n'
 'pleinement. Nous vous embrassons bien affectueusement. Michel.\n'
 'Grosses bises à tous. Jacqueline.',
 '20/8/70\nNos meilleurs pensées et bons souvenirs.',
 'Chers tous,\n'
 'Un petit mot pour vous signaler que nous avons déménagé. Jeudi et vendredi\n'
 'nous avons eu un temps splendide, mais, par contre, samedi et dimanche, le '
 'ciel\n'
 'était plus que gris et Saint-Palais est plutôt triste à cette époque sous un '
 'tel\n'
 "ciel et aucune distraction. La ville ne vit qu'à partir du 25 juin. Nous "
 'sommes\n'
 "donc aux Sables, à l'hôtel du Théâtre, et ce matin, il fait soleil. Bons "
 'baisers.\n'
 'Chantal.',
 'Tata, tonton,\n'
 "J'espère que vous allez bien, nous ça va, il fait beau. Nous passons de "
 'bonnes\n'
 "vacances. J'espère qu'il fait beau et que vous ne vous ennuyez pas trop. "
 'Dominique.',
 'Un bonjour.',
 '5 juillet 1918\nMeilleur souvenir à tous. Hommages respectueux.',
 'Pour une quinzaine de jours dans ce coin pittoresque et encore sauvage où '
 "s'étale\n"
 'la senteur des pins sous ce beau soleil. Je profite au maximum des '
 'baignades.\n'
 "Et toi, la santé, comment ça va ? Bien, j'espère. Je t'embrasse. Michelle.",
 'Cher petit homme,\n'
 "Nous avons bien reçu ta lettre et nous sommes très surpris d'apprendre "
 "qu'il\n"
 'pleut toujours, ici il fait si beau temps toujours une chaleur plutôt '
 'étouffante.\n'
 "Nous prenons toujours de bons bains et ce n'est pas de bon cœur que nous\n"
 "voyons la fin de nos vacances arriver, surtout de rentrer à Auxy d'un temps\n"
 'pareil. Mémère se chagrine, ici elle ne ressent aucune douleur. Tu sais, '
 'elle y\n'
 'resterait bien toujours. Nous allons toujours faire notre petit tour au ciné '
 'ainsi\n'
 "qu'à la brasserie. En un mot, on ne s'en fait pas. Bien des choses chez ma "
 'tante\n'
 'Maria, et à bientôt. Bons baisers.',
 'Souvenir de vacances avec le soleil.',
 'Chère maman, chère Yvonne,\n'
 'Voilà les vacances commencées après un bon voyage sans histoire pour les '
 'deux\n'
 'voitures. Contretemps heureux, pour nous, de la dernière heure : par suite\n'
 "de la grève d'Air France, Nicole n'a pas pu partir. Elle est donc avec nous "
 'et\n'
 "ne semble pas trop déçue... Nous avons dû changer d'hôtel tellement nous\n"
 'étions mal. Nous sommes à la Paix où les chambres ont été entièrement '
 'refaites.\n'
 'Tout est pour le mieux. Nous pensons bien à vous et vous embrassons bien\n'
 'affectueusement. Robert.',
 "Chère mamie, l'Autre,\n"
 "Je t'écris de Royan où j'ai passé le mois de juillet. Il fait bon et je "
 "m'amuse bien.\n"
 "Notre immeuble est là où il y a la croix. Je pense et j'espère que l'on se "
 'verra\n'
 "chez marraine. Je t'embrasse de tout mon cœur et te souhaite une bonne "
 'santé.\n'
 'Valérie.',
 'Ola chère mémé,\n'
 'Je suis très bien arrivé à Royan avec un soleil super, une plage '
 'formidable.\n'
 "J'ai déjà pris des couleurs, je reviendrai bronzé, tu ne pourras même plus "
 'me\n'
 "reconnaître. Je mange très bien et beaucoup, car la plage donne l'appétit.\n"
 "J'espère que toi ça va et que tu te reposes bien. Bon, je te quitte et "
 "t'embrasse\n"
 'très très fort. Gros bisous. Corinne.',
 'Mille baisers. Amitiés. Marguerite. 12 juillet 1908.',
 'Royan, les Sablons\n'
 '30 août\n'
 'Chère madame,\n'
 'Monsieur Pernès nous a transmis vos bons compliments, il nous dit que vous\n'
 'allez bientôt aller passer quelques jours près de votre fils, cela vous fera '
 'du bien.\n'
 'Après une quinzaine assez belle voici le temps devenir orageux est '
 'incertain.\n'
 'Quel été ! Mon mari se joint à moi pour vous prier de recevoir en famille '
 'nos\n'
 'très amicales salutations.',
 "Royan, le 1er octobre 57. Mon petit chéri, tu es vraiment mignon de m'avoir\n"
 "écrit. Si tu savais tout le plaisir que tu m'as fait ! Sans doute "
 "aujourd'hui as-tu\n"
 "pris le chemin de l'école, j'aurais bien voulu être petit oiseau pour "
 "m'envoler vers\n"
 'toi et te voir partir avec ton petit cartable. Je suis sûre que tu '
 "t'habitueras très\n"
 'vite et que tu deviendras si sage que plus jamais tu ne seras privé de ton '
 'train. Il\n'
 'fait beau, mais froid, ta pauvre grand-mère va commencer à enfiler ses '
 'pull-overs.\n'
 "Je t'embrasse très fort, de tout mon cœur qui t'aime. Ta mémé Odette.",
 'De bons baisers toutes deux.',
 '20-6-1967\n'
 'Chers frères, je vous prie bien le bonjour de Royan, je pense de repasser '
 'pour le\n'
 "mois d'août vers le quinze. Je suis toujours avec Jules Brunet. Frère "
 'Grégoire\n'
 "s'il arrive du courrier pour moi, gardez-le. Ici, il fait très beau et je "
 'pense monter\n'
 "de l'alcool pour faire les prunes. Le vin est très bon par ici cher frère "
 'Grégoire.\n'
 'Je vous quitte, car on va casser la graine. Kuzma Joseph.',
 'Mercredi deux heures\n'
 'Cher René, Jeannette,\n'
 "On m'a remis à midi, en rentrant, votre lettre qui m'a fait plaisir. J'ai "
 'écrit\n'
 "à André. J'espère que vous avez de mes nouvelles. Vous n'avez qu'à vous les\n"
 'communiquer. Bien contente de vous savoir en bonne santé. Tu ne me dis\n'
 "pas si Henriette souffre. Toujours très contente comme je l'ai écrit à "
 'Gérard.\n'
 "Les amis sont contents d'avoir trouvé cette gentille maison. Hier "
 'après-midi\n'
 "j'étais parti seul, Alice était restée à se reposer jusqu'à cinq heures. "
 'Elle et son\n'
 "mari étaient tentés de prendre le tram pour revoir sa petite maison. C'est "
 'un\n'
 'artisan tout seul avec un manœuvre. Ils achètent des terrains pas trop '
 'chers,\n'
 'font des petites maisons avec cuisine petite, deux pièces moyennes, une '
 'salle\n'
 "d'eau avec le chauffe-eau, la douche dans la cuisine, électricité, fosse "
 'septique,\n'
 "l'évier, peintures et papiers au choix. Tout sera fait quand ils rentreront. "
 'Petit\n'
 'jardin avec clôture et grande entrée pour voiture. Il fait faire en '
 'supplément un\n'
 'garage qui lui servira de bâtiment. Ce matin, avec eux, je suis allée à la '
 'gare\n'
 'pour me renseigner des heures de train pour revenir. Pas avant une huitaine\n'
 'de jours. En revenant, nous passerons une journée à Angoulême et le soir, '
 "l'on\n"
 'reprendra le train.',
 'Souvenirs de voyage. R. Lecœur.',
 'Amical souvenir. Jeanne Murat.',
 'Bons baisers à tous de Royan. Jean-Pierre.',
 'Bonjour de Royan. M. Apcher\n'
 'Notre famille, Parc de Vallières, par Saint-Georges-de-Didonne, Charente '
 'inférieure.',
 'Mon bon souvenir et les bonnes amitiés de Royan. Catherine.',
 'Bien arrivé à Royan. Très beau temps, beaucoup de soleil. La ville de Royan\n'
 "est très jolie et bien animée jusqu'à une heure du matin. On se pavane toute "
 'la\n'
 'journée. Belle plage. Grosses bises. Nadine, Richard.',
 "Bien chers parents. L'endroit est charmant. Nous sommes à une minute de\n"
 'la plage. Le logement est gentil : petite cuisine, salle à manger et une '
 'grande\n'
 'chambre. Nous avons un petit balcon. Dany est heureuse. Déjà elle a pris '
 'bonne\n'
 "mine. Elle mange beaucoup de poissons. Le temps est splendide, j'ai déjà "
 'pris\n'
 'un bain et des coups de soleil. Nous attendons maman demain. Chers parents, '
 'je\n'
 'vous quitte en vous embrassant bien affectueusement. Votre fille qui vous '
 'aime.',
 '4e Soir Royan\n'
 'Cher Charles,\n'
 "Nous sommes arrivés après avoir eu bien chaud et, sur le matin, j'ai été "
 'bien\n'
 "contente d'avoir mon manteau, il faisait froid. Ils ont été très contents de "
 'notre\n'
 'arrivée. Le matin, je suis allée seule à la poste, me suis très bien '
 'retrouvée. Cet\n'
 'après-midi, il fait une chaleur atroce aussi nous ne sortirons que ce soir '
 'avec\n'
 'Marguerite. Elle a beaucoup de travail. André est parti avec le cousin à la\n'
 'ville. Je suis bien contente, mais je suis pas chez moi. Bons baisers de la '
 'famille.\n'
 'J. Leru.',
 'Passons de bonnes vacances ensoleillées. Bons baisers de Christine, Jack, '
 'Claudine.',
 'Bon souvenir de Royan. Dominique.',
 "En vous priant de m'excuser pour le retard que j'ai mis à répondre à votre\n"
 'aimable carte. Veuillez recevoir mon souvenir le plus amical. Théodore.',
 'Mes bons souvenirs. A. Granier.',
 'Cher papa,\n'
 'Avons fait très bon voyage. Mangé à Saint-Maixent et arrivé vers cinq '
 'heures.\n'
 'Nous travaillons à nettoyer, ranger du matin au soir. La Quatre chevaux '
 'marche\n'
 "bien. Jusqu'alors rien à signaler. Tout le monde ici t'embrasse bien fort. "
 'François.\n'
 'Mille amitiés. Sylvaine.\n'
 'Affectueusement à tous. Irène.',
 'Affectueux baisers. M. Rolot.',
 'Un bonjour de Royan où nous sommes actuellement en vacances. Bons baisers.\n'
 'Paulette, Moïse.',
 'Ma chère cousine,\n'
 "Aujourd'hui, je ne peux pas venir. Santé à part cela pas trop mauvaise.\n"
 "Mamie toujours pareille, c'est-à-dire souvent patraque, souffrante, mais "
 'toujours courageuse. Bons baisers de nous deux. Votre cousin affectueux. '
 'Dupuis.\n'
 'Plus affectueux. Vous embrasse très fort. M. Cuffet.',
 'Royan, 29 août\n'
 "Sommes ici depuis hier. N'avons pas trouvé de place à Ronce. D'ailleurs "
 'nous\n'
 'trouvons la plage de Royan plus chic. Je vous embrasse bien '
 'affectueusement.\n'
 'Hôtel du Centre, Royan, Charente Inférieure. Guy.',
 "Bons souvenirs d'agréables vacances. Amitiés. L. B. Bourillon. M. Couillard.",
 'Bon souvenir. Any.',
 'Chère Chantal,\n'
 "Je t'envoie un bonjour de Royan ainsi qu'à tes parents, tes frères et "
 'sœurs.\n'
 'Évelyne.',
 'Nos vacances se poursuivent bien et voici que nous entamons notre troisième\n'
 'semaine. Il fait beau. Il y a beaucoup de monde. Nous faisons de jolies\n'
 'promenades et nous nous reposons. Affectueux baisers.',
 'Au départ. Un baiser. Jeanne.',
 'Cher Monsieur Joussau,\n'
 "J'ai reçu votre aimable et jolie carte. Nous sommes bien contents de "
 'posséder\n'
 'se souvenir et je vous fais tous mes compliments. Embrasser bien de notre '
 'part\n'
 'Madame Joussau ainsi que le bon petit Pierrot. Nous vous serrons '
 'cordialement\n'
 'la main.',
 'Agréable séjour avec un beau soleil ! Mes amitiés et tous mes souhaits pour '
 'une\n'
 'très heureuse arrivée, à partager avec toute votre famille. Meilleurs vœux '
 'aussi\n'
 'des miens pour vous tous. À bientôt, vous revoir. E. Lieury.',
 'Nous sommes à Royan. Venons de passer une excellente journée. Avez-vous vu\n'
 "voler l'aviateur Brind Jhon qui a fait deux superbes vols. Bons départs et "
 'bons\n'
 "atterrissages. C'est merveilleux. Meilleurs souvenirs de tous. Bons baisers "
 'de\n'
 'ma part. M.B.',
 "J'espère que deux et deux font toujours quatre. Ici on pense à rien. "
 'Souvenir.\n'
 'Dany.',
 'Mes bons souvenirs. M. Durand.',
 'Chers amis,\n'
 "Je me permets de vous écrire aujourd'hui pour vous demander s'il n'y serait\n"
 'possible de venir vous voir dimanche prochain et non le quinze août, car à '
 'cette\n'
 "date nous sommes retenus sans qu'il ne soit possible de reporter cette date. "
 'Un\n'
 'silence de votre part sera considéré pour moi comme un accord tacite ; '
 "j'avais\n"
 'également visité Madame Dubois qui pense que vous ne verrez pas '
 "d'inconvénient,\n"
 "si elle est libre bien sûr, à la visiter avec vous. En cas d'urgence, vous "
 'pouvez\n'
 'téléphoner à notre voisin Monsieur Bretécher (05 25 51) ; je viens '
 "d'apprendre\n"
 'par la presse le cambriolage de chez Monsieur et Madame Douve. Quelle '
 'affaire !\n'
 'En espérant vous voir dimanche prochain, je vous adresse à tous les deux '
 'mes\n'
 'sincères amitiés. Patricia.',
 'Voyage très agréable. Ville superbe. Beau temps, mais depuis deux jours '
 'mistral.\n'
 'Nous dormons beaucoup et le repos est salutaire. À bientôt votre tour. Nous\n'
 'vous envoyons nos pensées et amitiés sincères. À bientôt. Monsieur et '
 'Madame\n'
 'Durand.',
 'Embrassant mille bien fort tous les deux. Affectueux baisers.',
 'Royan, 5 septembre\n'
 "J'ai reçu ta carte de Bordeaux et je serai bien heureuse d'avoir bientôt "
 "d'autres\n"
 'nouvelles. Bons baisers. Betty.\n'
 'Pensées affectueuses, Colette.\n'
 'Affectueux baisers, Victor.\n'
 'Gros baisers de ta petite nièce, Andrée.',
 'Une pensée de Royan. Berthe.',
 "Hôtel de l'océan, 72 boulevard Botton, 17 Royan\n"
 'Lundi 5 avril 1971\n'
 'Chère madame,\n'
 'Bien arrivé à 12h30 par un temps superbe tout à fait inespéré. Nous étions\n'
 'hier soir à Vendôme à 19h30. Tout va donc pour le mieux. Sur cette carte, '
 'tu\n'
 'as une vue du port de plaisance, du casino où nous jouerons (la rotonde) '
 'est\n'
 'derrière notre hôtel avec flèche sur la fenêtre de notre chambre. Plus en '
 'arrière,\n'
 "la cathédrale en ciment. Bons baisers ainsi qu'à Berthe.\n"
 'Bons baisers. Martine, Robert.',
 "Où sont les belles cathédrales d'antan ? Juan.\n"
 "Où sont aussi les beaux étés d'antan ? Claudie.",
 'Grosses bises à tous les deux de Royan. Il fait très beau et tout le monde '
 'profite\n'
 'à merveille de ce petit séjour. Zaza et bébé sont en pleine forme. Beaucoup '
 'de\n'
 'balades aussi. Nous espérons que Raymond se remet bien de son petit '
 'accident.\n'
 'Grosses bises, à bientôt. Françoise, Philippe, Patrick et Zaza.',
 'Bon souvenir de vacances. Denise.',
 'Chers amis,\n'
 'Nous descendions sur la côte basque, mais deux jours de pluie sans une '
 'seule\n'
 'éclaircie ont eu une raison de notre patience et nous remontons au Sable '
 'via\n'
 "Royan. Nous espérons qu'il fera meilleur quand ce sera votre tour. Amitiés.\n"
 'Roger.',
 'Ma chère maman,\n'
 "Il faut songer au retour et tu préviendras Rosa de m'envoyer chercher par\n"
 'Monsieur Guerain au train qui arrive à 3h22. Il est entendu que je prolonge\n'
 "jusqu'à lundi. Bons baisers.",
 'Nous voici bien arrivés à Royan. Nous sommes arrivés hier soir à six '
 'heures.\n'
 "Il fait un temps splendide. Le camp est très bien, c'est à "
 'Saint-Palais-sur-mer\n'
 "au-dessus de Royan. C'est vraiment joli. Enfin, ma petite Ninique, j'espère "
 'que\n'
 "pour Armelle ça va. Pour la clinique, voici l'adresse : camping les "
 'mimosas,\n'
 'Saint-Palais-sur-mer, Charente-Maritime, 17. Grosses bises, ma petite '
 'Aline.\n'
 'Nous vous embrassons bien fort. Mado, Michel, Anaïs, Domi.',
 '1er novembre 1904\n'
 'Sommes heureux de savoir que vous avez fait bon voyage et que vous soyez en\n'
 'bonne santé. Espérons que Théo sera content de son nouveau poste ; '
 "habiterezvous l'hôpital ? Ici ça va bien. Louis est très fatigué et il a été "
 'enrhumé. On le\n'
 'soigne ; espérons que ça ira mieux ; il aurait besoin de se reposer quelque '
 'temps.\n'
 'Maman est repartie le 28 ; elle voulait être pour la Toussaint à Paris. Elle '
 'était\n'
 "contente d'avoir passé un moment avec nous. Marcel se met au courant ; ça\n"
 "a l'air de marcher. Tante Eulalie est à Paris chez Henriette depuis 18 "
 'jours\n'
 'pendant que son mari fait ses 28 jours au Mans. Chez Paul, ils vont bien ; '
 'petit\n'
 'Louis à deux dents ; il va bien. Nous nous unissons pour vous embrasser tous '
 'de\n'
 'cœur. De gros bisous aux enfants pour Maurice.\n'
 'Jeanne',
 'À bientôt. Hermann.',
 'Chers Ninique, Jaquot, Aline, Armelle,\n'
 'Petite carte pour vous donner de nos nouvelles qui sont toujours très '
 'bonnes.\n'
 "Toujours un temps merveilleux. On crève de chaleur. À l'eau tous les jours.\n"
 'Tu verrais maman, maintenant, ça lui plaît. Enfin, le coin est magnifique. '
 'Ma\n'
 "petite Ninique, j'espère que pour vous ça va et que Mémelle est revenu avec "
 'vous\n'
 "et que tout se passe très bien. Enfin, je vous quitte pour aujourd'hui en "
 'vous\n'
 'embrassant tous bien fort ainsi que Michel et Dominique qui sont en train '
 'de\n'
 'faire une partie de raquette. Michel, maman, Domi, Mado.',
 'Chère madame,\n'
 'Nous vous adressons nos meilleurs souvenirs de nos vacances. Il fait un '
 'temps\n'
 'merveilleux, mais beaucoup de vent, même de la tempête. Nous devons rester '
 'à\n'
 "l'ombre tout l'après-midi tellement nous sommes brûlés par le soleil. Nous "
 'ne\n'
 'pouvons pas dormir tellement que cela nous brûle. Nous allons à la mer que\n'
 'vers sept heures trente. Merci pour votre souhait pour la fête des Mères. '
 'Nos\n'
 'pensées bien à tous. Michelle, Daniel.',
 '3/9/68\n'
 'Chère madame,\n'
 'Nous voici depuis dimanche soir à Royan sous la pluie qui ne cesse pas '
 'depuis\n'
 'trois jours. Aussi, si cela continue, nous rentrons dimanche et pour moi, '
 'je\n'
 "puis vous le dire, c'est la dernière fois. Vive mon chez-moi enfumé. Dans "
 'cette\n'
 "chambre, c'est triste, ciel gris, etc. Et Denise d'une nervosité incroyable "
 '? Elle\n'
 'se joint à moi pour vous faire une grosse bise, en vous disant à bientôt '
 "j'espère.",
 'Fouras, 19 août 1936\n'
 'Mes chers petits amis,\n'
 "Merci beaucoup pour les excellentes dragées que j'ai reçues hier. J'espère "
 'que\n'
 "Mme Vachon se remet de son abcès, car c'est quelque chose qui fait bien "
 'souffrir,\n'
 "paraît-il ! Compliment à Christiane pour sa sagesse, je pense qu'elle "
 'continuera\n'
 'ainsi. Je compte trouver en arrivant à Casa la petite photo annoncée. '
 'Depuis\n'
 'mon arrivée à Fouras, le temps est à peu près beau. Mais ayant trouvé mon '
 'père\n'
 'malade, cela a gâté mon séjour ici. Enfin, il y a du mieux et je repartirai '
 'plus\n'
 'tranquille. Dans dix jours, je serai à mon foyer. Bons baisers amicaux à '
 'tous les\n'
 'trois. B. Carruy.',
 'Chère Madame,\n'
 'Je suis étonnée de ne pas avoir reçu mon costume pour le quatorze comme '
 'vous\n'
 "l'aviez dit, donc je pense le recevoir dans le courant de la semaine "
 'prochaine, car\n'
 "j'en suis très pressé. Sommes toujours en bonne santé. Mille baisers. Jane.",
 'Bien arrivé avec du beau temps. Nous avons déjà pris des coups de soleil,\n'
 'nous avons même trop chaud, un orage cette nuit, et le beau temps est '
 'revenu,\n'
 'espérons pour vous ce sera pareil. Pensons que Grégory va bien. Nous vous\n'
 'souhaitons de bonnes vacances à tous les trois. Éric et les filles '
 'embrassent Titi.\n'
 'Éric en profite. On ne le voit pas beaucoup. Enfin, tout le monde se '
 'repose.\n'
 'Recevez notre meilleur souvenir. J. Lemercier. Grosses bises à Titi.',
 'Chère Évelyne,\n'
 '"Pauvre Évelyne" (tu vois je m\'attendris sur ton sort) quand j\'ai reçu ta '
 'lettre\n'
 "j'étais en stage et n'avait pas le temps décrire et, depuis, j'ai attendu... "
 'je pense\n'
 "aussi que tu n'es plus sur l'île de Wight. Tu es peut-être sur un bord de "
 'mer\n'
 'français ou étranger. Pour ma part, je suis à Royan depuis début août. Le '
 'temps\n'
 "est beau depuis plus d'une semaine et c'est déjà pas mal même si on ne "
 "s'amuse\n"
 "pas toujours très bien. En t'envoyant donc un rayon de soleil, je te "
 'souhaite une\n'
 'bonne fin de vacances. Marcel.',
 "À Royan pour quelques jours, nous t'envoyons nos meilleures amitiés. Nous\n"
 'remontrons bientôt à Valpuiseaux. Bons baisers. Claude Gilbert.',
 'Amical souvenir de vacances. À bientôt. Amitiés. M. Ibrahimi. Mon bon\n'
 "souvenir à Madame Laudelle. Je n'ai pas son adresse.",
 '2/9/54\n'
 'Chers amis,\n'
 "Nous vous remercions tardivement de vos aimables cartes de la Côte d'Azur. "
 'Elles\n'
 'nous ont représenté le splendide voyage que vous avez réalisé. Nous '
 'terminons\n'
 "un court séjour à Mauzan avant la reprise du travail qui s'avance "
 'rapidement.\n'
 'Nous vous disons bonne fin de vacances, bonne rentrée en vous adressant nos\n'
 'amitiés et notre bon souvenir.',
 'Saint-Sulpice de Royan, 7 mai 1913.\n'
 'Chère Elizabeth,\n'
 "Je vous envoie mes meilleures amitiés ainsi qu'à tous les vôtres. Je suis à "
 "SaintSulpice depuis une semaine. Je remplace l'instituteur jusqu'à samedi "
 'prochain.\n'
 "Aurais-je le plaisir de vous voir à Vaux bientôt. Je l'espère. Votre toute "
 'dévouée.\n'
 'L.',
 'Chers amis,\n'
 'Ne ne comptons pas encore rentrer avant la fin de la semaine, car ils ne '
 'veulent\n'
 'pas encore la laisser sortir. Nous commençons à nous ennuyer de la '
 'Normandie.\n'
 'Gros baisers de toute la famille.',
 'Amitiés de vacances et bons baisers fleuris de soleil. À bientôt. R.J. '
 'Balieu.',
 'Chers amis,\n'
 'Je déplore de ne pas vous avoir vu avant mon départ, mais ayant trouvé '
 'trois\n'
 "fois porte close, j'ai pensé que Mme Bescha était à Sandgate, sachant que "
 'Mimi\n'
 'devait aller en Angleterre. À ce propos, je la remercie bien de sa carte. '
 'Nous\n'
 'avons trouvé toute la famille à notre arrivée : avec tante, cousins de '
 'Gaillac.\n'
 "Ils s'en vont demain. Mes parents ne sont pas trop mal physiquement bien\n"
 "que maman soit fatiguée et ait un moral déplorable. Le temps n'est pas "
 'aussi\n'
 "beau que l'année dernière, mais permet aux enfants de se baigner et d'avoir\n"
 "bon appétit, Dominique moins, mais ils ont de l'espace pour se détendre. "
 'Bons\n'
 'baisers à tous les deux de la part de tout le monde. Lucette.',
 'Chère Elodie,\n'
 "J'espère que cette carte te trouvera en bonne santé. Nous nous allons bien, "
 'mais\n'
 "c'est le temps qui ne va pas, ce n'est pas comme l'année dernière. J'ai eu "
 'deux\n'
 'visites hier : Émile et Georges avec Jeanne. Ils sont repartis ce matin '
 'avec\n'
 'Maurice. Jeanne passera huit jours et nous irons avec elle deux jours à '
 'Bordeaux\n'
 'de Saint-Georges. Jeanne.',
 'Bien le bonjour à tous et à bientôt. Madame Édeline.',
 'Bons baisers de mémé Jeannette de Rigidi. Pépé Mémé. Jeff.',
 'Ma chère Françoise,\n'
 'Nous vous adressons notre meilleur souvenir de vacances. Elles sont '
 'ensoleillées et\n'
 "nous espérons qu'il fera aussi chaud quand votre retour sera venu. "
 'Amicalement.',
 'Royan, 13/8/25\nPensée affectueuse. Bons baisers. Marcelle.',
 'Plage où nous sommes allés hier et qui est superbe. Amitiés à toute la '
 'famille et\n'
 'un bon baiser pour toi. A. L.',
 '27 août\n'
 'Chers amis,\n'
 'Nous voilà bien arrivés. On a rien perdu en route. On a été pas mal secoué. '
 'Il\n'
 'fait beau et chez vous aussi sans doute. Genie et Georges, ils étaient bien '
 'sages.\n'
 'À bientôt de vos nouvelles. Bien affectueusement à vous.',
 "Souvenir de Royan. J'ai rencontré le sous-chef de gare Monsieur Nauvet qui "
 'est\n'
 'détaché lui aussi.',
 'Le 12/8/70\n'
 'Chers parents,\n'
 'Tout va toujours pour le mieux à notre campement, nous profitons au maximum\n'
 'du soleil ; baignade, promenade, sieste sont nos principales activités. Tous '
 'les\n'
 'matins je vais au marché tandis que Jean-Claude essaie de pécher ! Car le\n'
 "poisson est rare et jusqu'à présent rien a été pris. À la fin de la semaine "
 'dernière,\n'
 'il y a eu un violent orage sur Royan ; mais nous sommes sur la hauteur et '
 'nous\n'
 "n'avons eu aucun dégât. C'est en espérant que vous êtes en bonne santé et "
 'que\n'
 "le temps est aussi beau en Normandie qu'ici que je termine en vous envoyant\n"
 'nos meilleurs baisers. Marie-Françoise. PS : Bonjour aux Marseillais.',
 'Royan, hôtel de Bayonne, 19 08 30.\n'
 "J'espère que vous allez bien tous deux. Nous avons relativement beau temps\n"
 'et passons des vacances très agréables. Nous avons recommencé à prendre des\n'
 'bains et... attraper des coups de soleil ! Bons baisers.',
 "Souvenir d'un passage à Royan.",
 'Partirai demain. Bonjour. Suzon.',
 "Le temps est assez beau, particulièrement aujourd'hui. À bientôt. Johanne.",
 'Mon cher petit Olivier,\n'
 'Je pense que tu passes de bonnes vacances avec tes frères, que vous vous '
 'amusez\n'
 "bien. J'ai confiance en toi pour arranger ça, mais hélas il ne fait pas beau "
 'pour\n'
 'jouer dehors. Grosses bises. À bientôt. Mémé Jeannette.',
 'Cordiale poignée de main. André Gueffier.',
 'Chère mademoiselle,\n'
 'Merci pour votre gentille carte, et nous espérons que vous avez passé un '
 'agréable\n'
 'séjour. À notre tour, nous vous envoyons nos meilleurs souvenirs de '
 'vacances.\n'
 "Les enfants profitent bien de l'air, de la mer et le tout petit supporte "
 'parfaitement\n'
 "le climat. Nous n'avons pas trop chaud ici et nous avons de temps à autre de "
 'la\n'
 'pluie pour nous rafraîchir. Toute la famille se joint à moi pour vous '
 'envoyer nos\n'
 'affectueuses pensées. Michel Schoeb.',
 'Royan, le 29 août\n'
 'Merci pour votre carte plutôt tardive, mais enfin... Dépêchez-vous de venir '
 'à\n'
 'Royan. César est arrivé ce matin et, croyant que nous étions partis, voulait '
 'déjà\n'
 "s'en aller. Nous vous attendons avec impatience. Il fait un temps superbe. "
 'En\n'
 'attendant le plaisir de vous voir. Meilleures amitiés. Alice',
 "En promenade sur Arces où j'attends chez le coiffeur. Je profite pour vous "
 'dire\n'
 'un petit bonjour. Il fait beau, mais hier soir et toute la nuit, nous avons '
 'eu une\n'
 'pluie diluvienne, mais ici le beau temps revient vite. Bien cordialement. '
 'Amitiés\n'
 'à Raymond, Nicole. Mémé.',
 'Jeudi soir\n'
 "Je pars pour Paris. Rentrerai après-demain. Je t'admirerai lundi. Article "
 'sur\n'
 'voyageurs URSS pour Tribune. Merci tes bonnes lettres. Amitiés. Villa Les\n'
 'Ross, Ronce-les-Bains.',
 'Le 8/5/34\n'
 'Nous avons fait un bon voyage, mais sommes un peu fatigués. Négociations\n'
 'laborieuses, avons pas encore terminé. Royan très joli.',
 'Samedi, 24/9/60\n'
 'Mon gros loulou ex Corsico chérie,\n'
 "Tu peux juger de notre surprise et de notre joie en recevant aujourd'hui la "
 'seule\n'
 'et unique missive depuis notre arrivée ici. Quinze jours déjà demain. Il est '
 'à\n'
 "se demander ce qui a pu se produire d'un côté comme de l'autre puisque tu "
 'ne\n'
 'recevais rien non plus. Heureusement, nous avions de tes nouvelles par tes '
 'sœurs\n'
 'sans quoi nous nous serions crus abandonnés. Tu vois là donc sur le chemin\n'
 'du retour. Tu seras sans doute très fatigué en arrivant et le soir tu auras '
 'hâte\n'
 'de trouver ton lit que tu partageras avec ton compagnon habituel qui ne '
 'sera\n'
 'pas fâché de retrouver son tyran. Royan est une très belle ville qui nous '
 'plaît\n'
 'beaucoup et où nous avons retrouvé un soleil merveilleux qui me ravigote un\n'
 'peu. Pour le reste, aucun changement. Mille baisers à vous partager. Mamy.',
 '9 juillet\n'
 'Madame,\n'
 'Je termine une cure à Saujon, Charente-Maritime. Ma famille qui est restée\n'
 "jusqu'à présent à Arles a hâte d'aller à la fraîcheur et je déciderai sans "
 'doute\n'
 "pour La Bourboule où j'irai également passer quelques jours. Le Mont-Dore "
 'ne\n'
 'vous attire-t-il pas cette année ? Je serais trop heureuse si vous pouviez '
 'mettre\n'
 "L'Auvergne dans vos projets d'été. Agréez mes très respectueuses "
 'salutations.',
 'Il faut que Madame Maquinet te fasse deux paires de socquettes.\n'
 'Ma chère petite Lydie,\n'
 "Je pense que tu as déjà reçu ma carte, tu les garderas en souvenir ; j'en ai "
 'acheté\n'
 'deux très bien en couleur, mais elles sont trop grandes pour les enveloppes '
 "; j'ai\n"
 'aussi acheté aux galeries deux écheveaux de soie très jolis comme teinte '
 'rouge\n'
 'vif et bleu. Aussi, une boîte de Ripolin bleu moyen pour ta bicyclette et '
 'aussi\n'
 'pour la table du gaz. Je te les ferais bien parvenir, mais il faut aller à '
 'la poste\n'
 "à Pontaillac et ce n'est pas ici. Je pense que tu t'arranges toujours bien "
 'pour\n'
 'la cuisine ; il faudrait demander à Madame Moreau si tu as du raccommodage\n'
 'soit chaussettes ou autres et lui demander de laver quelques serviettes pour '
 'vous\n'
 'aider. Elle ne vous refuserait pas. Recevez pour tous mes meilleurs baisers '
 'et\n'
 'embrassez toute la famille : Jean, Jules, Jack, Yvonne. Ta maman.',
 '19-9-61\n'
 'Chère madame,\n'
 "Bon souvenir d'un Royan bien ensoleillé depuis huit jours que vous ne "
 'reconnaîtriez plus. Pense que les Bernard très pris ne passeront pas chez '
 'moi. Si\n'
 "vous les voyez, voudriez-vous les prier de mettre mes clés d'appartement "
 'dans\n'
 "ma boîte aux lettres après s'être assurés qu'elle est bien fermée pour que "
 'je les\n'
 "trouve à mon retour, car je n'en ai pas d'autres. Vous souhaitant meilleur "
 'temps\n'
 "que celui d'ici, et avec mes bonnes salutations.",
 "Nous sommes bien arrivés. Nous avons un temps merveilleux. J'espère que "
 "c'est\n"
 'de même chez vous. Nos amitiés. Serge, Simone et les enfants.',
 "Passons d'agréables vacances avec le soleil. Il fait très très chaud. La "
 'mère est\n'
 'belle et chaude. Le coin est splendide. Grosses bises. Patrick, Françoise, '
 'les\n'
 'filles.',
 "Nous avons eu hier une mer entièrement démontée et c'était très curieux à "
 'voir\n'
 'du haut de ces rochers. Amitiés de toute la famille à tous et un bon baiser '
 'pour\n'
 'toi. B. B.',
 'Chers tous,\n'
 "Je n'ai pas encore eu de vos nouvelles. J'espère que vous allez tous bien et "
 'que\n'
 "vous avez du beau temps. Pour nous, c'est fini, nous avons de la pluie "
 'depuis\n'
 "dimanche. Ce n'est pas gai. Si le temps continue, nous rentrerons samedi. "
 'Ma\n'
 'petite grand-mère quand je rentrerai, je te donnerai un petit billet pour '
 'finir tes\n'
 "vacances. J'espère que grand-père vient souvent vous voir. Tu l'embrasseras "
 'pour\n'
 'moi. Mon petit Daniel, je pense que tu as nettoyé mon Solex sinon Michel te\n'
 'fera encore des prises de judo en revenant de vacances. Ma petite mère, '
 "j'espère\n"
 "que tu passes de bonnes vacances et que tous les caractères s'accordent "
 'bien. En\n'
 'attendant de nous revoir, nous vous embrassons tous. Christiane, Michel.',
 'Amitiés. Édouard Armand.',
 'Chère cousine et cousin,\n'
 "Un bonjour de Royan où je passe d'agréables vacances dans une très belle "
 'région\n'
 'où il fait très bon. Attention particulière pour mon cousin : on y boit de '
 'bons\n'
 "petits vins de Bordeaux et on n'y mange des bons petits plats. Je suis en "
 'train\n'
 "de vous mettre l'eau à la bouche. Au fait, Thérèse, j'espère que tu viens à "
 'irons\n'
 "avec Laurent au 15 août, car j'ai trois jours de congé. En ne fleursvoyant "
 'plus\n'
 'grand-chose à vous dire, je vous embrasse bien fort tous les trois, en '
 'espérant\n'
 'vous voir bientôt. Bernard.',
 'Amical souvenir de Royan, et merci pour le courrier.',
 'Chers amis,\n'
 "Je pense beaucoup à vous, mais pour écrire, c'est autre chose. Pour faire "
 'la\n'
 "saison, j'ai pris plus de quinze mille francs de médicaments, plus le "
 'docteur.\n'
 'Enfin, la saison est finie comme vous dites, la location aussi. Mais tout '
 "payé, c'est\n"
 "la première année où il nous restera si peu, l'hiver à passer, et quatre "
 'acheteurs\n'
 "à attendre, car la vie est de plus en plus dur et j'ai pas mal de "
 'raccommodage\n'
 'et couture de vêtements à retourner et à arranger. Enfin, si on a la santé, '
 'on a\n'
 "le bien le plus précieux de la triste vie. Chers amis, j'espère que vous ne "
 "m'en\n"
 "voudrez pas et que vous me répondrez bien vite, car on est heureux d'avoir "
 'des\n'
 'amis qui pensent à vous. Nous vous embrassons de tout cœur. Colette, Odette.',
 "Mon cher ami. Notre chère Titine n'y tenant plus d'être trop tranquille, je "
 'serai\n'
 "à Périgueux demain 6 juin à 8 h soir où j'aurai le plaisir d'embrasser toute "
 'la\n'
 'famille, principalement cette bonne Nini. Bien des choses de toute la '
 'famille.\n'
 'Frédéric.',
 "Bon souvenir de vacances. Il fait beau, nous passons d'agréables vacances. "
 'Nous\n'
 'vous embrassons. À bientôt. Marcel, Albertine.',
 'Ma cher pompon,\n'
 "J'arrive à Royan où j'ai trouvé ton mas. Il m'a fait bien plaisir. Royan est "
 'très\n'
 'jolie. Je vais aller à Vallières. La maison est épatante.',
 'Chers cousins cousines et grand-mère,\n'
 'Bon souvenir de vacances. Un très beau temps. Janine.',
 '28-8-32\n'
 'Chère Babette,\n'
 "Mon patron m'ayant envoyé en voyage à moitié chemin d'ici, j'en ai profité "
 'pour\n'
 'venir connaître Royan à meilleur moment. Il me tarde bien de vous voir '
 'là-bas.\n'
 "Michel ne va plus me reconnaître. J'espère que ces longues et belles "
 'vacances\n'
 "vous auront fait beaucoup de bien à tous et qu'elles sont réussies. "
 'Germaine.',
 'Cher papa,\n'
 "Je viens de recevoir ta lettre et t'envoie cette petite carte. Je suis "
 'toujours en\n'
 'bonne santé. Je dors bien et mange bien. Je pense aller te voir bientôt. Ta\n'
 "petite fille qui t'aime bien. JoJo.",
 '18 -8-38\nBon souvenir de vacances. Suzanne.',
 '18 août 1919,\n'
 'Chère tante et mon cher Joseph,\n'
 "J'ai fait un bon voyage, un peu long. Je suis arrivée à neuf heures et "
 'quelques.\n'
 'Ces dames à la gare sont heureuses de me recevoir. Madame au lit très '
 'souffrante.\n'
 'Mille baisers. Mes amitiés à Jeanne.',
 '16 avril 1928\n'
 'Poulette chérie,\n'
 'Il nous tarde de bien de t\'embrasser et de te faire des "coquineries". À '
 'bientôt\n'
 'et le plein de casino de "bises" en attendant.',
 "Un bon baiser d'une sortie aux environs de Rochefort. Votre fils. Robert. "
 'Un\n'
 'amical bonjour. Émile.',
 'Amitiés, bon courage. Gayont.',
 'De passage à Royan, une pensée à ceux qui sont au boulot. Prenez courage !\n'
 'Bientôt le retour. Bonnes vacances à ceux qui vont partir.',
 'Bon souvenir de vacances. Ici, il fait très beau, et à Vierzon ? À bientôt. '
 'Andrée\n'
 'Emmanuel Hubert.',
 'Le 20 août\n'
 "Avec nos tendres baisers, ma grande sœur chérie. Un petit souvenir t'attend\n"
 'à Courbevoie. Merci de ta carte, nous espérons que tu as beau temps comme\n'
 "nous. Patrick et Marc (ce dernier aime l'eau jusqu'au cou) et nous profitons "
 'de\n'
 'la plage. Gros gros baisers. Ta petite sœur.',
 'Royan, 22 août 1908\nÀ demain à Cognac. Oscar.',
 'Bonne pensée de Royan. Marie Hannin.',
 "Amitiés sincères d'une petite amie. Juliette Jorget.",
 'Cher vous deux,\n'
 'Je vous écris comme prévu pour vous donner de nos nouvelles. Nous sommes\n'
 'arrivés jeudi à 15h30 sous une chaleur accablante qui persiste toujours. '
 "J'espère\n"
 'que vous êtes bien arrivés et que vous vous reposez bien. Nous allons '
 'JeanFrançois et moi à la plage presque tous les jours pendant que mes '
 'parents se\n'
 "reposent. Au fait, j'espère que Tata a été reçue à son examen dont elle "
 'espérait\n'
 'beaucoup. En attendant une petite carte de vous, mes parents et J.-F. se '
 'joignent\n'
 'à moi pour vous embrasser tous les deux bien fort en vous disant au mois de\n'
 'septembre. Marie-Hélène.',
 'Le 3/11/82\n'
 'Cher Olivier,\n'
 "Nous sommes très contents d'avoir reçu une lettre de toi et de voir que tu "
 'te\n'
 "défends pas mal à la machine à écrire. J'espère que tu as passé de bonnes "
 'et\n'
 "belles vacances avec ce beau soleil. Ici c'est la marée, la mer est haute, "
 'mais\n'
 'calme, les gens se baignent, le sable est chaud, la planche à voile a du '
 'succès\n'
 'ainsi que les détecteurs de bijoux, ce qui est très à la mode en ce moment. '
 'Enfin\n'
 "te voilà maintenant motocycliste, tu pourras aller jusqu'à l'école. Tu dois "
 'être\n'
 "content. Bonjour aux parents ainsi qu'à chat. Bonne santé, bons baisers. "
 'Marie.\n'
 'Bons baisers à tous. Mémé Jeannette.',
 'Chers parents, chère Yvonne,\n'
 'Bien reçu votre dernière lettre du sept qui nous apporte de bonnes '
 'nouvelles.\n'
 "Nous partirons d'ici mercredi après-midi, coucherons en route et arriverons "
 'à\n'
 'Rouen jeudi en fin de journée. Le temps est merveilleux depuis le 1er '
 'septembre\n'
 'et nous profitons pleinement de nos derniers jours de grand air. Bien des '
 'gens\n'
 'pensent maintenant au retour. À jeudi, donc. Ne vous inquiétez pas si nous\n'
 'sommes quelque peu en retard. Venons par Chartres, Dreux, Évreux, Louviers.\n'
 'Affectueux baisers de tous. Robert.',
 'Mes amitiés. Bonnes vacances à tous. G.',
 'Lundi 19 juillet,\n'
 'Chère madame,\n'
 'Excusez-moi si je ne vous ai pas écrit depuis mon départ, mais nous avons '
 'été\n'
 "si inquiets de Mme Guignant mère que je n'avais guère d'entrain. Elle a "
 'subi\n'
 'une opération le neuf juin et, depuis, il y a eu des alternatives de mieux '
 'et\n'
 "d'aggravation. Elle est morte mercredi soir et ses obsèques ont lieu "
 "aujourd'hui,\n"
 'à Vaux. Je pense bien à vous pendant les pénibles jours que vous traversez '
 'et je\n'
 'vous suivrai par la pensée dans notre nouvelle installation. Mon entourage '
 'vous\n'
 'envoie à tous son bon souvenir et je vous embrasse bien affectueusement '
 'ainsi\n'
 'que les enfants. Hubert.',
 'Royan, 17 août, Hôtel des Pins. Avons fait bon voyage. Le temps semble '
 'vouloir\n'
 'se mettre au beau. Nous vous embrassons tous les trois affectueusement.\n'
 'Blanche',
 'Bons baisers de nous deux. Robert',
 'Chère mémé,\n'
 "Je te remercie pour le séjour chez toi en ce moment, j'ai une bonne "
 'bronchite.\n'
 'Bons bisous à pépé. Pierre Louis.',
 'Bons souvenirs à tous. G et E. Tassin.',
 'Sommes pour quelques jours dans cette région. Très handicapée par douleur\n'
 "dans l'épaule. Envisageons retour. Baisers affectueux pour tous. Geneviève.",
 "Ce n'est pas très sérieux pour commencer ma reprise de travail lundi. Enfin "
 'que\n'
 'cette carte te redonne goût à la vie et te fasse espérer dans des jours '
 'meilleurs !\n'
 'Je suis heureux de me remettre dans le bain. Un bonjour à Yvon. Bon vent...\n'
 'Frère.',
 'Royan, 16 juillet 1914\nCordial souvenir. E. Morcelles. Hôtel de Foncillon.',
 'Chère Anne-Marie,\n'
 "J'espère que tu vas bien que tu as fait un bon voyage. Je suis à Royan, je "
 "n'ai\n"
 "pas beau temps, il pleut tous les jours, mais aujourd'hui je suis allé me "
 'baigner,\n'
 "mais l'eau était froide. Je t'embrasse bien fort ainsi que tes parents et "
 'tes frères\n'
 'et sœurs. Sylvie Montrognon.',
 'Mon grand Christophe,\n'
 'Je pense que tu es chez tes parents en vacances avec tes frères. Tu vas '
 'reconnaître\n'
 'ce bac, tu es monté dessus. À bientôt, grosses bises. Mémé Jeannette.',
 'Bons baisers de la plage où nous passons de bonnes vacances avec un temps\n'
 'magnifique. Bons baisers de tous cinq. Yvette, Claude.',
 'Chers Blanche et René,\n'
 "Nous sommes arrivés ici hier midi pour une semaine, aussi aujourd'hui il a "
 'plu\n'
 'et le soleil est de sortie, enfin espérons que le beau temps va revenir pour '
 'que les\n'
 'gamins puissent se baigner. À bientôt. Famille Juillet.',
 'Cher mère et cher Gérard,\n'
 'Nous sommes bien arrivés à Royan où il fait beau. Je voudrais bien y rester '
 'en\n'
 "vacances, mais le travail d'abord. Bons baisers. À bientôt. Michel.",
 'Un bonjour de vacances. Marie.',
 'À Royan, 7 août 1937\n'
 "Très respectueux souvenir d'un court séjour ensoleillé. Je rejoindrai mon "
 'poste\n'
 'mardi matin. G. Vauvat.',
 'Cher Christian,\n'
 "Je viens vous donner des nouvelles de ma santé. C'est toujours le moral, je "
 'vais\n'
 "toujours voir le spécialiste à l'établissement Dubois. Cela fait deux ans. "
 'Je vais\n'
 'voir Lucette de temps en temps, car elle ne peut pas sortir comme elle '
 'veut.\n'
 'Merci pour votre invitation. Nous avons toujours un temps merveilleux. Je '
 'vois\n'
 "que pour vous tout va bien. J'étais contente que Françoise me téléphone. Je\n"
 "pense bien à vous tous les petits et les grands. J'espère aller vous voir, "
 'cela me\n'
 'fera du bien pour le moral. De bonnes et grosses bises à tout le monde. Ta '
 'mère\n'
 'R.Vit.\n'
 "Gino et Jeannine sont partis pour 15 jours dans les Pyrénées. Quand j'irai "
 'vous\n'
 'voir, je passerai quelques jours avec eux.\n'
 "Pour Janine, c'est pas merveilleux, deux crises au cœur !",
 'Chers amis,\n'
 'Nous avons beau temps. Tous bronzés. Climat idéal. Malheureusement ,le\n'
 'séjour passe trop vite. Bientôt, ça sera votre tour. Surtout, Madame Parig, '
 'si\n'
 "vous entendez que l'on demande une vendeuse, pensez à moi, je vous en "
 'serais\n'
 'reconnaissante. Acceptez ma sincère amitié. Nous rentrons le 15. Madame\n'
 'Bottés.',
 'Nous espérons que cette carte vous trouvera en parfaite santé maintenant. '
 "C'est\n"
 'le vœu que le vacancier heureux de leur état forme à votre adresse. Tout le\n'
 'monde a bonne mine et profite pleinement des joies de la plage et du jardin\n'
 'entourant la maison. Il faut dire que nous avons en plus de cela un très '
 'beau\n'
 'soleil. Tous se joignent à moi pour vous embrasser. Denise.',
 'Vu le prix du phare de Saint-Georges. La maison du lutin existe toujours.\n'
 'Sincères souvenirs à une amie. Laure. Prière de faire suivre.',
 'Chère amie,\n'
 'De Royan où nous passons un agréable séjour, beau site, bonne table. Un peu\n'
 'moins chaud depuis hier. Je viens vous souhaiter une bonne et heureuse '
 'fête.\n'
 "J'espère que votre séjour à Menton s'est bien passé. Vous devez avoir eu "
 'chaud\n'
 'aussi à Paris. Ma tante trotte comme un lapin et se joint à moi pour vous\n'
 'embrasser bien affectueusement.',
 'Notre bon souvenir de Royan. Émilienne.',
 'Chère madame et amie,\n'
 "Entendu pour samedi vingt-trois août. Je n'userai pas du train autant que\n"
 "possible, car j'envisage la course à bicyclette avec grand plaisir. J'espère "
 'que\n'
 "le beau temps me permettra de partir. J'arriverai sur les 9 h 30-10 heures. "
 'Si\n'
 "l'orage vient, je prendrai le train toutefois et ce ne serait qu'en cas de "
 'prévision\n'
 'de pluie durable que je ne partirai pas. Ce serait alors pour dimanche.',
 'Chère maman,\n'
 "Ainsi que tu peux t'en douter, tout va très bien, et la température est "
 'bonne,\n'
 'car il paraît que dans toute la France, il fait très chaud. Monsieur et '
 'Madame\n'
 "Lebœuf qui sont venus nous surprendre ce matin nous l'ont confirmé. Tu ne "
 'dois\n'
 "pas faire beaucoup de chemin. Je t'embrasse bien fort pour nous tous. "
 'Denise,\n'
 'Michel.',
 "Souvenir d'une belle journée. Monsieur Gavois.",
 'Profitons des vacances avec beau temps. Attendons les petits-fils pour '
 "l'automne.\n"
 'Bons baisers à tous. Pépé.',
 'Bon souvenir de vacances qui se passe très bien sous un beau soleil. Nous '
 'allons\n'
 'souvent à la plage. Grosses bises à vous deux. Claudine, Yannick.',
 'Saint-Georges, le 12\n'
 'Chers oncle et tante,\n'
 'Déjà huit jours que nous sommes rendues en colonie. Nous sommes allées au\n'
 'phare, aux huîtres, et nous nous baignons pas souvent, car il y a des cas '
 "d'angine.\n"
 "Je suis dans l'équipe des « Gargantua ». Je vous embrasse bien fort tous "
 'les\n'
 'deux. Bonne santé. Michelle.',
 "Chère mimi. J'espère que tu es toujours toujours sage. Je t'envoie ces "
 'petits\n'
 'enfants qui font joujou dans la mer comme tu faisais ici. Bons baisers de '
 'ta\n'
 'Maguy.',
 'Bons souvenirs. Marcelle Cher.',
 'Bons baisers Marcelle. M. Bigot.',
 'Nous sommes en famille, la conversation est des plus animées. Albert est '
 'là.\n'
 'Après un déjeuner assez copieux, nous sommes tous vaseux. Nous vous '
 'envoyons\n'
 'nos bons baisers. Mary, Marcel, Albert, Georgette, Némaine.',
 'Si vous saviez comme on est bien ici ! Clara se grise du parfum de la brise '
 'marine.\n'
 'On reste en extase devant les couchers de soleil. Ah ! Rien ne vaut notre '
 'Océan.\n'
 "Venez donc l'admirer si vous le pouvez. Bons baisers de ma part à Madame\n"
 'Blanche, un aimable bonjour à Mademoiselle Suzette et à votre grand-père.\n'
 'Recevez, petite amie, mille baisers de Marius.',
 "Sommes à Pontaillac à l'entrée de Royan. Très joli camp et promenade sur "
 'côte\n'
 'magnifique. Tu es très beau. Si besoin, écrire adresse : camping « la ferme '
 'de\n'
 'Pontaillac », 17 Vaux-sur-Mer. Baisers. Maman, Papa.',
 "La Charente-Maritime n'oublie pas ses amis et vous envoie les meilleurs "
 'souvenirs\n'
 "d'une permission trop courte. Recevez mes meilleures amitiés. J. Lartigues.",
 'Notre meilleur souvenir, et baisers de mémé. Maguie.',
 'Mémé Solé pense à toi. Bons baisers de mémé Jeannette Rigide. Pépé, Jeff.',
 'Chers parents,\n'
 'Nous sommes très bien arrivés et avons fait bon voyage sur une route bien\n'
 'calme. Nous sommes installés dans un petit coin bien calme, pas trop loin '
 'du\n'
 'marché et de la plage. Nous avons du beau temps, en un mot tout va bien. '
 'Vous\n'
 'espérons en bonne santé et se joignons tous les trois pour vous embrasser '
 'bien\n'
 'affectueusement. Germaine, Raoul, Michèle.',
 'Bien chers parents, Ma petite Niguette,\n'
 'Vendredi soir à neuf heures. Je vous ai écrit souvent afin que vous nous '
 'suiviez\n'
 "dans notre voyage. Comme je vous le disais, nous avons quitté l'Aiguillon,\n"
 'en Vendée, pays de pêcheurs tout à fait pauvre pour La Rochelle, ville très\n'
 'importante de quarante-cinq habitants, touristique et historique. Elle nous '
 'a\n'
 "bien plu. Nous y sommes restés jusqu'à trois heures cet après-midi. C'était\n"
 'très vivant, le port, la ville, beaucoup de mouvement. Nous avons continué '
 'sur\n'
 'Rochefort, mais là quel changement. Ville morte, vieille, sans rien '
 "d'intéressant,\n"
 'ni port ni ville. Nous avions un temps nuageux.',
 'Pontaillac, 27-7-57\n'
 'Chère amie,\n'
 'Comme prévu, je suis parti mardi soir couché à Châteauroux. Mercredi, je '
 'suis\n'
 'passé par La Rochelle et ai couché à la Dallice. Et le jeudi, après '
 "déjeuner, j'étais\n"
 "au camp. Tout le monde va bien et Bobby m'a fait fête, heureux de retrouver\n"
 'sa voiture pour y passer la nuit. Le temps est agréable. Beau, pas trop '
 'chaud.\n'
 'Je ne sais pas encore quand je prendrai le chemin du retour. Quelques jours '
 'de\n'
 'repos ne me déplaisant pas. Amitiés, et bons souvenirs de tous, et à la '
 'semaine\n'
 'prochaine. Patrick.',
 'Tu voudras bien leur donner le bonjour de ma part, et que je pense souvent '
 'à\n'
 "eux. Je t'envoie deux têtes qui vont te faire rire. C'est notre homme qui "
 'nous a\n'
 'photographiés en costume de bain au bord de la mer. Moi, je semble une '
 'vieille\n'
 'grand-mère. Réponds-moi de chez toi, tu me feras bien plaisir. Je ne sais si '
 'tu\n'
 "seras partie. Tu sais que j'attends l'adresse de Mme Allary.",
 'Cher monsieur,\n'
 "Enfin nous sommes installés, ici, à Royan, à l'hôtel Victoria. Nous avons\n"
 "aujourd'hui un temps affreux. Si cette mauvaise température persiste, nous "
 'irons\n'
 'chez Walter. Néanmoins, nous serons une huitaine ici. Mes parents me charge\n'
 'de les rappeler à votre bon souvenir et à celui de notre famille. Cher '
 'Monsieur,\n'
 "l'expression de ma reconnaissance. Mubert.",
 'Bons baisers de vacances. Beau soleil. Nous espérons que tu te portes bien. '
 'Tu\n'
 'embrasseras oncle Bernard de notre part. Grosses bises. À bientôt. Françoise '
 'et\n'
 'Daniel.',
 'Merci beaucoup. Bons souvenirs à tous. Nous partons demain. Germaine.',
 'Bon souvenir de vacances. Nous avons beau temps sans trop de chaleur. '
 'Toutes\n'
 'nos amitiés.',
 "De grosses bises de Royan que je visite aujourd'hui même avec ma chère "
 'copine.\n'
 'Cela nous a beaucoup plu. Nous allons maintenant visiter Saint-Palais. '
 'Grosses\n'
 'bises à tous.',
 'Ma chère Marie-Louise. Je compte sur toi pour samedi soir 8h30. Je serai à '
 'la\n'
 'gare avec Madeleine Saint Blancard. Marius arrive aussi par le même train '
 'que\n'
 'toi. Vous vous trouverez peut-être en route. Bons baisers à partager avec '
 'Jean.\n'
 'Suzanne.',
 'Écrit en mer. Claude, Simone, Georgette, Marthe, Henriette.',
 'Août 18\n'
 'Chère maman,\n'
 "Bien reçu ta lettre qui m'a fait très plaisir. Sinon, moi, aujourd'hui, "
 "c'est mon\n"
 "tournoi de Royan. C'est noir de monde. Toute la famille est sur la plage. "
 'Je\n'
 "t'attends, car nous devons prendre des huîtres à Marennes au retour.",
 'Le 28-8-1975\n'
 "Je te remercie pour ta lettre et pour tes sous. Je vais bien. Aujourd'hui, "
 'il ne fait\n'
 "pas beau. Ce matin, nous avons été en ville. J'ai acheté un livre. Je "
 "t'embrasse.\n"
 'François.',
 'Séjour agréable. Soleil, repos, promenades constituant le plus important de '
 'nos\n'
 'activités. Gros baisers à tous. Joël, Alex, Évelyne.',
 'Souvenir du camp du premier au cinq septembre 1969.',
 'Chers parents et Étienne,\n'
 'Bien reçu votre lettre. Nous allons toujours bien. Ça y est, on a de la '
 'chaleur :\n'
 "trente-cinq degrés dans la caravane. Vivement qu'il pleuve ! On a du mal à "
 'rester\n'
 "sur la plage d'ailleurs, le sable est brûlant. Nous espérons que vous avez "
 'passé\n'
 'un bon week-end. Dimanche nous sommes allés à La Rochelle. Bernard a fait '
 'un\n'
 'concours de boules dans le camp, il a été classé vingt-sept sur '
 'soixante-dix-huit\n'
 'et il a gagné... Le droit de rejouer. Nous avons eu une bouteille de '
 'champagne\n'
 "quand l'on a bu le soir même. Dites-nous quand vous comptez arriver qu'on "
 'soit\n'
 "là. Je joins l'itinéraire, mais c'est assez facile à trouver. Je vous quitte "
 'là en\n'
 'vous embrassant bien fort et souhaitant bonne route. Grosses bises aux '
 'jeunes\n'
 'et aux Poulots. Sylvie, Bernard.',
 '3/9/61\n'
 'Avant de prendre la route pour Perpignan et Alger, nous vous adressons tous\n'
 'les quatre nos plus affectueux baisers. Nous espérions un peu vous '
 'apercevoir\n'
 'à Paris pendant les quelques heures que nous y avons passées. Ce sera pour\n'
 'une autre fois. Espérons que ce ne sera pas trop long. Finissez de vous '
 'reposer\n'
 'pour aborder plus facilement cette nouvelle année puisque maintenant, un '
 'peu\n'
 'comme les gosses, nous comptons en année scolaire. Je vous embrasse très\n'
 'affectueusement pour quatre. Mick.\n'
 'Avec un peu de tristesse, beaucoup de tendresse en te disant que ta lettre '
 'est\n'
 "arrivée samedi au moment même où nous partions. C'était un peu toi qui nous\n"
 'embrassais ma petite sœur.',
 'Bons baisers de Royan. Michelle Delia.',
 'Beaucoup de soleil. Mais bientôt le boulot. Claude, Monique.',
 'Temps chaud, ciel sans nuage. Je profite au maximum du bon air, du bain et '
 'du\n'
 'dolce farniente. Meilleur souvenir.',
 'Samedi 7h30\n'
 'Nous sommes arrivés ici cette nuit à 1h30. De bonnes tendresses à mon '
 'lever.\n'
 'Temps idéalement beaux et doux ! Si le groupe Pasteur était ici !',
 'Chère petite sœur,\n'
 'Malgré que je ne trouve pas le temps long avec cette bonne Marie, je compte\n'
 "partir demain. Je vous prie de prévenir grand-mère et de l'embrasser pour "
 'moi.\n'
 "Marie se joint à moi pour vous embrasser tous d'un bon cœur. Votre frère, E "
 '.D.',
 'Chère tante,\n'
 "Un petit bonjour de Royan, ville très animée et agréable à s'y promener. "
 'Nous\n'
 'avions grand besoin de repos et nous profitons beaucoup de la plage, car il '
 'fait\n'
 'très beau. Valérie qui était très nerveuse se trouve un peu au calme avec '
 'nous et\n'
 'ça lui fait grand bien. Bons baisers. Janine, Jacques, Valérie.',
 'En promenade à Royan. Baisers. Fernandes, Angèle, Yvette.',
 'Chère bonne amie,\n'
 "Je suis très bien arrivé sans fatigue, mais aussitôt à l'hôtel, j'apprenais "
 'la mort\n'
 'de cette bonne Bretonne. Vous pensez que ce pauvre monsieur Pierre est '
 'chagrin\n'
 "et occupé ; je l'ai vu ce matin et je vais les rejoindre à Saint-Georges "
 'tantôt après\n'
 'avoir fait en tram la Grande Côte et Pontaillac. Nous serons pas là jeudi '
 'matin,\n'
 "l'enterrement sera demain après-midi. Je rentrerai moi jeudi dans "
 "l'après-midi.\n"
 'Je vais voir les falaises et vous écrirai. Bons baisers et déjà bons et '
 'meilleurs\n'
 'souhaits.',
 'Le 11 juillet 1909\n'
 'Chers parents,\n'
 'Votre lettre me fait bien plaisir de savoir que vous êtes tous en bonne '
 'santé et\n'
 "nous aussi pour le moment. Je ne sais pas encore quand j'irai à Guéret. Je\n"
 "vais avoir du monde et cela va me donner un petit peu plus d'occupation "
 "qu'à\n"
 "l'ordinaire. Mes amitiés respectueuses. Recevez chers parents les tendres "
 'baisers\n'
 'de vos enfants. Marie et Jean.',
 'Excellentes vacances avec un soleil splendide. Christine a été sage malgré '
 'le\n'
 'changement de lit. Nous sommes depuis trois jours à Royan. Bonnes amitiés.\n'
 'Ginette, Claude.',
 'Je pense que Gustave viendra demain dimanche passer quelques heures avec '
 'vous\n'
 "et que vous serez redevenu tout à fait gai, car j'espère bien que le sursis "
 'aura été\n'
 "obtenu. Je le souhaite ardemment. J'avais fait un petit oubli lors du départ "
 'de\n'
 "Renée qui m'avait qui m'avez dit de vous remercier de votre carte dit de "
 'vous\n'
 'remercier de votre carte que vous avez envoyée étant avec votre cousin. '
 'Pour\n'
 "aujourd'hui, je n'écris pas davantage, car j'ai à faire ce matin bien du "
 'travail à\n'
 'la maison vu que ce tantôt je vais aider la maman de Renée. Bien des choses '
 'de\n'
 'la part de mes parents et daignez recevoir mon cher filleul mes sincères '
 'amitiés.\n'
 'Et un grand bonjour. Marie.',
 "Bon souvenir d'un agréable séjour. Amitiés de deux amis. Yvonne et Alice.",
 'Bon souvenir à tous. Tonton, tata.',
 '6 août\n'
 "Ce n'est pas la pluie après dix jours magnifiques qui me donne goût à la "
 'plume,\n'
 'mais la pensée des Parisiens rester au bercail. Peut-être pas pour longtemps '
 '?\n'
 "Reçu une lettre collective de Françoise, mais j'avais eu des détails par "
 'Michel,\n'
 'heureux de sa soirée avec vous. Mille baisers. Max.',
 'Cher Dédé, chère Lydie,\n'
 "Je croyais avoir de vos nouvelles aujourd'hui, mais rien reçu : je vous "
 'pense tous\n'
 'deux en bonne santé ; nous avons eu le colis samedi et nous vous remercions\n'
 'beaucoup ; le sulfate se balader partout, la boîte avait été mal mise. Il '
 'fait un\n'
 'sale temps depuis plusieurs jours et de la tempête ; hier impossible de '
 'sortir.\n'
 "Aujourd'hui, je suis allé à Pontaillac à l'entraide pour avoir des effets "
 'chauds pour\n'
 'Mady ; il faut encore y retourner demain. Avez-vous été chez Margot ? Vous\n'
 'auriez pu demander à Estelle un manteau pour Lydie. Bons baisers affectueux\n'
 'de tous pour vous.',
 'Le 12 juin 81\n'
 'Cher David,\n'
 "Nous voilà revenus à l'écurie par beau temps très chaud. Nous sommes "
 'heureux\n'
 'de te savoir bientôt en vacances et pensons que vous êtes tous en bonne '
 'santé.\n'
 'Serons heureux de vous voir à votre passage. Micros bises de nous deux. '
 'Marcel.\n'
 'Bons baisers pour toi et tes frères. À bientôt mon coco. Mémé.',
 'Tempête du 10 novembre 31\n'
 'À ma lettre, je joins cette carte, mon cher cousin, en souvenir de la '
 'tempête qui\n'
 "a eu lieu pendant que j'étais avec vous. Bien abriter dans votre bonne "
 'maison\n'
 'nous parlions au coin du feu, mets un bruit qui monter de la plaine '
 'annonçait\n'
 'que la tempête venait ici. En voyant ses vagues sous la courbe si abritée de '
 'la\n'
 "Conche, pensez à ce qu'elles étaient sur les falaises ! C'est à une tempête "
 'de\n'
 "cette ampleur de trois jours et de trois nuits que l'oncle Lecoud a assisté "
 "lorsqu'il\n"
 'était avec nous à la rue des fardes. Affectueusement.',
 "Qu'il est doux de ne rien faire, mais courage, votre tour approche. "
 'Meilleurs\n'
 'souvenirs de Royan. Christine.',
 'Chers amis\n'
 'De Saint-Georges où nous continuons nos vacances ensoleillées. Nous vous\n'
 "envoyons notre bon souvenir. J'espère que notre petite carte vous trouvera "
 'tous\n'
 'en bonne santé.',
 'Un bonjour de Royan où nous pensons bien à vous. Les filles profitent bien.\n'
 'Charline dort bien, elle est sage. Audrey un peu moins, mais tout va bien. '
 'Il\n'
 'fait chaud. Grosses bises à vous. Bonjour à tout le monde. Maryline.\n'
 'À bientôt les filles. Thierry.',
 'Le 8 septembre 1963\n'
 'Merci de votre carte de Vichy. Nous espérons que ce repos vous a été très\n'
 'bienfaisant. À notre tour, nous avons pris quelques jours de vacances qui '
 'se\n'
 'terminent dimanche. Sentiments très amicaux. À bientôt.',
 'Saint-Palais-sur-Mer, le 7-5-1980\n'
 'Chers Monsieur et Madame,\n'
 'Merci beaucoup pour votre gentille lettre et vos bonnes nouvelles. Pour '
 'nous,\n'
 'cela se maintient et avec joie, nous allons nous retrouver bientôt. Ici, il '
 'fait un\n'
 "temps très beau et la saison s'annonce bien. Donc vous pouvez arriver quand "
 'il\n'
 'vous plaira, tout sera prêt. Dans cette attente, mon épouse se joint à moi '
 'pour\n'
 'vous saluer tous très respectueusement. Gourbeuil.',
 'Chère Madame,\n'
 'Nous voilà installés depuis samedi. Il fait un temps splendide. Le soleil '
 'est\n'
 'chaud, mais la brise de la mer tempère beaucoup et le soir il faut se '
 'couvrir un\n'
 "peu. Occupons le même logement qu'autrefois, et une famille de Parisiens "
 'arrive\n'
 'demain. Bonnes amitiés. Molinier, trente, avenue des tilleuls, Royan.',
 'Chère mémé,\n'
 "J'espère que tu vas bien et que le soleil est au rendez-vous des vacances. "
 'Nous\n'
 "nous promenons tous les jours et aujourd'hui nous attendons le petit-fils à "
 'Chariel.\n'
 'Gros bisous à toi et à Jeff ! David. Je pense bien à vous en ces jours de '
 'vacances\n'
 'avec mon petit fou. Anne. Bonjour, Christophe, à Jeff. Jeannette',
 'Chère marraine et oncle,\n'
 'Les vacances se terminent bien, nous avons fait un détour par la côte. Il '
 'fait un\n'
 "temps superbe. J'espère que vos vacances se passent bien. Je vous quitte en\n"
 'vous embrassant bien fort. Bernard, Jean-Claude.',
 'Bon souvenir de vacances. Vous pensons en bonne santé tous deux. Nous, ça '
 'va\n'
 'très bien. Malheureusement temps incertain. Nous sommes à neuf kilomètres '
 'de\n'
 'Royan dans un endroit très calme, aussi on pique de bons roupillons : onze '
 'à\n'
 "douze heures par nuit. On n'en prend pour notre année. Affectueux baisers "
 'de\n'
 'nous trois. Colette, Fred.',
 'Le 20 août 1955\nDe... Royan. Amical souvenir. J.-D.',
 'Bien reçu ta carte et te remercie pour le dérangement. Le principal que tout '
 'le\n'
 "monde soit reçu. J'espère que pour le boulot ce n'est pas trop dur. Pour moi "
 'RP\n'
 "est bien loin. Ici il fait beau. Hier j'étais à la pêche au carrelet. On est "
 'revenu\n'
 'avec environ 5 kg de poisson tous les deux. Bien le bonjour aux copains et '
 'vive\n'
 'les vacances. Cordiales poignées de main. Amitiés à ta famille. Pierre.',
 "J'ai le plaisir de passer la soirée à Royan avec tante. C'est une "
 'magnifique\n'
 'réalisation. Nous avons un tarif idéal et sommes heureuses de jouir ensemble '
 'de\n'
 'ce beau cadre. En vous espérant, en bonne santé, nous vous envoyons, à tous '
 'les\n'
 'trois, nos meilleurs baisers. Andrée.',
 '16-08-1922\n'
 'Mes bien chers grands frères et sœurs,\n'
 'Je vous ai déjà envoyé une longue lettre ce matin, mais je viens de recevoir '
 'la vôtre\n'
 'à 2h. Merci beaucoup et je vous embrasse tous les deux biens '
 'affectueusement. Je\n'
 "suis raisonnable et me soigne bien et ne m'ennuie pas, mais je serai "
 'naturellement\n'
 'contente de retourner auprès de Margot et de vous. Ici nous avons du beau\n'
 "temps ; je n'ai vu qu'une matinée de pluie. Par moment, il fait un peu plus "
 'frais,\n'
 "mais en se couvrant on est très bien surtout au soleil où je m'étends comme\n"
 "une grande flegme. Je n'ai pas encore grossi, mais patience il n'y a encore "
 'que\n'
 "quinze jours. Toujours pas de mieux. Consoler vous, rien de plus mal, c'est "
 'déjà\n'
 "beau. J'espère que vous allez bien et je vous quitte en vous embrassant de "
 'tout\n'
 'cœur. Anne-Marie.',
 'Bon souvenir de vacances. Amitiés. Madame Hourdel.',
 'Royan, le 18-09-73\n'
 'Chers amis,\n'
 'Votre longue lettre est venue nous rejoindre avant que nous quittions le '
 'vingt\n'
 "septembre pour Brigueil où nous serons jusqu'au premier novembre. Le "
 'vingt-six\n'
 "nous assisterons notre veille bonne à l'hôpital de Montmorillon qu'on va "
 'opérer\n'
 "d'un abcès, mauvaise chose !, et j'ai beaucoup de peine. Je serai chez moi "
 'le\n'
 'vingt-six au soir. Si vous voulez vous arrêter au retour de Reims à '
 'Brigueuil, je\n'
 'peux vous recevoir pour déjeuner ou pour diner avec le plus grand plaisir '
 'avec\n'
 'votre ami. Vous récupérerez la chambre à deux lits jumeaux. Prévenez-nous\n'
 'dès que possible. Téléphone trente-huit, à Brigueuil, par Montmorillon, '
 'Vienne.\n'
 'Nous aurons le temps de parler « chiens ». En attendant de vos nouvelles et\n'
 'votre passage en Poitou, je vous embrasse tous les deux. Pour nous deux. '
 'Vos\n'
 'très. L. Repuis.\n'
 'Des succès aux chiens dans deux ring à Reims.',
 '9 septembre 1908.\nNos meilleures amitiés. Irma.',
 'Royan, 11 juillet 1958\n'
 'Ma petite Marie-Thérèse,\n'
 "Je me fais l'interprète de toute la famille pour vous adresser les "
 'affectueuses\n'
 'félicitations que vous méritez pour votre beau succès. Peut-être, les '
 'vacances\n'
 "nous permettront elles de vous les renouveler de vive voix, car j'espère que "
 'vous\n'
 "songerez à ne plus travailler d'ici septembre. Notre amical souvenir à vos "
 'parents.\n'
 'De bons baisers pour vous. Catherine et François. Et encore une fois : bravo '
 '!\n'
 'J. Hugard.',
 "Bien amicalement de Royan sous le soleil. Nous passons d'agréables vacances. "
 'À\n'
 'bientôt. Famille Destenay.',
 'Souhaits affectueux. J. Gaurier.',
 'Depuis quelques jours le soleil nous réchauffe.. Nous en sommes heureux. De\n'
 'grosses bises de nous tous.',
 'Samedi soir. Arriverai lundi 12h30. Charles.',
 'Chers amis,\n'
 'Si vous pouvez suivre sur une carte le trajet que nous avons fait, vous '
 'allez vous\n'
 'rendre compte que quelques jours de repos sont nécessaires. Nous sommes à\n'
 "Saujon où nous étions l'année passée. Nous reviendrons par les châteaux de "
 'la\n'
 'Loire lundi après-midi ou mardi matin. À bientôt. Musy.',
 '26 avenue de la Gare, Royan, Charente Inférieure\n'
 'Quelques jours passés sur cette plage ou plus simplement à Royan vous '
 'reposeraient très bien de vos durs travaux de moisson. Mme Huguet et moi '
 'vous\n'
 'adressons à toute la famille nos plus affectueux souvenirs.',
 'Souvenirs. Georges.',
 'Le mercredi 21 juin\n'
 'Bien chers tous trois,\n'
 'Nous passons de très bonnes vacances avec du temps très chaud. Nous sommes\n'
 "très heureux d'être en famille, c'est la gaieté. Notre Jacky est en pleine "
 'forme,\n'
 'mange très bien. Nous allons chaque jour à la baignade. Il aime ça, il a des '
 'coups\n'
 'de soleil jusque dessus les pieds. Dimanche, nous allons en pique en '
 'pique-nique\n'
 "dans l'île de Ré avec une bande de jeunes. Nous désirons de tout cœur que\n"
 'Tante Céline se porte mieux. Bientôt les vacances, Adriano ? Nos très '
 'affectueux\n'
 'baisers et bons souvenirs de ma famille.',
 'Voyage non réussi. Trop de monde sur les routes, mais nous sommes payés de\n'
 'nos peines, car le temps est très beau. Beaucoup de monde. Rien de '
 'sensationnel\n'
 "dans Royan. Toujours les mêmes smalas sur la plage. L'eau est supportable\n"
 'même pour des gelés – machin. Avons reçu nouvelles des tourtereaux qui ne\n'
 'paraissent pas enchantés par leur séjour pluvieux. Nos gros baisers. Michel.',
 '28/8/1931\nBeau temps. Séjour agréable. L et J. Coudrai.',
 "D'une croix, je vous ai marqué la villa.\n"
 'Chère Marie,\n'
 'Le temps est inespéré après la fin de juillet que nous avions eue. Soleil et '
 'chaleur\n'
 'font que nous nous baignons tous les jours. Les enfants apprécient surtout.\n'
 'Jacques est reparti ce matin, mardi, après être arrivé de Chambray samedi '
 'soir.\n'
 "Paul et Jeannine sont arrivés l'autre semaine, mais ils ont dû repartir "
 "aujourd'hui.\n"
 "Mon beau-père est parti aujourd'hui pour Villejuif pour y subir pendant cinq "
 'ou\n'
 "six jours des séances de rayon. Ma belle-mère, fatiguée, ne l'a pas "
 'accompagné.\n'
 "Janine s'est donc privée de la fin de ses vacances pour tenir compagnie à sa "
 'mère.\n'
 'Paul repart à Paris. Jacques est à Chambray pendant ce temps. Je suis donc '
 'ici\n'
 'avec Éric et les enfants. Nous repartirons peut-être avant la fin du mois, '
 'cela\n'
 "dépend de l'état de santé de mon beau-père. Affectueusement. Pierrette.",
 '2-7-1920, 19 rue Notre-Dame, Royan\n'
 'Respectueux souvenirs de Royan. Thérèse, Patrick.',
 'Samedi 1er juillet 72\n'
 'Chers grands-parents,\n'
 "Tout va bien à bord. À l'arrivée, il a plu. Je vous envoie de gros bisous. "
 'Bibi.\n'
 'PS : Dis le bonjour à Tata et tonton.\n'
 'Chers tous deux,\n'
 'Voyage effectué dans de très bonnes conditions. Nous sommes installés et '
 'nous\n'
 "allons profiter maintenant des vacances. La pluie s'est arrêtée et le soleil "
 'brille.\n'
 "Espérons qu'il ne nous quittera plus. Philippe est en pleine forme. Mille "
 'fois\n'
 'merci pour Nicole que je suis confuse que vous gâtiez ainsi. À tous deux, '
 'toutes\n'
 'nos pensées affectueuses et de grosses bises. Denise, Nicole, Charly.',
 'Chère Marie-Laure,\n'
 "Je passe d'agréables vacances à Saint-Palais où le soleil brille. Grosses "
 'bises.\n'
 'Liliane.',
 '23 mars 1908\nSouvenirs affectueux. Prevotel.',
 "Amical souvenir d'une bonne semaine de vacances, trop vite passée. À "
 'bientôt.\n'
 'Marguerite.',
 'Le 28 août 1962\n'
 'Chers amis,\n'
 'Nous sommes ici depuis quinze jours et pensons rester la semaine prochaine. '
 'Ce\n'
 "changement d'air me fait du bien après le « tintouin » de la noce. Nous "
 'habitons\n'
 'dans un appartement qui appartient à ma sœur de Rodez. Elle est ici avec '
 'nous\n'
 "et sa fille. On se réchauffe les idées avant d'affronter les longues "
 "journées d'hiver.\n"
 "J'espère que vous aussi avez pris un peu de repos. Donnez le bonjour de "
 'notre\n'
 'part à Pierrot et sa gentille femme et recevez de nous deux nos plus '
 'sincères\n'
 'amitiés.',
 'Bien affectueux souvenirs à vous deux. Jean.',
 "Je vous souhaite un été meilleur. Ici, de l'eau tous les jours. Bonjour à "
 'vos\n'
 'enfants. Amitiés. Pierre.',
 'On rigole avec beaucoup de gens, car nous sommes des équipes qui sont pas '
 'des\n'
 'ennemis de la joie. Moi, je suis facteur auxiliaire et je fais le cordonnier '
 '; et je\n'
 "reste sur le bord de la Sèvres. Je vais toujours m'occuper de ce que vous "
 'me\n'
 "demandez. Répondez-moi pour la date. En l'attente de vos nouvelles, "
 'recevez,\n'
 'cher camarade, une conviviale poignée de main. Raymond Bessus.',
 'Chère Mademoiselle,\n'
 'Recevez de Royan toutes mes bonnes amitiés. Jean Aupart.',
 'Souvenirs de vacances. Monique.',
 'Pontaillac, le 3 août,\n'
 'Charles, cher Arnaud,\n'
 "J'ai fait un très bon voyage, mais j'étais fatigué en arrivant, car nous "
 'sommes\n'
 'arrivés à 6h du matin ici, ayant passé la nuit très durement. Je vous '
 'écrirai\n'
 'bientôt plus longuement. Dites-moi quand vous partez. Meilleurs baisers. '
 'Marie.\n'
 'Villa les hirondelles, Royan, Pontaillac, Charente Inférieure.',
 "Amicale pensée de Royan avec beaucoup de soleil jusqu'ici. A. Noury.",
 'Royan, le 20 mars 1933.\n'
 'Ma chère Nini,\n'
 'Bien reçu votre carte. Ayant trois bonnes à son service, la maman Barilleux\n'
 "vous attend avec impatience. Cependant, je crois qu'il n'y a pas lieu "
 "d'écourter\n"
 "votre séjour, car elle est bien soignée d'après Marie. Mon meilleur souvenir "
 'à\n'
 'vous tous, votre B. F.',
 'Jolie journée de Pâques ensoleillé. Une belle fête. Royan en 1900 : '
 'bicycles,\n'
 'costumes, il ne manque rien. On repart de ce soir. Baisers affectueux. '
 'Papa,\n'
 'maman.',
 'Bon souvenir à tous. Louise, Yvonne.',
 'Bon souvenir de toute la famille Baffert.',
 'Le 20-8-81\n'
 "Meilleurs souvenirs de vacances. Aujourd'hui, c'est notre premier jour de "
 'pluie.\n'
 'Les enfants ont bien profité. Nous sommes allés au zoo de la Palmyre et '
 'dans\n'
 'un village du Far West. Toutes nos amitiés. Madame Richard.',
 'Mon cher petit Roger,\n'
 "Nous sommes à Royan depuis ce matin. Le temps est superbe. Je t'embrasse\n"
 'bien fort.',
 'Chère Rolande, papa, maman,\n'
 "J'ai une prolongation de 15 jours ici, mais nous en faites pas ça va très "
 'bien\n'
 "maintenant, mais j'avais été un peu secoué par la maladie de Mimi, mais ça\n"
 "va mieux, elle aussi, mais le docteur aurait voulu qu'elle parte trois mois "
 'dans\n'
 'une maison comme ici, mais elle ne veut de toute façon quitter nos enfants '
 'et\n'
 "je la comprends alors. Elle s'ennuierait et cela ne lui servirait à rien. Et "
 'vous,\n'
 "comment allez-vous ? Bien ? J'espère que la santé est bonne et que pour le "
 'reste\n'
 "ça va aussi. J'envoie carte à Georgette et Lucienne, la même chose. Je vous\n"
 'embrasse tous les trois de tout cœur. André.',
 'Bons souvenirs de vacances bien agréables. Roger, Frédéric et Valérie.',
 'Souvenir de Royan. 10 août 1906. Marius Sazel.',
 'Le 12-09-1906\n'
 'Chère Victoire.\n'
 'Nous allons passer quelques jours à Bourganeuf. Nous espérons bien vous '
 'voir.\n'
 "J'ai passé quelques heures avec vous. Nous partons le 14 et vous envoyant "
 'en\n'
 "attendons nos meilleures amitiés ainsi qu'à ma tante.",
 'Un petit bonjour de vacances. Le temps est chaud. Je pense que tout va très\n'
 'bien pour vous ainsi que tous. Gros bisous. Gérard, Céline, Sébastien Botté.',
 'Ce dimanche\n'
 'Ma chère Lydie\n'
 'Hier nous pensions avoir un petit mot de toi ainsi que de Serge, mais rien '
 'est\n'
 'venu : ce sera sûrement pour mardi ; nous te pensons en bonne santé ainsi\n'
 'que Jeanne et Simone dit que vous vous plaisez bien à la pension ; Monsieur\n'
 "Renge doit aller à Jonzac jeudi et il va vous voir ; si je n'envoie pas tes "
 'cartes\n'
 "d'alimentation, il les remettra jeudi à Mademoiselle Chabot à qui tu "
 'voudras\n'
 "bien transmettre notre bon souvenir ainsi qu'aux demoiselles. JeanMarie est\n"
 "parti ce matin avec Monsieur Renge à Cognac avec l'équipe ; il doit "
 "t'envoyer\n"
 "une carte ; si Royan gagne, ils montront probablement en division d'honneur "
 ';\n'
 "je serai content. Pas cette semaine, mais l'autre après, nous viendrons, "
 'mais\n'
 'quel jour je ne sais ; le lundi ou vendredi. Je te quitte pour ce jour. '
 'Reçois de\n'
 'nous tous aussi de Josi de bons baisers affectueux. Ta maman. Maddy a 19 '
 'ans\n'
 'mardi.',
 'Temps agréable, mer admirable. Gérald.',
 'Saint-Georges-de-Didonne, Charente inférieure, Hôtel Central, rue du '
 'Général\n'
 'Pétain\n'
 'Nous sommes à cette adresse depuis hier au soir, quatorze juillet. Nous '
 'avons\n'
 'voyagé par une chaleur épouvantable, Bakou et Poupon ont été très mal à '
 "l'aise.\n"
 "Ça va très bien. La forêt et la mer, c'est merveilleux. Bons baisers. "
 'Francis.',
 "Petit amie, L‘océan n'a donc pour vous aucun attrait, que je n'ai pas encore "
 'eu\n'
 "l'honneur de votre visite. C'est jolie, mademoiselle ! La bécane ne marche "
 'pas ?',
 'Toujours en route, nous sommes à Royan. La ville est magnifique, très '
 'moderne.\n'
 'Nous partons ce soir pour Blaye. Les camps ici sont bondés aussi nous avons '
 'dû\n'
 'sortir de la ville afin de pouvoir trouver un camp tranquille à huit '
 'kilomètres. À\n'
 'bientôt. Bons baisers.',
 'Bons baisers de Royan. Martine.\n'
 "Je ne sais pas si nous reviendrons bronzées, mais pour l'instant nous nous "
 'faisons\n'
 'dorer au soleil. Bons baisers. Françoise.',
 'Chers amis,\n'
 'Je viens vous adresser mes meilleurs souvenirs de vacances en attendant le '
 'plaisir\n'
 "de vous voir en août. Ici, le temps s'est ralenti, les jours traînent. "
 'Vivement de\n'
 'revoir le clocher de Saint-Michel ! Heureusement, il ne fait pas très chaud. '
 'Vous\n'
 'devez profiter de vos petits-enfants. Amitiés. À bientôt. Dominique.',
 'Mon cher Francis,\n'
 'Nous sommes sûrs que ta convalescence se poursuit favorablement et que les\n'
 'bonnes nouvelles que nous donne tes parents te permettront un rapide retour '
 'à\n'
 "la maison. Nous te souhaitons le retour rapide de tes forces et t'envoyons "
 'nos\n'
 'plus amicales pensées.',
 'Bon souvenir de Royan. La mer est bien belle et nous profitons pour prendre\n'
 'de bons bains de pieds, car nous ne savons pas nager. Le temps est bien '
 'beau\n'
 "et chaud. Nous espérons qu'à Rouen le temps est beau aussi à la fin du "
 'mois.\n'
 'Recevez notre bonne amitié. Monsieur et Madame Hédouin.',
 'De promenade à Royan, recevez mon bon souvenir. Lucette.',
 'Je pense toujours à mes Liza et regrette bien de ne pas les avoir près de '
 'moi\n'
 'pour radoucir mon caractère qui est devenu comme une râpe, mais on me le\n'
 'pardonne. Adieu Lisa et amitiés à toutes. Tonton Dorian.',
 'Bons souvenirs de Royan. Germaine.',
 'Chers frères et sœurs,\n'
 "Passons d'excellentes vacances dans les Charentes avec un temps superbe.\n"
 'Le pineau est délicieux de même que la nourriture. Partons dimanche pour\n'
 "l'Auvergne. Bons baisers. Marcel.",
 'Cher petit olivier,\n'
 "Merci de ta gentille carte. Tu dois être bien content d'avoir été à La "
 'Baule. Gros\n'
 'baisers. Mémé Jeanette.',
 'Sommes partis samedi dix-huit avec mon frère pour Saint-Palais. Nous '
 'rentrerons\n'
 'dimanche, donc je vous attends la semaine prochaine. Nous avons un temps\n'
 'superbe et commençons à Bruni. Bon souvenir à tous deux.',
 'Chers parents,\n'
 'Mes sincères pensées. À jeudi prochain. Bons baisers de nous deux. Votre\n'
 'grande.',
 'Le 28 août 1923\nBonjour de Royan. Bons souvenirs. Gilbert.',
 'Le temps est toujours beau, les vacances se sont bien passées. À bientôt, '
 'bons\n'
 'baisers. Jean, Sandy',
 'Chère madame,\n'
 'Un petit mot de souvenirs de cette jolie plage, où nous passons un séjour\n'
 "charmant. J'espère que de votre côté vous passez de bonnes vacances.",
 "On réserve pour vous. J'ai aperçu Thérèse hier en voiture. Mon mari l'a\n"
 'rencontrée ce matin et elle nous a fait annoncer sa visite pour demain. '
 'Merci\n'
 'encore avec toutes mes amitiés pour vous et autour de vous et des baisers '
 'aux\n'
 'enfants.',
 'Royan 30 décembre 1906\n'
 'Chers cousins,\n'
 "C'est bien aimable à vous de songer à m'envoyer des cartes durant votre "
 'voyage.\n'
 "Merci des deux vues de Biarritz, merci surtout de l'attention ! Nous avons "
 'ici\n'
 "aussi neige grêle, etc. N'est-ce pas le temps de la saison ? Mille vœux de "
 'bonheur\n'
 'pour 1907. R.N.V',
 'Cher frère, sœurs et neveux,\n'
 "Nous sommes arrivés à Royan après avoir fait un petit détour par l'Espagne "
 'qui\n'
 'nous a fortement déçus. Tout y est sale et miséreux et à la place de soleil, '
 'ce\n'
 "n'était que pluie. Il nous a été impossible de dresser la tente, nous "
 'pouvions à\n'
 'peine sortir de la voiture, des routes étaient coupées par la crue des '
 'ruisseaux,\n'
 "ce qui nous a fait faire un grand détour. Je croyais ne plus m'en sortir de "
 'cette\n'
 'Espagne. Enfin, maintenant, nous sommes bien en France et nous y resterons.\n'
 "J'espère que vous êtes en bonne santé et que Denise se porte toujours bien. "
 'Bons\n'
 'baisers. Thierry, Claude, Colette.',
 'Royant, le 29 août\n'
 'Nous arrivons de voir la fille de Rolland, le mot splendide est insignifiant '
 'pour\n'
 'définir ce que nous avons vu. Amitiés à tous. Pauline',
 'Chers amis,\n'
 'Ici, un temps splendide. Espérons que la Normandie est un peu ensoleillée '
 'elle\n'
 'aussi. Bien fatigué, mais je profite quand même de voir de beaux paysages. '
 'Les\n'
 'plages sont jolies. À bientôt, le plaisir de vous revoir. Je pense bien à '
 'Madeleine.',
 '2/7/20,\nRespectueux souvenirs de Royan. Thérèse, Dietrich.',
 "Étant un peu abrutie, je croyais cette carte nouvelle alors qu'elle est "
 'ancienne.\n'
 "Ça ne t'apprendra donc rien. Je ferai faire des identités samedi ou lundi et "
 'tu\n'
 "les porteras avec le chèque, mais c'est pour le 25. Pas de panique. Je "
 "t'embrasse\n"
 'mon cher, bien triste encore et tellement navré pour lui et pour toi. À toi.',
 'Il sera très facile de trouver une maison avec électricité. Ici le courant '
 'est en\n'
 'deux cent trente volts et il coûte deux francs trente-cinq le Kilowatt.\n'
 "Ces deux cartes, c'est mon beau-frère qui est maitre pêcheur qui les a "
 'écrites.\n'
 "J'ai bien trouvé une maison garnie à louer avec électricité à cent francs "
 'par mois,\n'
 "mais je trouve que c'est un peu élevé pour vous. J'espère trouver mieux.",
 'M. et J.,\n'
 "Après un voyage à l'île de Ré, La Rochelle, Rochefort, Bordeaux, je me suis\n"
 'reposé ici, sur cette plage toujours charmante. Amitiés.',
 'Chers tous,\n'
 "Passons un agréable séjour à La Rochelle. Aujourd'hui, nous sommes à Royan.\n"
 'La plage est très belle, le temps est plutôt orageux. Dommage que nous '
 'devions\n'
 "rentrer, car nous avons passé d'agréables vacances. Bons baisers. Nadine, "
 'Dany,\n'
 'Martine, Pierrot.',
 'Bonnes amitiés de Royan. Pense rentrer demain soir à six heures et demie. '
 'Je\n'
 "passerai chez vous pour déposer mes valises et irai coucher à l'hôtel. Je "
 'passerai\n'
 'la journée de mercredi avec vous. Amitiés.',
 'Jeudi 20 septembre,\n'
 'Cher grand-père,\n'
 "Ta petite carte m'a fait grand plaisir. Mes parents, mes grands-parents et "
 'moi\n'
 "t'attendons toujours. J'espère que tu vas bien vite venir. Gros baisers. "
 'Pascal',
 'Un bonjour de vacances, les journées passent vite. Pensons bien à vous '
 'deux.\n'
 'Tout va bien. Il fait beau et allons bien. À bientôt. Grosses bises. '
 'Pauline,\n'
 'Thierry, les filles.',
 "Ce n'est pas la Côte d'Azur, mais c'est tout de même bien agréable avec du\n"
 "soleil même s'il n'est pas très chaud ! Dans quelques jours ce sera votre "
 'tour et\n'
 'avec mon meilleur souvenir, je vous souhaite de bonnes vacances.',
 'Affectueux souvenirs de tous. Sabrina.',
 "Bonjour de Royan où nous passons d'agréables vacances. Bien "
 'affectueusement.\n'
 'Simone, Auguste, Chantal, Annick, Christian, Danièle.',
 'Un bonjour de notre voyage à Royan. Temps magnifique, tout va bien. Espère\n'
 "qu'il en est de même pour vous. Grosses bises de Renée et Raymond et Claude.",
 "Nous arriverons vendredi soir à 17h23, gare de l'État. Tout va bien. Bons "
 'baisers.\n'
 'J. Couprie.',
 "Bien arrivé. Nous avons du beau soleil et nous sommes bien. J'espère que "
 'vous\n'
 'avez aussi du beau temps et que vous allez aussi bien que la dernière fois '
 'que\n'
 "l'on vous a vu. Bons baisers à bientôt. Didier, Brigitte."]

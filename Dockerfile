# docker build -t royan .
# docker run -p 8000:8000 -v ~/royan_output:/royan/output --rm -it royan

FROM python

WORKDIR /royan

ADD  data data
ADD  page page
ADD  cerevoice cerevoice
COPY gen.py .
COPY config.py .
COPY server.py .
RUN  mkdir output

EXPOSE 8000

# Start server
# -u flag to enable logging in docker desktop (disable buffering)
CMD [ "python3", "-u", "server.py" ]

